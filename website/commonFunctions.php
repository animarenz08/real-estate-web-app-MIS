<?php
include '../config/config.php';
include '../common/class.users.php';
include '../common/class.ajax.php';

session_start();

if(!empty($_POST)){
	if(isset($_POST['newSalesAgent'])){

		$user = new User();

		$params = array(
			'userId' => $_POST['userId'],
			'idUser' => $_POST['userId'],
			'userType' => 4,
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'firstName' => $_POST['firstName'],
			'middleName' => $_POST['middleName'],
			'lastName' => $_POST['lastName'],
			'spouseName' => $_POST['spouseName'],
			'nickName' => $_POST['nickName'],
			'DOB' => $_POST['DOB'],
			'Age' => $_POST['Age'],
			'gender' => $_POST['gender'],
			'tinNumber' => $_POST['tinNumber'],
			'emailAddress' => $_POST['emailAddress'],
			'contactNo' => $_POST['contactNo'],
			'homeAddress' => $_POST['homeAddress'],
			'occupation' => $_POST['occupation'],
			'seminarDate' => $_POST['seminarDate'],
			'seminarVenue' => $_POST['seminarVenue'],
			'recruitedBy' => $_POST['recruitedBy'],
			'recruitedByPos' => $_POST['recruitedByPos'],
			'recruitedByDiv' => $_POST['recruitedByDiv'],
			'trainorName' => $_POST['trainorName'],
			'salesDirector' => $_POST['salesDirector'],
			'divManager' => $_POST['divManager'],
			'occ_name' => $_POST['occ_name'],
			'occ_pos' => $_POST['occ_pos'],
			'occ_from' => $_POST['occ_from'],
			'occ_to' => $_POST['occ_to'],
			'educSchool' => $_POST['educSchool'],
			'educLocation' => $_POST['educLocation'],
			'educAttainment' => $_POST['educAttainment'],
			'educYearFrom' => $_POST['educYearFrom'],
			'educYearTo' => $_POST['educYearTo'],
			'accountCreate' => 'Registration',
			'divisionId' => $_POST['recruitedByDiv']
		);

		$result = $user->insertUserData($params);
		if($result){
			$user->redirect('login.php');
		} else {
			$user->redirect('login.php');
		}

	} else if (isset($_POST['empnewUser'])) {
		$user = new User();

		$params = array(
			'userId' => $_POST['userId'],
			'idUser' => $_POST['userId'],
			'userType' => $_POST['userType'],
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'firstName' => $_POST['firstName'],
			'middleName' => $_POST['middleName'],
			'lastName' => $_POST['lastName'],
			'nickName' => $_POST['nickName'],
			'DOB' => $_POST['DOB'],
			'Age' => $_POST['Age'],
			'gender' => $_POST['gender'],
			'tinNumber' => $_POST['tinNumber'],
			'emailAddress' => $_POST['emailAddress'],
			'contactNo' => $_POST['contactNo'],
			'homeAddress' => $_POST['homeAddress'],
			'occupation' => $_POST['occupation'],
			'seminarDate' => $_POST['seminarDate'],
			'seminarVenue' => $_POST['seminarVenue'],
			'recruitedBy' => $_POST['recruitedBy'],
			'recruitedByPos' => $_POST['recruitedByPos'],
			'recruitedByDiv' => $_POST['recruitedByDiv'],
			'trainorName' => $_POST['trainorName'],
			'salesDirector' => $_POST['salesDirector'],
			'divManager' => $_POST['divManager'],
			'occ_name' => $_POST['occ_name'],
			'occ_pos' => $_POST['occ_pos'],
			'occ_from' => $_POST['occ_from'],
			'occ_to' => $_POST['occ_to'],
			'educSchool' => $_POST['educSchool'],
			'educLocation' => $_POST['educLocation'],
			'educAttainment' => $_POST['educAttainment'],
			'educYearFrom' => $_POST['educYearFrom'],
			'educYearTo' => $_POST['educYearTo'],
			'accountCreate' => 'Encode',
			'divisionId' => $_POST['recruitedByDiv']
		);

		$result = $user->insertUserData($params);
		if($result){
			$user->redirect('empEncode.php');
		} else {
			$user->redirect('empEncode.php');
		}

	} else if (isset($_POST['newUser'])) {
		$user = new User();

		$params = array(
			'userId' => $_POST['userId'],
			'idUser' => $_POST['userId'],
			'userType' => $_POST['userType'],
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'firstName' => $_POST['firstName'],
			'middleName' => $_POST['middleName'],
			'lastName' => $_POST['lastName'],
			'nickName' => $_POST['nickName'],
			'DOB' => $_POST['DOB'],
			'Age' => $_POST['Age'],
			'gender' => $_POST['gender'],
			'tinNumber' => $_POST['tinNumber'],
			'emailAddress' => $_POST['emailAddress'],
			'contactNo' => $_POST['contactNo'],
			'homeAddress' => $_POST['homeAddress'],
			'occupation' => $_POST['occupation'],
			'seminarDate' => $_POST['seminarDate'],
			'seminarVenue' => $_POST['seminarVenue'],
			'recruitedBy' => $_POST['recruitedBy'],
			'recruitedByPos' => $_POST['recruitedByPos'],
			'recruitedByDiv' => $_POST['recruitedByDiv'],
			'trainorName' => $_POST['trainorName'],
			'salesDirector' => $_POST['salesDirector'],
			'divManager' => $_POST['divManager'],
			'occ_name' => $_POST['occ_name'],
			'occ_pos' => $_POST['occ_pos'],
			'occ_from' => $_POST['occ_from'],
			'occ_to' => $_POST['occ_to'],
			'educSchool' => $_POST['educSchool'],
			'educLocation' => $_POST['educLocation'],
			'educAttainment' => $_POST['educAttainment'],
			'educYearFrom' => $_POST['educYearFrom'],
			'educYearTo' => $_POST['educYearTo'],
			'accountCreate' => 'Encode',
			'divisionId' => $_POST['recruitedByDiv'],
			'userstatus' => $_POST['userstatus']
		);

		$result = $user->insertUserDatabyAdmin($params);
		if($result){
			$user->redirect('adminMaintenance.php');
		} else {
			$user->redirect('adminMaintenance.php');
		}
	}

	if(isset($_POST['login'])){
		$user = new User();
		$result = $user->login($_POST['employeeuser'],$_POST['password']);
		if($result){
			$user->redirect('index.php');
		} else {
			$user->redirect('login.php');
		}
	}

	if(isset($_POST['salesClosing'])){
		$user = new User();

		$params = array(
			'familyName' => $_POST['familyname'],
			'firstName' => $_POST['firstname'],
			'phoneNo' => $_POST['phoneno'],
	        'telno' => $_POST['telno'],
	        'subdivision' => $_POST['subdivision'],
	        'location' => $_POST['location'],
	        'developer' => $_POST['developer'],
	        'phase' => $_POST['phase'],
	        'block' => $_POST['block'],
	        'lot' => $_POST['lot'],
	        'housemodel' => $_POST['housemodel'],
	        'floorarea' => $_POST['floorarea'],
	        'lotarea' => $_POST['lotarea'],
	        'tcpdev' => $_POST['tcpdev'],
	        'reservationfee' => $_POST['reservationfee'],
	        'oipr' => $_POST['oipr'],
	        'division' => $_POST['division'],
	        'divisionmanager' => $_POST['divisionmanager'],
	        'divcomrate' => $_POST['divcomrate'],
	        'salesdirector' => $_POST['salesdirector'],
	        'dircomrate' => $_POST['dircomrate'],
	        'salestrainee' => $_POST['salestrainee'],
	        'salescomrate' => $_POST['salescomrate'],
	        'rcdiv' => $_POST['rcdiv'],
	        'rcdivcomrate' => $_POST['rcdivcomrate'],
	        'rcdir' => $_POST['rcdir'],
	        'rcdircomrate' => $_POST['rcdircomrate'],
	        'rcsales' => $_POST['rcsales'],
	        'rcsalescomrate' => $_POST['rcsalescomrate'],
	        'rfa' => $_POST['rfa'],
	        'rfacomrate' => $_POST['rfacomrate'],
	        'bookassist' => $_POST['bookassist'],
	        'bookcomrate' => $_POST['bookcomrate'],
	        'aassist' => $_POST['aassist'],
	        'acomrate' => $_POST['acomrate'],
	        'preparedBy' => $_POST['preparedBy'],
	        'closingdate' => $_POST['closingDate'],
	        'dateCreated' => date('Y-m-d'),
	        'user' => $_POST['userId'],
	        'status' => 'For Approval'
	    );

	    $result = $user->insertClosingData($params);
	    if($result){
	    	$user->redirect('salesEncode.php');
	    } else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
	    	$user->redirect('salesEncode.php');
	    }
	}

	if(isset($_POST['dirClosing'])){
		$user = new User();

		$params = array(
			'familyName' => $_POST['familyname'],
			'firstName' => $_POST['firstname'],
			'phoneNo' => $_POST['phoneno'],
	        'telno' => $_POST['telno'],
	        'subdivision' => $_POST['subdivision'],
	        'location' => $_POST['location'],
	        'developer' => $_POST['developer'],
	        'phase' => $_POST['phase'],
	        'block' => $_POST['block'],
	        'lot' => $_POST['lot'],
	        'housemodel' => $_POST['housemodel'],
	        'floorarea' => $_POST['floorarea'],
	        'lotarea' => $_POST['lotarea'],
	        'tcpdev' => $_POST['tcpdev'],
	        'reservationfee' => $_POST['reservationfee'],
	        'oipr' => $_POST['oipr'],
	        'division' => $_POST['division'],
	        'divisionmanager' => $_POST['divisionmanager'],
	        'divcomrate' => $_POST['divcomrate'],
	        'salesdirector' => $_POST['salesdirector'],
	        'dircomrate' => $_POST['dircomrate'],
	        'salestrainee' => $_POST['salestrainee'],
	        'salescomrate' => $_POST['salescomrate'],
	        'rcdiv' => $_POST['rcdiv'],
	        'rcdivcomrate' => $_POST['rcdivcomrate'],
	        'rcdir' => $_POST['rcdir'],
	        'rcdircomrate' => $_POST['rcdircomrate'],
	        'rcsales' => $_POST['rcsales'],
	        'rcsalescomrate' => $_POST['rcsalescomrate'],
	        'rfa' => $_POST['rfa'],
	        'rfacomrate' => $_POST['rfacomrate'],
	        'bookassist' => $_POST['bookassist'],
	        'bookcomrate' => $_POST['bookcomrate'],
	        'aassist' => $_POST['aassist'],
	        'acomrate' => $_POST['acomrate'],
	        'preparedBy' => $_POST['preparedBy'],
	        'closingdate' => $_POST['closingDate'],
	        'dateCreated' => date('Y-m-d'),
	        'user' => $_POST['userId'],
	        'status' => 'For Approval'
	    );

	    $result = $user->insertClosingData($params);
	    if($result){
	    	$user->redirect('dirEncode.php');
	    } else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
	    	$user->redirect('dirEncode.php');
	    }
	}


	if(isset($_POST['divClosing'])){
		$user = new User();

		$params = array(
			'familyName' => $_POST['familyname'],
			'firstName' => $_POST['firstname'],
			'phoneNo' => $_POST['phoneno'],
	        'telno' => $_POST['telno'],
	        'subdivision' => $_POST['subdivision'],
	        'location' => $_POST['location'],
	        'developer' => $_POST['developer'],
	        'phase' => $_POST['phase'],
	        'block' => $_POST['block'],
	        'lot' => $_POST['lot'],
	        'housemodel' => $_POST['housemodel'],
	        'floorarea' => $_POST['floorarea'],
	        'lotarea' => $_POST['lotarea'],
	        'tcpdev' => $_POST['tcpdev'],
	        'reservationfee' => $_POST['reservationfee'],
	        'oipr' => $_POST['oipr'],
	        'division' => $_POST['division'],
	        'divisionmanager' => $_POST['divisionmanager'],
	        'divcomrate' => $_POST['divcomrate'],
	        'salesdirector' => $_POST['salesdirector'],
	        'dircomrate' => $_POST['dircomrate'],
	        'salestrainee' => $_POST['salestrainee'],
	        'salescomrate' => $_POST['salescomrate'],
	        'rcdiv' => $_POST['rcdiv'],
	        'rcdivcomrate' => $_POST['rcdivcomrate'],
	        'rcdir' => $_POST['rcdir'],
	        'rcdircomrate' => $_POST['rcdircomrate'],
	        'rcsales' => $_POST['rcsales'],
	        'rcsalescomrate' => $_POST['rcsalescomrate'],
	        'rfa' => $_POST['rfa'],
	        'rfacomrate' => $_POST['rfacomrate'],
	        'bookassist' => $_POST['bookassist'],
	        'bookcomrate' => $_POST['bookcomrate'],
	        'aassist' => $_POST['aassist'],
	        'acomrate' => $_POST['acomrate'],
	        'preparedBy' => $_POST['preparedBy'],
	        'closingdate' => $_POST['closingDate'],
	        'dateCreated' => date('Y-m-d'),
	        'user' => $_POST['userId'],
	        'status' => 'For Approval'
	    );

	    $result = $user->insertClosingData($params);
	    if($result){
	    	$user->redirect('divEncode.php');
	    } else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
	    	$user->redirect('divEncode.php');
	    }
	}

	// if(isset($_POST['closing'])){
	// 	$user = new User();

	// 	$params = array(
	// 		'familyName' => $_POST['familyname'],
	// 		'firstName' => $_POST['firstname'],
	// 		'phoneNo' => $_POST['phoneno'],
	//         'telno' => $_POST['telno'],
	//         'subdivision' => $_POST['subdivision'],
	//         'location' => $_POST['location'],
	//         'developer' => $_POST['developer'],
	//         'phase' => $_POST['phase'],
	//         'block' => $_POST['block'],
	//         'lot' => $_POST['lot'],
	//         'housemodel' => $_POST['housemodel'],
	//         'floorarea' => $_POST['floorarea'],
	//         'lotarea' => $_POST['lotarea'],
	//         'tcpdev' => $_POST['tcpdev'],
	//         'reservationfee' => $_POST['reservationfee'],
	//         'oipr' => $_POST['oipr'],
	//         'division' => $_POST['division'],
	//         'divisionmanager' => $_POST['divisionmanager'],
	//         'divcomrate' => $_POST['divcomrate'],
	//         'salesdirector' => $_POST['salesdirector'],
	//         'dircomrate' => $_POST['dircomrate'],
	//         'salestrainee' => $_POST['salestrainee'],
	//         'salescomrate' => $_POST['salescomrate'],
	//         'rcdiv' => $_POST['rcdiv'],
	//         'rcdivcomrate' => $_POST['rcdivcomrate'],
	//         'rcdir' => $_POST['rcdir'],
	//         'rcdircomrate' => $_POST['rcdircomrate'],
	//         'rcsales' => $_POST['rcsales'],
	//         'rcsalescomrate' => $_POST['rcsalescomrate'],
	//         'rfa' => $_POST['rfa'],
	//         'rfacomrate' => $_POST['rfacomrate'],
	//         'bookassist' => $_POST['bookassist'],
	//         'bookcomrate' => $_POST['bookcomrate'],
	//         'aassist' => $_POST['aassist'],
	//         'acomrate' => $_POST['acomrate'],
	//         'preparedBy' => $_POST['preparedBy'],
	//         'closingdate' => $_POST['closingDate'],
	//         'dateCreated' => date('Y-m-d'),
	//         'user' => $_POST['userId']
	//     );

	//     $result = $user->insertClosingData($params);
	//     if($result){
	//     	$user->redirect('empEncode.php');
	//     } else {
	//     	$_SESSION['Message'] = "Something went wrong please try again.";
	//     	$_SESSION['MsgCode'] = 2;
	//     	$user->redirect('empEncode.php');
	//     }
	// }

	if(isset($_POST['updateclosing'])){
		$user = new User();
		

		$params = array(
			'salesId' => $_POST['salesId'],
			'familyName' => $_POST['familyname'],
			'firstName' => $_POST['firstname'],
			'phoneNo' => $_POST['phoneno'],
	        'telno' => $_POST['telno'],
	        'subdivision' => $_POST['subdivision'],
	        'location' => $_POST['location'],
	        'developer' => $_POST['developer'],
	        'phase' => $_POST['phase'],
	        'block' => $_POST['block'],
	        'lot' => $_POST['lot'],
	        'housemodel' => $_POST['housemodel'],
	        'floorarea' => $_POST['floorarea'],
	        'lotarea' => $_POST['lotarea'],
	        'tcpdev' => $_POST['tcpdev'],
	        'reservationfee' => $_POST['reservationfee'],
	        'oipr' => $_POST['oipr'],
	        'division' => $_POST['division'],
	        'divisionmanager' => $_POST['divisionmanager'],
	        'divcomrate' => $_POST['divcomrate'],
	        'salesdirector' => $_POST['salesdirector'],
	        'dircomrate' => $_POST['dircomrate'],
	        'salestrainee' => $_POST['salestrainee'],
	        'salescomrate' => $_POST['salescomrate'],
	        'rcdiv' => $_POST['rcdiv'],
	        'rcdivcomrate' => $_POST['rcdivcomrate'],
	        'rcdir' => $_POST['rcdir'],
	        'rcdircomrate' => $_POST['rcdircomrate'],
	        'rcsales' => $_POST['rcsales'],
	        'rcsalescomrate' => $_POST['rcsalescomrate'],
	        'rfa' => $_POST['rfa'],
	        'rfacomrate' => $_POST['rfacomrate'],
	        'bookassist' => $_POST['bookassist'],
	        'bookcomrate' => $_POST['bookcomrate'],
	        'aassist' => $_POST['aassist'],
	        'acomrate' => $_POST['acomrate'],
	        'closingdate' => $_POST['closingDate']
	    );

	    $result = $user->updateClosingData($params);
	    if($result){
	    	$user->redirect('adminMaintenance.php');
	    } else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
	    	$user->redirect('adminMaintenance.php');
	    }
	}

	if(isset($_POST['approvalAccount'])){
		$user = new User();

		if($_POST['status']=='Active'){
		$name = $_POST['fullnameuser'];
		$to = $_POST['useremail'];
		$subject = 'Account Activated';
		$message = '
			<html>
			<head>
			  <title>Account Activation</title>
			</head>
			<body>
			  <h4>Hello! Mr/Ms. ' . $name . '</h4>
			  <p>Your Account has successfully approved!</p>
			  <p>You can now login by clicking the link below</p>
			 https://dreamhouserealty.000webhostapp.com/website/login.php
			</body>
			</html>
			';
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=iso-8859-1';

		$headers[] = 'From: Dream House Realty <email@example.com>';
		mail($to, $subject, $message, implode("\r\n", $headers));
		}


		$params = array(
			'userId' => $_POST['salesId'],
			'status' => $_POST['status']
		);

		$result = $user->updateStatus($params);
		if($result){
			$user->redirect('empApproval.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empApproval.php');
		}
	}

	if(isset($_POST['reset'])){
		$user = new User();

		$email = $_POST['email'];

		$result = $user->getUserbyEmailAdd($email);
		if($result){
			$_SESSION['Message'] = "Please check your email <span>$email</span>"
        . " for a confirmation link to complete your password reset!";
			$_SESSION['MsgCode'] = 1;
			$user->redirect('forgotpassword.php');
		} else {
			$_SESSION['Message'] = "User with that email doesn't exist!";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('forgotpassword.php');
		}
    }

    if(isset($_POST['setpassword'])){
    	$user = new User();

    	$params = array(
    		'newpassword' => $_POST['newpassword'],
    		'email' => $_POST['email']
    	);

    	if ( $_POST['newpassword'] == $_POST['confirmpassword'] ) {


    		$result = $user->UpdatePasswordbyEmail($params);
    		if($result){
				$user->redirect('login.php');
			} else {
				$_SESSION['Message'] = "Something went wrong please try again.";
				$_SESSION['MsgCode'] = 2;
				$user->redirect('login.php');
			}


    	}else{
    		$_SESSION['message'] = "Two passwords you entered don't match, try again!";
    		$_SESSION['MsgCode'] = 2;
			$user->redirect('forgotpassword.php');
   		}
	}

	if(isset($_POST['adminapprovalAccount'])){
		$user = new User();

		if($_POST['status']=='Active'){
			$name = $_POST['fullnameuser'];
			$to = $_POST['emailuser'];
			$subject = 'Message Feedback';
			$message = '
				<html>
				<head>
				  <title>Account Activation</title>
				</head>
				<body>
				  <h4>Hello! Mr/Ms. ' . $name . '</h4>
				  <p>Your Account has successfully approved!</p>
				  <p>You can now login by clicking the link below</p>
				  https://dreamhouserealty.000webhostapp.com/website/login.php
				</body>
				</html>
				';
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';

			$headers[] = 'From: Dream House Realty <email@example.com>';
			mail($to, $subject, $message, implode("\r\n", $headers));
		}

		$params = array(
			'userId' => $_POST['salesId'],
			'status' => $_POST['status']
		);

		$result = $user->updateStatus($params);
		if($result){
			$user->redirect('adminApproval.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('adminApproval.php');
		}
	}

	// if(isset($_POST['search'])){
	// 	$user = new User();

	// 	$params = array(
	// 		'houseName' => $_POST['search'],
	// 		'location' => $_POST['search']
	// 	);

	// 	$result = $user->SearchbyName($params);
	// 	if($result){
	// 		$user->redirect('search.php');
	// 	} else {
	// 		$_SESSION['Message'] = "Something went wrong please try again.";
	// 		$_SESSION['MsgCode'] = 2;
	// 		$user->redirect('search.php');
	// 	}
	// }

	if(isset($_POST['updateSeminar'])){
    $user = new User();

    $params = array(
      'seminarId' => $_POST['seminarId'],
      'seminarName' => $_POST['seminarName'],
      'seminarDate' => $_POST['seminarDate'],
      'seminarTimeStart' => $_POST['seminarTimeStart'],
      'seminarTimeEnd' => $_POST['seminarTimeEnd'],
      'seminarLocation' => $_POST['seminarLocation']
    );

    $result = $user->updateSeminarbyId($params);
    if($result){
      $user->redirect('adminMaintenance.php');
    } else {
      $_SESSION['Message'] = "Something went wrong please try again.";
      $_SESSION['MsgCode'] = 2;
      $user->redirect('adminMaintenance.php');
    }
  }


	if(isset($_POST['updateUser'])){
    $user = new User();

    $params = array(
      'userId' => $_POST['userId'],
      'usersId' => $_POST['idUser'],
      'userDetails' => $_POST['userId'],
      'detailsUser' => $_POST['userId'],
      'firstName' => $_POST['firstName'],
      'middleName' => $_POST['middleName'],
      'lastName' => $_POST['lastName'],
      // 'spouseName' => $_POST['spouseName'],
      'nickName' => $_POST['nickName'],
      'DOB' => $_POST['DOB'],
      'Age' => $_POST['Age'],
      'gender' => $_POST['gender'],
      'tinNumber' => $_POST['tinNumber'],
      'emailAddress' => $_POST['emailAddress'],
      'contactNo' => $_POST['contactNo'],
      'homeAddress' => $_POST['homeAddress'],
      'username' => $_POST['username'],
      'educSchool' => $_POST['educSchool'],
      'educLocation' => $_POST['educLocation'],
      'educAttainment' => $_POST['educAttainment'],
      'educYearFrom' => $_POST['educYearFrom'],
      'educYearTo' => $_POST['educYearTo'],
      'educId' => $_POST['educId']
    );

    $result = $user->updateUserbyId($params);
    if($result){
      $user->redirect('adminMaintenance.php');
    } else {
      $_SESSION['Message'] = "Something went wrong please try again.";
      $_SESSION['MsgCode'] = 2;
      $user->redirect('adminMaintenance.php');
    }
  }

  if(isset($_POST['adminupdateHouse'])){
    $user = new User();

    $params = array(
      'houseprojId' => $_POST['houseprojId'],
      'houseName' => $_POST['houseName'],
      'description' => $_POST['description'],
      'finish' => $_POST['finish'],
      'finishId' => $_POST['finishId'],
      'finishhouseprojId' => $_POST['finishhouseprojId'],
      'location' => $_POST['location'],
      'promos' => $_POST['promos'],
      'subd' => $_POST['subd'],
      $_FILES['houseImageFile']['tmp_name'] = $_POST['houseImageFile'],
      $_FILES['houseImageFile']['name'] = $_POST['houseImageFile'],
       $_FILES['houseImageFile']['size'] = $_POST['houseImageFile']
    );

    if (count($_FILES['houseImageFile']['tmp_name']) > 0) {
      for ($x = 0; $x < count($_FILES['houseImageFile']['tmp_name']); $x++) {
        $file_name = $_FILES['houseImageFile']['name'][$x];
        $file_size = $_FILES['houseImageFile']['size'][$x];
        $file_tmp  = $_FILES['houseImageFile']['tmp_name'][$x];

          $t = explode(".", $file_name);
          $t1 = end($t);
          $file_ext = strtolower(end($t));

          $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
          if(in_array($file_ext,$allowExt)) {
            $fileTmp = $file_tmp;
            $file_path = "uploads/" . $file_name;
            move_uploaded_file($fileTmp, $file_path);

            $imageParamHouse = array(
              'imagePath' => $file_path,
              'imageId' => $_POST['imageId']
            );

            $user->updateImagesHouse($imageParamHouse);
          }
      }
    }

    $result = $user->updateHousebyId($params);
    if($result){
      $user->redirect('adminMaintenance.php');
    } else {
      $_SESSION['Message'] = "Something went wrong please try again.";
      $_SESSION['MsgCode'] = 2;
      $user->redirect('adminMaintenance.php');
    }
  }

	if(isset($_POST['updateHouse'])){
		$user = new User();

    $params = array(
      'houseprojId' => $_POST['houseprojId'],
      'houseName' => $_POST['houseName'],
      'description' => $_POST['description'],
      'finish' => $_POST['finish'],
      'finishId' => $_POST['finishId'],
      'finishhouseprojId' => $_POST['finishhouseprojId'],
      'location' => $_POST['location'],
      'promos' => $_POST['promos'],
      'subd' => $_POST['subd'],
      $_FILES['houseImageFile']['tmp_name'] = $_POST['houseImageFile'],
      $_FILES['houseImageFile']['name'] = $_POST['houseImageFile'],
       $_FILES['houseImageFile']['size'] = $_POST['houseImageFile']
    );

    if (count($_FILES['houseImageFile']['tmp_name']) > 0) {
      for ($x = 0; $x < count($_FILES['houseImageFile']['tmp_name']); $x++) {
        $file_name = $_FILES['houseImageFile']['name'][$x];
        $file_size = $_FILES['houseImageFile']['size'][$x];
        $file_tmp  = $_FILES['houseImageFile']['tmp_name'][$x];

          $t = explode(".", $file_name);
          $t1 = end($t);
          $file_ext = strtolower(end($t));

          $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
          if(in_array($file_ext,$allowExt)) {
            $fileTmp = $file_tmp;
            $file_path = "uploads/" . $file_name;
            move_uploaded_file($fileTmp, $file_path);

            $imageParamHouse = array(
              'imagePath' => $file_path,
              'imageId' => $_POST['imageId']
            );

            $user->updateImagesHouse($imageParamHouse);
          }
      }
    }

    $result = $user->updateHousebyId($params);
		if($result){
			$user->redirect('empMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empMaintenance.php');
		}
	}

	if(isset($_POST['updateNews'])){
		$user = new User();

		$params = array(
			'newsId' => $_POST['newsId'],
			'title' => $_POST['title'],
			'content' => $_POST['content'],
			$_FILES['newsImageFile']['tmp_name'] = $_POST['newsImageFile'],
      	$_FILES['newsImageFile']['name'] = $_POST['newsImageFile'],
       	$_FILES['newsImageFile']['size'] = $_POST['newsImageFile']
    );

    if (count($_FILES['newsImageFile']['tmp_name']) > 0) {
      for ($x = 0; $x < count($_FILES['newsImageFile']['tmp_name']); $x++) {
        $file_name = $_FILES['newsImageFile']['name'][$x];
        $file_size = $_FILES['newsImageFile']['size'][$x];
        $file_tmp  = $_FILES['newsImageFile']['tmp_name'][$x];

          $t = explode(".", $file_name);
          $t1 = end($t);
          $file_ext = strtolower(end($t));

          $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
          if(in_array($file_ext,$allowExt)) {
            $fileTmp = $file_tmp;
            $file_path = "uploads/" . $file_name;
            move_uploaded_file($fileTmp, $file_path);

            $imageParamHouse = array(
              'imagePath' => $file_path,
              'imageId' => $_POST['imageId']
            );

            $user->updateImagesNews($imageParamHouse);
          }
      }
    }

		$result = $user->updateNewsbyId($params);
		if($result){
			$user->redirect('empMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empMaintenance.php');
		}
	}

	if(isset($_POST['addNewHouseAdmin'])){
		$user = new User();

		$params = array(
			'houseName' => $_POST['houseName'],
			'houseprice' => $_POST['houseprice'],
			'description' => $_POST['description'],
			'finish' => $_POST['finish'],
			'location' => $_POST['location'],
			'promos' => $_POST['promos'],
			'subd' => $_POST['subd'],
			'userId' => $_POST['userId']
		);

		if (count($_FILES['houseImageFile']['tmp_name']) > 0) {
			for ($x = 0; $x < count($_FILES['houseImageFile']['tmp_name']); $x++) {
				$file_name = $_FILES['houseImageFile']['name'][$x];
				$file_size = $_FILES['houseImageFile']['size'][$x];
				$file_tmp  = $_FILES['houseImageFile']['tmp_name'][$x];

			    $t = explode(".", $file_name);
			    $t1 = end($t);
			    $file_ext = strtolower(end($t));

			    $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
			    if(in_array($file_ext,$allowExt)) {
			    	$fileTmp = $file_tmp;
			    	$file_path = "uploads/" . $file_name;
			    	move_uploaded_file($fileTmp, $file_path);

			    	$imageParamHouse = array(
			    		'imagePath' => $file_path,
			    		'isHouseSample' => 1,
			    		'isFloorPlan' => 0
			    	);

			    	$user->insertImages($imageParamHouse);
			    }
			}
		}

		if (count($_FILES['floorPlanImgFile']['tmp_name']) > 0) {
			for ($x = 0; $x < count($_FILES['floorPlanImgFile']['tmp_name']); $x++) {
				$file_name = $_FILES['floorPlanImgFile']['name'][$x];
				$file_size = $_FILES['floorPlanImgFile']['size'][$x];
				$file_tmp  = $_FILES['floorPlanImgFile']['tmp_name'][$x];

			    $t = explode(".", $file_name);
			    $t1 = end($t);
			    $file_ext = strtolower(end($t));

			    $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
			    if(in_array($file_ext,$allowExt)) {
			    	$fileTmp = $file_tmp;
			    	$file_path = "uploads/" . $file_name;
			    	move_uploaded_file($fileTmp, $file_path);

			    	$imageParamFloor = array(
			    		'imagePath' => $file_path,
			    		'isHouseSample' => 0,
			    		'isFloorPlan' => 1
			    	);

			    	$user->insertImages($imageParamFloor);
			    }
			}
		}

		$result = $user->insertHouseProject($params);
		if($result){
			$user->redirect('adminMaintenance.php');
			$_SESSION['MsgCode'] = 0;
		} else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('adminMaintenance.php');
		}
	}

	if(isset($_POST['addNewHouse'])){
		$user = new User();

		$params = array(
			'houseName' => $_POST['houseName'],
			'houseprice' => $_POST['houseprice'],
			'description' => $_POST['description'],
			'finish' => $_POST['finish'],
			'location' => $_POST['location'],
			'promos' => $_POST['promos'],
			'subd' => $_POST['subd'],
			'userId' => $_POST['userId']
		);

		if (count($_FILES['houseImageFile']['tmp_name']) > 0) {
			for ($x = 0; $x < count($_FILES['houseImageFile']['tmp_name']); $x++) {
				$file_name = $_FILES['houseImageFile']['name'][$x];
				$file_size = $_FILES['houseImageFile']['size'][$x];
				$file_tmp  = $_FILES['houseImageFile']['tmp_name'][$x];

			    $t = explode(".", $file_name);
			    $t1 = end($t);
			    $file_ext = strtolower(end($t));

			    $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
			    if(in_array($file_ext,$allowExt)) {
			    	$fileTmp = $file_tmp;
			    	$file_path = "uploads/" . $file_name;
			    	move_uploaded_file($fileTmp, $file_path);

			    	$imageParamHouse = array(
			    		'imagePath' => $file_path,
			    		'isHouseSample' => 1,
			    		'isFloorPlan' => 0
			    	);

			    	$user->insertImages($imageParamHouse);
			    }
			}
		}

		if (count($_FILES['floorPlanImgFile']['tmp_name']) > 0) {
			for ($x = 0; $x < count($_FILES['floorPlanImgFile']['tmp_name']); $x++) {
				$file_name = $_FILES['floorPlanImgFile']['name'][$x];
				$file_size = $_FILES['floorPlanImgFile']['size'][$x];
				$file_tmp  = $_FILES['floorPlanImgFile']['tmp_name'][$x];

			    $t = explode(".", $file_name);
			    $t1 = end($t);
			    $file_ext = strtolower(end($t));

			    $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
			    if(in_array($file_ext,$allowExt)) {
			    	$fileTmp = $file_tmp;
			    	$file_path = "uploads/" . $file_name;
			    	move_uploaded_file($fileTmp, $file_path);

			    	$imageParamFloor = array(
			    		'imagePath' => $file_path,
			    		'isHouseSample' => 0,
			    		'isFloorPlan' => 1
			    	);

			    	$user->insertImages($imageParamFloor);
			    }
			}
		}

		$result = $user->insertHouseProject($params);
		if($result){
			$user->redirect('empEncode.php');
			$_SESSION['MsgCode'] = 0;
		} else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('empEncode.php');
		}
	}

	if(isset($_POST['addNews'])){
		$user = new User();
		$params = array(
			'title' => $_POST['title'],
			'content' => $_POST['content'],
            'userId' => $_POST['userId']
        );

		if (count($_FILES['newsImgFile']['tmp_name']) > 0) {
			for ($x = 0; $x < count($_FILES['newsImgFile']['tmp_name']); $x++) {
				$file_name = $_FILES['newsImgFile']['name'][$x];
				$file_size = $_FILES['newsImgFile']['size'][$x];
				$file_tmp  = $_FILES['newsImgFile']['tmp_name'][$x];

			    $t = explode(".", $file_name);
			    $t1 = end($t);
			    $file_ext = strtolower(end($t));

			    $allowExt = array("jpg", "jpeg", "png", "gif", "bmp");
			    if(in_array($file_ext,$allowExt)) {
			    	$fileTmp = $file_tmp;
			    	$file_path = "uploads/" . $file_name;
			    	move_uploaded_file($fileTmp, $file_path);

			    	$imageParamNews = array(
			    		'imagePath' => $file_path,
			    		'isNews' => 1
			    	);

			    	$user->insertImages($imageParamNews, 'News');
			    }
			}
		}

		$result = $user->insertNews($params);
		if($result){
			$user->redirect('empEncode.php');
			$_SESSION['MsgCode'] = 0;
		} else {
	    	$_SESSION['Message'] = "Something went wrong please try again.";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('empEncode.php');
		}
	}

	if(isset($_POST['requestNewSeminar'])){
		$user = new User();

		$semName = $_POST['seminarName'];
		$semDate = $_POST['seminarDate'];
		$start = $_POST['seminarTimeStart'];
		$end = $_POST['seminarTimeEnd'];
		$Ymd = date('Y-m-d'); 
		$tstart = strtotime("$Ymd $start");
		$tend = strtotime("$Ymd $end");

		$validate = $user->validateSeminar($semName,$semDate,$start,$end);
		if($validate == 'true'){
			$_SESSION['Message'] = "Seminar Schedule already exist!";
	     	$_SESSION['MsgCode'] = 2;
		 	$user->redirect('divRequestSched.php');

		} elseif($tstart >= $tend) {
			$_SESSION['Message'] = "Invalid Seminar Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');

		}else{
				$params = array(
				'seminarName' => $_POST['seminarName'],
				'seminarDate' => $_POST['seminarDate'],
				'timeStart' => $_POST['seminarTimeStart'],
				'timeEnd' => $_POST['seminarTimeEnd'],
				'seminarLocation' => $_POST['seminarLocation'],
				'userId' => $_POST['userId']
			);
				$_SESSION['Message'] = "Invalid Seminar Time";
				$_SESSION['MsgCode'] = 0;
				$user->redirect('divRequestSched.php');

				$result = $user->insertRequestSeminar($params);
				if($result){
					$user->redirect('divRequestSched.php');
					$_SESSION['MsgCode'] = 0;
				} else {
			    	$_SESSION['Message'] = "Something went wrong please try again.";
			    	$_SESSION['MsgCode'] = 2;
					$user->redirect('divRequestSched.php');
				}	

		}
	}

	if(isset($_POST['requestNewTrip'])){
		$user = new User();

		$pickup = ($_POST['locationPickUpTime'] . ' - ' . $_POST['locationPickUpTime1']);
		$destination = ($_POST['locationDestinationTime'] . ' - ' . $_POST['locationDestinationTime1']);
		$Ymd = date('Y-m-d'); 
		$pstart = strtotime("$Ymd ".$_POST['locationPickUpTime']."");
		$pend = strtotime("$Ymd" .$_POST['locationPickUpTime1']."");
		$dstart = strtotime("$Ymd ".$_POST['locationDestinationTime']."");
		$dend = strtotime("$Ymd" .$_POST['locationDestinationTime1']."");

		$validate = $user->validateTripping($_POST['tripDate'],$pickup,$destination);
		$valdriver = $user->validateTrippingDriver($_POST['tripDate'],$pickup,$destination,$_POST['driverName']);
		$valvehicle = $user->validateTrippingVehicle($_POST['tripDate'],$pickup,$destination,$_POST['plateNo']);

		if($pstart > $pend) {
			$_SESSION['Message'] = "Invalid Pick Up Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else if($dstart > $dend){
			$_SESSION['Message'] = "Invalid Destination Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else if($pend >= $dstart){
			$_SESSION['Message'] = "Invalid Time for end of Pick Up Time and Start of Destination Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else if($valdriver == true){
			$_SESSION['Message'] = "The Driver has already scheduled on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else if($valvehicle == true){
			$_SESSION['Message'] = "Vehicle already have a schedule on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else if($validate == true){
			$_SESSION['Message'] = "Invalid Vehicle Tripping Schedule! Schedule already exist";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');
		}else{

		$params = array(
			'tripDate' => $_POST['tripDate'],
			'locationPickUp' => $_POST['locationPickUp'],
			'locationPickUpTime' => $pickup,
			'locationDestination' => $_POST['locationDestination'],
			'locationDestinationTime' => $destination,
			'driverName' => $_POST['driverName'],
			'plateNo' => $_POST['plateNo'],
			'contactNo' => $_POST['contactNo'],
			'userId' => $_POST['userId']
		);
			$result = $user->insertRequestVehicleTrip($params);
			if($result){
				$user->redirect('divRequestSched.php');
				$_SESSION['MsgCode'] = 0;
			} else {
	    		$_SESSION['Message'] = "Something went wrong please try again.";
	    		$_SESSION['MsgCode'] = 2;
				$user->redirect('divRequestSched.php');
			}
		}
		
	}


	if(isset($_POST['requestNewSeminarDir'])){
		$user = new User();

		$semName = $_POST['seminarName'];
		$semDate = $_POST['seminarDate'];
		$start = $_POST['seminarTimeStart'];
		$end = $_POST['seminarTimeEnd'];
		$Ymd = date('Y-m-d'); 
		$tstart = strtotime("$Ymd $start");
		$tend = strtotime("$Ymd $end");

		$validate = $user->validateSeminar($semName,$semDate,$start,$end);
		if($validate == 'true'){
			$_SESSION['Message'] = "Seminar Schedule already exist!";
	     	$_SESSION['MsgCode'] = 2;
		 	$user->redirect('dirRequestSched.php');

		} elseif($tstart >= $tend) {
			$_SESSION['Message'] = "Invalid Seminar Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');

		}else{
				$params = array(
				'seminarName' => $_POST['seminarName'],
				'seminarDate' => $_POST['seminarDate'],
				'timeStart' => $_POST['seminarTimeStart'],
				'timeEnd' => $_POST['seminarTimeEnd'],
				'seminarLocation' => $_POST['seminarLocation'],
				'userId' => $_POST['userId']
			);
				$_SESSION['Message'] = "Invalid Seminar Time";
				$_SESSION['MsgCode'] = 0;
				$user->redirect('dirRequestSched.php');

				$result = $user->insertRequestSeminar($params);
				if($result){
					$user->redirect('dirRequestSched.php');
					$_SESSION['MsgCode'] = 0;
				} else {
			    	$_SESSION['Message'] = "Something went wrong please try again.";
			    	$_SESSION['MsgCode'] = 2;
					$user->redirect('dirRequestSched.php');
				}	

		}
	}

	if(isset($_POST['requestNewTripDir'])){
		$user = new User();

		$pickup = ($_POST['locationPickUpTime'] . ' - ' . $_POST['locationPickUpTime1']);
		$destination = ($_POST['locationDestinationTime'] . ' - ' . $_POST['locationDestinationTime1']);
		$Ymd = date('Y-m-d'); 
		$pstart = strtotime("$Ymd ".$_POST['locationPickUpTime']."");
		$pend = strtotime("$Ymd" .$_POST['locationPickUpTime1']."");
		$dstart = strtotime("$Ymd ".$_POST['locationDestinationTime']."");
		$dend = strtotime("$Ymd" .$_POST['locationDestinationTime1']."");

		$validate = $user->validateTripping($_POST['tripDate'],$pickup,$destination);
		$valdriver = $user->validateTrippingDriver($_POST['tripDate'],$pickup,$destination,$_POST['driverName']);
		$valvehicle = $user->validateTrippingVehicle($_POST['tripDate'],$pickup,$destination,$_POST['plateNo']);

		if($pstart > $pend) {
			$_SESSION['Message'] = "Invalid Pick Up Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else if($dstart > $dend){
			$_SESSION['Message'] = "Invalid Destination Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else if($pend >= $dstart){
			$_SESSION['Message'] = "Invalid Time for end of Pick Up Time and Start of Destination Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else if($valdriver == true){
			$_SESSION['Message'] = "The Driver has already scheduled on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else if($valvehicle == true){
			$_SESSION['Message'] = "Vehicle already have a schedule on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else if($validate == true){
			$_SESSION['Message'] = "Invalid Vehicle Tripping Schedule! Schedule already exist";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('dirRequestSched.php');
		}else{

		$params = array(
			'tripDate' => $_POST['tripDate'],
			'locationPickUp' => $_POST['locationPickUp'],
			'locationPickUpTime' => $pickup,
			'locationDestination' => $_POST['locationDestination'],
			'locationDestinationTime' => $destination,
			'driverName' => $_POST['driverName'],
			'plateNo' => $_POST['plateNo'],
			'contactNo' => $_POST['contactNo'],
			'userId' => $_POST['userId']
		);
			$result = $user->insertRequestVehicleTrip($params);
			if($result){
				$user->redirect('dirRequestSched.php');
				$_SESSION['MsgCode'] = 0;
			} else {
	    		$_SESSION['Message'] = "Something went wrong please try again.";
	    		$_SESSION['MsgCode'] = 2;
				$user->redirect('dirRequestSched.php');
			}
		}
		
	}

	if(isset($_POST['requestNewTripSales'])){
		$user = new User();

		$pickup = ($_POST['locationPickUpTime'] . ' - ' . $_POST['locationPickUpTime1']);
		$destination = ($_POST['locationDestinationTime'] . ' - ' . $_POST['locationDestinationTime1']);
		$Ymd = date('Y-m-d'); 
		$pstart = strtotime("$Ymd ".$_POST['locationPickUpTime']."");
		$pend = strtotime("$Ymd" .$_POST['locationPickUpTime1']."");
		$dstart = strtotime("$Ymd ".$_POST['locationDestinationTime']."");
		$dend = strtotime("$Ymd" .$_POST['locationDestinationTime1']."");

		$validate = $user->validateTripping($_POST['tripDate'],$pickup,$destination);
		$valdriver = $user->validateTrippingDriver($_POST['tripDate'],$pickup,$destination,$_POST['driverName']);
		$valvehicle = $user->validateTrippingVehicle($_POST['tripDate'],$pickup,$destination,$_POST['plateNo']);

		if($pstart > $pend) {
			$_SESSION['Message'] = "Invalid Pick Up Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($dstart > $dend){
			$_SESSION['Message'] = "Invalid Destination Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($pend >= $dstart){
			$_SESSION['Message'] = "Invalid Time for end of Pick Up Time and Start of Destination Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($valdriver == true){
			$_SESSION['Message'] = "The Driver has already scheduled on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($valvehicle == true){
			$_SESSION['Message'] = "Vehicle already have a schedule on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($validate == true){
			$_SESSION['Message'] = "Invalid Vehicle Tripping Schedule! Schedule already exist";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else{

		$params = array(
			'tripDate' => $_POST['tripDate'],
			'locationPickUp' => $_POST['locationPickUp'],
			'locationPickUpTime' => $pickup,
			'locationDestination' => $_POST['locationDestination'],
			'locationDestinationTime' => $destination,
			'driverName' => $_POST['driverName'],
			'plateNo' => $_POST['plateNo'],
			'contactNo' => $_POST['contactNo'],
			'userId' => $_POST['userId']
		);
			$result = $user->insertRequestVehicleTrip($params);
			if($result){
				$user->redirect('salesRequestSched.php');
				$_SESSION['MsgCode'] = 0;
			} else {
	    		$_SESSION['Message'] = "Something went wrong please try again.";
	    		$_SESSION['MsgCode'] = 2;
				$user->redirect('salesRequestSched.php');
			}
		}
		
	}

	if(isset($_POST['NewSeminarAdmin'])){
		$user = new User();

		$semName = $_POST['seminarName'];
		$semDate = $_POST['seminarDate'];
		$start = $_POST['seminarTimeStart'];
		$end = $_POST['seminarTimeEnd'];
		$Ymd = date('Y-m-d'); 
		$tstart = strtotime("$Ymd $start");
		$tend = strtotime("$Ymd $end");

		$validate = $user->validateSeminar($semName,$semDate,$start,$end);
		if($validate == 'true'){
			$_SESSION['Message'] = "Seminar Schedule already exist!";
	     	$_SESSION['MsgCode'] = 2;
		 	$user->redirect('divRequestSched.php');

		} elseif($tstart >= $tend) {
			$_SESSION['Message'] = "Invalid Seminar Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('divRequestSched.php');

		}else{
				$params = array(
					'seminarName' => $_POST['seminarName'],
					'seminarDate' => $_POST['seminarDate'],
					'timeStart' => $_POST['seminarTimeStart'],
					'timeEnd' => $_POST['seminarTimeEnd'],
					'seminarLocation' => $_POST['seminarLocation'],
					'userId' => $_POST['userId'],
					'status' => $_POST['status']
				);

				$result = $user->insertSeminar($params);
				if($result){
					$user->redirect('adminSchedule.php');
					$_SESSION['MsgCode'] = 0;
				} else {
	    			$_SESSION['Message'] = "Something went wrong please try again.";
	    			$_SESSION['MsgCode'] = 2;
					$user->redirect('adminSchedule.php');
				}
		}
	}

	// 	$params = array(
	// 		'seminarName' => $_POST['seminarName'],
	// 		'seminarDate' => $_POST['seminarDate'],
	// 		'timeStart' => $_POST['seminarTimeStart'],
	// 		'timeEnd' => $_POST['seminarTimeEnd'],
	// 		'seminarLocation' => $_POST['seminarLocation'],
	// 		'userId' => $_POST['userId'],
	// 		'status' => $_POST['status']
	// 	);

	// 	$result = $user->insertSeminar($params);
	// 	if($result){
	// 		$user->redirect('adminSchedule.php');
	// 		$_SESSION['MsgCode'] = 0;
	// 	} else {
	//     	$_SESSION['Message'] = "Something went wrong please try again.";
	//     	$_SESSION['MsgCode'] = 2;
	// 		$user->redirect('adminSchedule.php');
	// 	}
	// }

	if(isset($_POST['NewTripAdmin'])){
		$user = new User();

		$pickup = ($_POST['locationPickUpTime'] . ' - ' . $_POST['locationPickUpTime1']);
		$destination = ($_POST['locationDestinationTime'] . ' - ' . $_POST['locationDestinationTime1']);
		$Ymd = date('Y-m-d'); 
		$pstart = strtotime("$Ymd ".$_POST['locationPickUpTime']."");
		$pend = strtotime("$Ymd" .$_POST['locationPickUpTime1']."");
		$dstart = strtotime("$Ymd ".$_POST['locationDestinationTime']."");
		$dend = strtotime("$Ymd" .$_POST['locationDestinationTime1']."");

		$validate = $user->validateTripping($_POST['tripDate'],$pickup,$destination);
		$valdriver = $user->validateTrippingDriver($_POST['tripDate'],$pickup,$destination,$_POST['driverName']);
		$valvehicle = $user->validateTrippingVehicle($_POST['tripDate'],$pickup,$destination,$_POST['plateNo']);

		if($pstart > $pend) {
			$_SESSION['Message'] = "Invalid Pick Up Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($dstart > $dend){
			$_SESSION['Message'] = "Invalid Destination Time Range! Must be equal or 30 minutes interval";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($pend >= $dstart){
			$_SESSION['Message'] = "Invalid Time for end of Pick Up Time and Start of Destination Time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($valdriver == true){
			$_SESSION['Message'] = "The Driver has already scheduled on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($valvehicle == true){
			$_SESSION['Message'] = "Vehicle already have a schedule on that day and time";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else if($validate == true){
			$_SESSION['Message'] = "Invalid Vehicle Tripping Schedule! Schedule already exist";
	    	$_SESSION['MsgCode'] = 2;
			$user->redirect('salesRequestSched.php');
		}else{

			$params = array(
				'tripDate' => $_POST['tripDate'],
				'locationPickUp' => $_POST['locationPickUp'],
				'locationPickUpTime' => $pickup,
				'locationDestination' => $_POST['locationDestination'],
				'locationDestinationTime' => $destination,
				'driverName' => $_POST['driverName'],
				'plateNo' => $_POST['plateNo'],
				'contactNo' => $_POST['contactNo'],
				'userId' => $_POST['userId'],
				'status' => $_POST['status']
			);

			$result = $user->insertVehicleTrip($params);
			if($result){
				$user->redirect('adminSchedule.php');
				$_SESSION['MsgCode'] = 0;
			} else {
		    	$_SESSION['Message'] = "Something went wrong please try again.";
		    	$_SESSION['MsgCode'] = 2;
				$user->redirect('adminSchedule.php');
			}
		}
	}

	if(isset($_POST['newsarchive'])){
		$user = new User();

		$params = array(
			'newsId' => $_POST['newsId'],
			'status' => $_POST['status']
		);

		$result = $user->archiveNewsbyStatus($params);
		if($result){
			$user->redirect('empMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empMaintenance.php');
		}		
	}


	if(isset($_POST['approvalClosing'])){
		$user = new User();

		$params = array(
			'closingId' => $_POST['closingId'],
			'status' => $_POST['closingstatus']
		);

		$result = $user->updateClosingStatus($params);
		if($result){
			$user->redirect('empApproval.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empApproval.php');
		}		
	}

	if(isset($_POST['approvalSeminar'])){
		$user = new User();

		$params = array(
			'seminarId' => $_POST['seminarId'],
			'status' => $_POST['semstatus']
		);

		$result = $user->updateSeminarStatus($params);
		if($result){
			$user->redirect('empApproval.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empApproval.php');
		}		
	}

	if(isset($_POST['approvalTrip'])){
		$user = new User();

		$params = array(
			'tripId' => $_POST['tripId'],
			'status' => $_POST['tripstatus']
		);

		$result = $user->updateVehicleTripStatus($params);
		if($result){
			$user->redirect('empApproval.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('empApproval.php');
		}		
	}

	if(isset($_POST['userDelete'])){
		$user = new User();

			$userId = $_POST['empId'];

		$result = $user->deleteUserbyId($userId);
		if($result){
			$user->redirect('adminMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('adminMaintenance.php');
		}		
	}

	if(isset($_POST['salesdelete'])){
		$user = new User();

			$salesId = $_POST['salesId'];

		$result = $user->deleteSalesbyId($salesId);
		if($result){
			$user->redirect('adminMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('adminMaintenance.php');
		}		
	}

	if(isset($_POST['tripdelete'])){
		$user = new User();

			$tripId = $_POST['tripId'];

		$result = $user->deleteTrippingbyId($tripId);
		if($result){
			$user->redirect('adminMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('adminMaintenance.php');
		}		
	}

	if(isset($_POST['seminardelete'])){
		$user = new User();

			$semId = $_POST['semId'];

		$result = $user->deleteSeminarbyId($semId);
		if($result){
			$user->redirect('adminMaintenance.php');
		} else {
			$_SESSION['Message'] = "Something went wrong please try again.";
			$_SESSION['MsgCode'] = 2;
			$user->redirect('adminMaintenance.php');
		}		
	}


	/*
	 * AJAX FUNCTIONS 
	 *
	 */
	if(isset($_POST['id'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getUserViaId($_POST['id']);
		echo $result;
	}
	if(isset($_POST['drivername'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getDriverViaName($_POST['drivername']);
		echo $result;
	}

	if(isset($_POST['subdname'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getSubdivisionViaSubdName($_POST['subdname']);
		echo $result;
	}

	if(isset($_POST['sortDownlineBySalesDir'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getDownlinesSalesDirector($_POST['sortDownlineBySalesDir']);
		echo $result;
	}

	if(isset($_POST['getClosing'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getClosingSalesViaUserId($_POST['getClosing']);

		echo $result;
	}

	// if(isset($_POST['getClosings'])){
	// 	$ajax = new ajaxFunction();
	// 	$result = $ajax->getClosingSalesCommissionViaUserId($_POST['getClosings'],$_POST['sessId']);

	// 	echo $result;
	// }

	if(isset($_POST['getUserProfile'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getUserInfoViaUserId($_POST['getUserProfile']);

		echo $result;
	}

	if(isset($_POST['getDownlinesViaId']) && isset($_POST['divId'])){
		$ajax = new ajaxFunction();
		$result = $ajax->getDownlinesPerDivision($_POST['divId'], $_POST['getDownlinesViaId']);

		echo $result;
	}


































} else {
	//Header("Location: index.php");
}
?>
