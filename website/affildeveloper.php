<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

  $prop = new Property();

  $myDev = $prop->getAllDevelopers();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
  .devel{display: relative;}
 .devel img{
  width: 310px;
 }
  
</style>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="container">
    <div class="row">
        <h1 style="font-size:40px;color:#197319;"> Affiliated Developers </h1>
        <div class="md-col-offset-2">
          <hr style="border-color:#000;">
        </div>
        <br>
        <div class="content">
          <div class="devel">
            <div class="row">
              <?php foreach ($myDev as $dev) { ?>
             
                <img src="<?php echo $dev['dh_dev_logo']; ?>" alt="" />
                <h3><?php echo $dev['dh_dev_name']; ?></h3>
                <div class="viewbutton">
                  <a class="sb-btn sb-btn-style-2" href="<?php echo $dev['dh_dev_link']; ?>">View Details</a>
                </div>
              
              <br>
              <?php } ?>
            </div>
          </div>
        </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>