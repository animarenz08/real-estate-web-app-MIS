<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

  session_start();
  $currentMenu = 54;
  $userGroup = 1;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $alluserData2 = $user->getActiveUsersExceptEmpAdmin();
  
  

if(isset($_GET['id']))
{
  $id=$_GET['id'];
  $mysales = $user->ClosingSalesbysalesId($id);
}
foreach($mysales as $val){
$getSubdivision = $user->getSubdivisionexceptEdit($val["dh_subdivision"]);
$division = $user->getAllDivisionexceptEdit($val['dh_division']);

}

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

  <div class="content">

    <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Edit Closing Sales </h2>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <form method="post" role="form" action="commonFunctions.php">
          <?php foreach($mysales as $row) { ?>
          <input type="hidden" class="salesId" name="salesId" value="<?php echo $row['dh_closing_id']; ?>">
          <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Family Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="familyname" value="<?php echo $row['dh_familyName']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>First Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="firstname" value="<?php echo $row['dh_Firstname']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>Cellphone #</label>
                        <input type="text" class="form-control" pattern="[0-9]{10,11}" title="Must contain numbers only and minimum of 10 numbers and maximum of 11 numbers" name="phoneno" value="<?php echo $row['dh_phoneNo']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>Telephone #</label>
                        <input type="text" class="form-control" name="telno" value="<?php echo $row['dh_telNo']; ?>">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Subdivision</label>
                        <select id="subdivision" name="subdivision" required class="form-control" onchange="subdname()">
                            <option value="<?php echo $row['dh_subdivision']; ?>"><?php echo $row['dh_subdivision']; ?> </option>
                            <?php foreach($getSubdivision as $subd) { ?>
                           <option value="<?php echo $subd['dh_subd_name']; ?>" > <?php echo $subd['dh_subd_name']; ?> </option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-3">
                        <label>Location</label>
                        <input type="text" class="form-control" id="location" name="location" readonly value="<?php echo $row['dh_location']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>Developer</label>
                        <input type="text" class="form-control" id="developer" name="developer" readonly value="<?php echo $row['dh_developer']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>House Model</label>
                        <input type="text" class="form-control" name="housemodel" value="<?php echo $row['dh_houseModel']; ?>">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     
                     <div class="col-sm-2">
                        <label>Phase #</label>
                        <input type="text" class="form-control" name="phase" value="<?php echo $row['dh_phase']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>Blk #</label>
                        <input type="text" class="form-control" name="block" value="<?php echo $row['dh_block']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>Lot #</label>
                        <input type="text" class="form-control" name="lot" value="<?php echo $row['dh_lot']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label>Floor Area</label>
                        <input type="text" class="form-control" name="floorarea" value="<?php echo $row['dh_floorArea']; ?>">(sqm.)
                     </div>
                     <div class="col-sm-3">
                        <label>Lot Area</label>
                        <input type="text" class="form-control" name="lotarea" value="<?php echo $row['dh_lotArea']; ?>">(sqm.)
                     </div>
                  </div>
               </div>
               <br>
               <br>
               <div class="form-group">
                  <div class="row">
                    <div class="col-sm-3">
                        <label>TCP/Checked w/ Dev</label>
                        <input type="text" class="form-control" name="tcpdev" value="<?php echo $row['dh_tcpDev']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>Reservation Fee</label>
                        <input type="text" class="form-control" name="reservationfee" value="<?php echo $row['dh_reservationFee']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>OI/PR #</label>
                        <input type="text" class="form-control" name="oipr" value="<?php echo $row['dh_oiPR']; ?>">
                     </div>
                     <div class="col-sm-3">
                        <label> Closing Date </label>
                            <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd yyyy" data-link-field="closingDateInput" data-link-format="mm-dd-yyyy">
                                <input class="form-control" size="5" name="closingDate" type="text" value="<?php echo $row['dh_closingDate']; ?>" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        <input type="hidden" id="closingDateInput" name="closingDate" value="<?php echo $row['dh_closingDate']; ?>" /><br/>    
                     </div>
                     <div class="col-sm-2 ">
                        <label>Division</label>
                        <select name="division" class="form-control">
                            <option value="<?php echo $row['dh_division']; ?>"><?php echo $row['dh_division_name']; ?></option>
                            <?php foreach($division as $divData){ ?>
                            <option value="<?php echo $divData['dh_division_id']; ?>"> <?php echo $divData['dh_division_name']; ?> </option>
                            <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Division Manager</label>
                        <input type="text" class="form-control" name="divisionmanager" value="<?php echo $row['dh_divisionManager']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="divcomrate" value="<?php echo $row['dh_div_comrate']; ?>" >
                    </div>
                     <div class="col-sm-3 col-sm-offset-1">
                        <label>Sales Director</label>
                        <input type="text" class="form-control" name="salesdirector" value="<?php echo $row['dh_salesDirector']; ?>">
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="dircomrate" value="<?php echo $row['dh_dir_comrate']; ?>">
                    </div>
                 </div>
             </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Sales Trainee/Agent/Referral</label>
                        <input type="text" class="form-control" name="salestrainee" value="<?php echo $row['dh_salesTrainee']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="salescomrate" value="<?php echo $row['dh_sales_comrate']; ?>">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Division Manager</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdiv" value="<?php echo $row['dh_rc_divisionManager']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdivcomrate" value="<?php echo $row['dh_rcdiv_comrate']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RC Sales Director</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdir" value="<?php echo $row['dh_rc_salesDirector']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdircomrate" value="<?php echo $row['dh_rcdir_comrate']; ?>">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Sales Trainee</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcsales" value="<?php echo $row['dh_rc_salesTrainee']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcsalescomrate" value="<?php echo $row['dh_rcsales_comrate']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RFA</label>
                        <input type="text" class="form-control" name="rfa" value="<?php echo $row['dh_rfa']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rfacomrate" value="<?php echo $row['dh_rfa_comrate']; ?>">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>Booking Assistant</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="bookassist" value="<?php echo $row['dh_bookAssistant']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="bookcomrate" value="<?php echo $row['dh_book_comrate']; ?>">
                    </div>
                </div>
            </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Assistant Agent</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="aassist" value="<?php echo $row['dh_aAssistant']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="acomrate" value="<?php echo $row['dh_a_comrate']; ?>">
                    </div>
                  </div>
               </div>
               <div class="form-group clearfix">
                  <br>
                  <br>
                  <div class="col-sm-2 col-sm-offset-5">
                     <button type="submit" name="updateclosing" class="btn btn-primary btn-md" style="width:100%">Update</button>
                  </div>
               </div>
          <?php } ?>
        </form>
      </div>
    </div>

  </div>
  <?php include 'footerFiles.php'; ?>
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>

<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
        function subdname() {
            var x = document.getElementById("subdivision").value;
            $.ajax({
                type: 'POST',
                url: '../website/commonFunctions.php',
                data: {
                    'subdname': x
                    },
                dataType: 'json',
                success: function(data){
                    $('#location').val(data.dh_subd_location);
                    $('#developer').val(data.dh_dev_name);
                    $('#subdsid').val(data.dh_subd_id);
                }
            });
        }
    </script>
</body>

</html>