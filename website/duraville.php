<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

    $prop = new Property();
    
    $myProj = $prop->getAllHouseProjects();
    $subd = $prop->getAllSubdivision();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<div class="content">

  <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"> Duraville Realty & Devt Corp </a></li>
            <li><a data-toggle="tab" href="#about"> About Duraville </a></li>
            <li><a data-toggle="tab" href="#subd"> Duraville Subdivisions </a></li>
            <li><a data-toggle="tab" href="#house"> Duraville House Project </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For home antel -->
          <div id="home" class="tab-pane fade in active" style="padding:25px;">
            <div class="antelhead">
              <img src="subimg/durav.jpg" alt="">
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                <img src="subimg/dura.jpg" style="width: 100%;">
              </div>
              <div class="col-sm-5">
                <h2>Duraville Realty & Devt Corp</h2>
                <h3>Mary Cris Ave, General Trias, Cavite</h3>
                With a portfolio that boasts of humble beginnings, the company is now ahead of the curve in anticipating and meeting the increased demand for a more convenient and restful living experience.
                Its rock-stable familiarity in the construction business affords the company to cement its experience in building strong and quality houses. Its sheer optimism and effortless concern, however, makes it one of the few real estate companies to offer home styles and convenience with the best cost-effective terms.

                <br>
                <br>
                Every Duraville Project Has A Safe And Friendly Community Environment. It May Be The Only One Realty Company That Earnestly Retains An After-sales Service And Maintenance Staff In Each Of Its Project Communities. Any Resident Who Reaches Out-to Ask For A Plumber Referral Or The Like-understands That Even After They Settle In, Duraville Continuously Make Them Feel Truly At Home.
              </div>
            </div>
          </div>
          <!-- end of home -->
          <!-- start of about antel -->
          <div id="about" class="tab-pane fade" style="padding: 25px;">
            <div class="buyer-wrap">
              <div class="g-grids">
                <h3>Company Profile</h3>

                <p>Duraville Realty and Development Corporation is a real estate company borne to progressively heed the demand of a typical Filipino family’s need of an architecturally distinct, pleasantly homey, ergonomically useful, space functional yet budget-friendly dream houses for he Filipino family. </p>
                <br>
              </div>
              <div class="g-grids">
                <h3>Solid Company</h3>
                <p>With a portfolio that boasts of humble beginnings, the company is now ahead of the curve in anticipating and meeting the increased demand for a more convenient and restful living experience.</p>
                <br>

              </div>

              <div class="g-grids">
                <h3>Construction Innovation</h3>
                <p>Its rock-stable familiarity in the construction business affords the company to cement its experience in building strong and quality houses. Its sheer optimism and effortless concern, however, makes it one of the few real estate companies to offer home styles and convenience with the best cost-effective terms.</p>
                <br>
              </div>

              <div class="g-grids">
                <h3>Groundbreaking Concepts</h3>
                <p>To date, guided by the same spirit and dynamism to provide superior dream home experience, Duraville has developed, constructed and sold about 10,200 residential housing and executive home units while 3,000 more are under way. Duraville has been in the real estate business for 25 years.</p>
                <br>
              </div>
            </div>
          </div>
          <!-- end of about -->
          <!-- start of subdivisions -->
          <div id="subd" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($subd as $subdData) { ?>
            <?php if($subdData['dh_subd_dev_id'] == '5') { ?>
            <div class="col-sm-4">
              <img src="<?php echo $subdData['dh_subd_logo']; ?>" style="width: 100%;" alt="">
              <h3><?php echo $subdData['dh_subd_name']; ?></h3>
              <h4><?php echo $subdData['dh_subd_location']; ?></h4>
              <p><?php echo $subdData['dh_subd_description']; ?></p>
            </div>
            <?php } ?>
            <?php } ?>
          </div>
          <!-- end of subdivisions -->
          <!-- start of houses -->
          <div id="house" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($myProj as $proj) { ?>
            <?php if($proj['dh_subd_id'] == '7' OR $proj['dh_subd_id'] == '8') { ?>
            <div class="col-sm-4">
                <a href="<?php echo $proj['dh_image_path']; ?>"><img src="<?php echo $proj['dh_image_path']; ?>" style="width: 100%;" alt=""></a>
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
                <p><?php echo $proj['dh_description']; ?></p>
                <div class = "viewbutton">
                  <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details">
                  </form>
                </div>
                <br>
              </div>
              <?php } ?>
              <?php } ?>
          </div>
          <!-- end of houses -->
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>