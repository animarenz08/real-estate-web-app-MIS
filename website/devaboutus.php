<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 203;
    $developerid = 2;
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'devHeader.php'; ?>

<div class="container">

      <div class="buyer-wrap">
        <div class="g-grids">
                  <h3>Company Profile</h3>

                  <p>A development that is designed around and is tempered by 
                    human and natural elements. Committed to the protection and 
                    management of the environment. Constantly conscious of our 
                    responsibility to care for the Earth and to help shape a living 
                    and working environment in which Filipinos live in harmony with 
                    nature. Strategically developing properties that will spur 
                    progress as well as add value to the communities in which they are 
                    located. </p>
                  <br>
            </div>
            <div class="g-grids">
                  <h3>Solid Company</h3>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames 
              ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor 
              sit amet, ante.</p>
              <br>

            </div>

            <div class="g-grids">
                  <h3>Construction Innovation</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit, orci eu 
                facilisis efficitur, felis ipsum mollis ipsum, a iaculis lacus leo in libero. Duis 
                vehicula turpis eu tincidunt viverra. Fusce vehicula augue suscipit, congue nisi 
                sitamet, dapibus lectus. Morbi rutrum ipsum at pretium lacinia.</p>
                <br>

            </div>

            <div class="g-grids">
                  <h3>Groundbreaking Concepts</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit, orci eu 
                facilisis efficitur, felis ipsum mollis ipsum, a iaculis lacus leo in libero. Duis 
                vehicula turpis eu tincidunt viverra. Fusce vehicula augue suscipit, congue nisi 
                sitamet, dapibus lectus. Morbi rutrum ipsum at pretium lacinia.</p>
                <br>

            </div>
  </div>
</div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>