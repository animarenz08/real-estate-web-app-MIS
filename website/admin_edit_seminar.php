<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

	session_start();
	$currentMenu = 54;
  $userGroup = 1;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);

  if(isset($_GET['id']))
{
  $id=$_GET['id'];
  $editsem = $user->getSeminarbyId($id);
}


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
  <?php include 'mainHeader.php'; ?>

  <div class="content">

    <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Edit Seminar Schedule </h2>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <form method="post" action="commonFunctions.php">
          <?php foreach($editsem as $row) { ?>
            <input type="hidden" name="seminarId" value="<?php echo $row['dh_seminar_id']; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-5 col-sm-offset-1">
                <label> Seminar Name </label>
                <input type="text" required class="form-control" name="seminarName" value="<?php echo $row['dh_seminar_name']; ?>">
              </div>
              <div class="col-sm-5">
                <label> Seminar Date </label>
                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                  <input class="form-control" size="5" name="seminarDate" type="text" value="<?php echo $row['dh_seminar_date']; ?>" readonly>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
              <input type="hidden" id="dtp_input2" name="seminarDate" value="<?php echo $row['dh_seminar_date']; ?>" /><br/>
              </div>
            </div>
          </div>
          <div style="clear:both;"></div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-5 col-sm-offset-1">
                <label style="display:block;"> Seminar Time </label>
                <div class="input-group" style="width: 49%;float: left;margin-right:5px;">
                <input type="time" class="form-control" name="seminarTimeStart" value="<?php echo $row['dh_seminar_time_start']; ?>" style="display: inline-block;position: relative;"><span class="input-group-addon"> Start Time </span>
                </div>
                <div class="input-group" style="width: 49%;float: left;">
                <input type="time" class="form-control" name="seminarTimeEnd" value="<?php echo $row['dh_seminar_time_end']; ?>" style="display: inline-block;position: relative;"><span class="input-group-addon"> End Time </span>
                </div>
              </div>
              <div class="col-sm-5">
                <label> Seminar Location </label>
                <input type="text" required class="form-control" name="seminarLocation" value="<?php echo $row['dh_seminar_location']; ?>">
              </div>
            </div>
          </div>
          <br>
          <br>
          <div style="clear:both;"></div>
          <div class="form-group clearfix">
                      <div class="row">
                        <div class="col-sm-4 col-sm-offset-5">
                          <button type="submit" name="updateSeminar" class="btn btn-primary btn-lg">Update</button>
                        </div>
                      </div>
                    </div>
          <?php } ?>
        </form>
        <br>
        <br>
        <br>
      </div>
    </div>

  </div>
  <?php include 'footerFiles.php'; ?>
 <script src="js/jquery.js"></script>
  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
  <script type="text/javascript">
        $('.form_date').datetimepicker({
                language:  'ar',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
            });
    </script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>