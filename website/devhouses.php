<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 204;
    $developerid = 2;

    $prop = new Property();

    $myProj = $prop->HouseProjectsbySubdId($developerid);
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'devHeader.php'; ?>

  <div class="container">
    <div class="row">
        <div class="col-sm-7">
          <a href="subimg/antelheader.jpg"><img src="subimg/antelheader.jpg" alt=""></a>
        </div>
          <div class="col-sm-5">
            <div class="househeader">
              <h2>Model Units</h2>
              <p>
              A masterfully planned community of Antel Group of Companies, the Antel Grand
              Village (AGV) is the first and only self contained island community, south of Metro
              Manila, with more than 100 hectares of carefully planned residential villages, social
              and commercial districts, and a 1.7 hectare clubhouse, all in the midst of verdant
              sceneries and breathtaking riverside vistas.</p>
            </div>
          </div>
    </div>
    <div class="row">
      <div class="midcont">
        <h1 style="font-size:40px;color:#197319;"> Antel House Projects </h1>
        <div class="md-col-offset-2">
          <hr style="border-color:#000;">
          <div class="housed">
            <div class="row">
              <?php foreach ($myProj as $proj) { ?>
              <div class="col-md-4">
                <img src="<?php echo $proj['dh_image_path']; ?>" />
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <p><?php echo $proj['dh_description']; ?></p>
                <br>
                <div class="viewbutton">
                <a class="sb-btn sb-btn-style-2" href="<?php echo $proj['dh_project_path']; ?>">View Details</a>
                </div>
                <br>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
      
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>