<?php
	include '../common/class.users.php';
	include '../common/class.properties.php';
		session_start();
		$currentMenu = 16;
		$userGroup = 1;

	$user = new User();

	$user->isPageAccessible($_SESSION['user_type'], $userGroup);
	$myUsers = $user->getAllUserDetailsAdmin($_SESSION['user_session']);
	$mySem = $user->getAllSeminar();
	$mySales = $user->getAllClosingSales();
	$myTrip = $user->getAllVehicleTrip();

	$prop = new Property();
	$myProj = $prop->getAllHouseProjects();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/bootstrap-datepicker.css" />
<style>
.dataTables_wrapper .dt-buttons {
  float:none;  
  text-align:right;
}
</style>
</head>
<body>
		<?php include 'mainHeader.php'; ?>

	<div class="content">
		<?php if(isset($Message)){ ?>
				<div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
			&nbsp; <?php echo $Message; ?>!</div>
		<?php unset($_SESSION["Message"]); } ?>

		<br>
		<div class="row">
			<div class="col-md-10 col-sm-offset-1">
				<div class="encoder-container">
					<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#myProj"> House Project Report </a></li>
							<li><a data-toggle="tab" href="#myUser"> Employee Report </a></li>
							<li><a data-toggle="tab" href="#closesales"> Closing Sales Report </a></li>
							<li><a data-toggle="tab" href="#sem"> Seminar Report </a></li>
							<li><a data-toggle="tab" href="#trip"> Vehicle Trip Report </a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div id="myProj" class="tab-pane fade in active" style="padding:25px;">
					<h2> All House Projects </h2>
							<hr>
							<div class="row">
								<div class="rangedate">
									<div class="col-sm-2">
										<input type="text" name="projmin" id="projmin" placeholder="From" class="form-control" />
									</div>

									<div class="col-sm-2">
										<input type="text" name="projmax" id="projmax" placeholder="To" class="form-control" />
									</div>      
								</div>
							</div>
							<table id="housetable" class="table table-condensed table-hover table-striped" id="myHouseProjects">
							<thead>
								<tr>
									<th> House Project Name </th>
									<th> House Project Description </th>
									<th> House Image </th>
									<th> House Project Location </th>
									<th> Promos </th>
									<th> Date Created </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($myProj) > 0) { ?> 
									<?php foreach ($myProj as $projData) { ?>
										<tr>
												<td> <?php echo $projData['dh_house_name']; ?> </td>
												<td> <?php echo $projData['dh_description']; ?> </td>
												<td><img src="<?php echo $projData['dh_image_path']; ?>" style="width: 90%;" /> </td>
												<td> <?php echo $projData['dh_location']; ?> </td>
												<td> <?php echo $projData['dh_promos']; ?> </td>
												<td> <?php echo $projData['dh_date_created']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="6" style="text-align: center; color:red;"> 
												<h2> No House Project </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>													
					</div>
					<!-- Start For users -->
					<div id="myUser" class="tab-pane fade" style="padding:25px;">
						<h2> All Employees </h2>
							<hr>
							<div class="row">
								<div class="rangedate">
									<div class="col-sm-2">
										<input type="text" name="usermin" id="usermin" placeholder="From" class="form-control" />
									</div>

									<div class="col-sm-2">
										<input type="text" name="usermax" id="usermax" placeholder="To" class="form-control" />
									</div>      
								</div>
								<!-- <div class="col-sm-1">
									<button class="btn btn-info" type="button" id="clear">Clear</button>
								</div> -->
							</div>

							<table id="userTable" class="table table-condensed table-hover table-striped">
						     	<thead>
						     		<tr>
						       			<th> Full Name </th>
										<th> User Type </th>
										<th> Email Address </th>
										<th> Contact # </th>
										<th> Gender </th>
										<th> Birth Date </th>
										<th> Age </th>
										<th> Date Created </th>
										<th> User Status </th>
						      		</tr>
						     	</thead>
						     	<tbody>
									<?php if(count($myUsers) > 1) { ?> 
									<?php foreach ($myUsers as $userData) { ?>
										<tr>
												<td> <?php echo $userData['dh_firstName'].' '.$userData['dh_middleName'].' '.$userData['dh_lastName']; ?> </td>
												<td> <?php echo $userData['dh_user_group_name']; ?> </td>
												<td> <?php echo $userData['dh_email_address']; ?> </td>
												<td> <?php echo $userData['dh_contact_no']; ?> </td>
												<td> <?php echo $userData['dh_gender']; ?> </td>
												<td> <?php echo $userData['dh_bday']; ?> </td>
												<td> <?php echo $userData['dh_age']; ?> </td>
												<td> <?php echo $userData['dh_date_created']; ?> </td>
												<td> <?php echo $userData['dh_status']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="9" style="text-align: center; color:red;"> 
												<h2> No Employees </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						    </table>
					</div>

					<div id="closesales" class="tab-pane fade" style="padding:25px;">
					<h2> All Closing Sales </h2>
							<hr>
							<div class="row">
								<div class="rangedate">
									<div class="col-sm-2">
										<input type="text" name="salemin" id="salemin" placeholder="From" class="form-control" />
									</div>

									<div class="col-sm-2">
										<input type="text" name="salemax" id="salemax" placeholder="To" class="form-control" />
									</div>      
								</div>
							</div>

							<table id="closingTable" class="table table-condensed table-hover table-striped" id="myClosingSales">
							<thead>
								<tr>
									<th> Full Name </th>
									<th> Phone No </th>
									<th> Telephone No </th>
									<th> Complete Address </th>
									<th> Developer </th>
									<th> House Model </th>
									<th> Floor & Lot Area </th>
									<th> TCP/Checked w/ Dev </th>
									<th> Reservation Fee </th>
									<th> Division </th>
									<th> Prepared By </th>
									<th> Closing Sale Date </th>
									<th> Date Created </th>

								</tr>
							</thead>
							<tbody>
									<?php if(count($mySales) > 0) { ?> 
									<?php foreach ($mySales as $salesData) { ?>
										<tr>
												<td> <?php echo $salesData['dh_Firstname'].' '.$salesData['dh_familyName']; ?> </td>
												<td> <?php echo $salesData['dh_phoneNo']; ?> </td>
												<td> <?php echo $salesData['dh_telNo']; ?> </td>
												<td> <?php echo 'Blk '.$salesData['dh_block'].' & Lot '.$salesData['dh_lot'].' Phase '.$salesData['dh_phase'].', '.$salesData['dh_subdivision'].', '.$salesData['dh_location']; ?> </td>
												<td> <?php echo $salesData['dh_developer']; ?> </td>
												<td> <?php echo $salesData['dh_houseModel']; ?> </td>
												<td> <?php echo $salesData['dh_floorArea'].'sqm - '.$salesData['dh_lotArea'].'sqm'; ?> </td>
												<td> ₱ <?php echo $salesData['dh_tcpDev']; ?> </td>
						                        <td> ₱ <?php echo $salesData['dh_reservationFee']; ?> </td>
						                        <td> <?php echo $salesData['dh_division']; ?> </td>
						                        <td> <?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName']; ?> </td>
												<td> <?php echo $salesData['dh_closingDate']; ?> </td>
												<td> <?php echo $salesData['dh_created_date']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="13" style="text-align: center; color:red;"> 
												<h2> No Closing Sales </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>											
					</div>

					

					<div id="sem" class="tab-pane fade" style="padding:25px;">
					<h2> All Seminar </h2>
							<hr>
							<div class="row">
								<div class="rangedate">
									<div class="col-sm-2">
										<input type="text" name="semmin" id="semmin" placeholder="From" class="form-control" />
									</div>

									<div class="col-sm-2">
										<input type="text" name="semmax" id="semmax" placeholder="To" class="form-control" />
									</div>      
								</div>
							</div>
							<table id="semtable" class="table table-condensed table-hover table-striped" id="mySeminars">
							<thead>
								<tr>
									<th> Seminar Name </th>
									<th> Seminar Date </th>
									<th> Seminar Time Start & End </th>
									<th> Seminar Location </th>
									<th> Date Created </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($mySem) > 0) { ?> 
									<?php foreach ($mySem as $semData) { ?>
										<tr>
												<td> <?php echo $semData['dh_seminar_name']; ?> </td>
												<td> <?php echo $semData['dh_seminar_date']; ?> </td>
												<td> <?php echo $semData['dh_seminar_time_start'].' - '.$semData['dh_seminar_time_end'];  ?> </td>
												<td> <?php echo $semData['dh_seminar_location']; ?> </td>
												<td> <?php echo $semData['dh_date_created']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="5" style="text-align: center; color:red;"> 
												<h2> No Seminars </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>
					</div>

					<div id="trip" class="tab-pane fade" style="padding:25px;">
					<h2> All Vehicle Tripping </h2>
							<hr>
							<div class="row">
								<div class="rangedate">
									<div class="col-sm-2">
										<input type="text" name="tripmin" id="tripmin" placeholder="From" class="form-control" />
									</div>

									<div class="col-sm-2">
										<input type="text" name="tripmax" id="tripmax" placeholder="To" class="form-control" />
									</div>      
								</div>
							</div>
							<table id="triptable" class="table table-condensed table-hover table-striped" id="myTrip">
							<thead>
								<tr>
									<th> Vehicle Trip Date </th>
									<th> Location Pickup & Pickup Time </th>
									<th> Location Destination & Destination Time </th>
									<th> Driver Name </th>
									<th> Plate No </th>
									<th> Driver Contact No </th>
									<th> Date Created </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($myTrip) > 0) { ?> 
									<?php foreach ($myTrip as $tripData) { ?>
										<tr>
												<td> <?php echo $tripData['dh_trip_date']; ?> </td>
												<td> <?php echo $tripData['dh_location_pickup'].' - '.$tripData['dh_location_pickup_time']; ?> </td>
												<td> <?php echo $tripData['dh_location_destination'].' - '.$tripData['dh_location_destination_time'];  ?> </td>
												<td> <?php echo $tripData['dh_driver_name']; ?> </td>
												<td> <?php echo $tripData['dh_plateno']; ?> </td>
												<td> <?php echo $tripData['dh_driver_contact']; ?> </td>
												<td> <?php echo $tripData['dh_date_created']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="7" style="text-align: center; color:red;"> 
												<h2> No Vehicle Tripping </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
		
	</div>

	<?php include 'footerFiles.php'; ?>
	<script src="js/jquery.js"></script>

	<script src='js/moment.min.js'></script>
	<script src='js/jquery.min.js'></script>
	<script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="js/buttons.print.min.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
	<script>
		// function ClearFields() {
		// 	document.getElementById("usermin").value = "";
		// 	document.getElementById("usermax").value = "";
		// }
	</script>

	<script type="text/javascript">
		$(document).ready(function () {

			$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					var salemin = $('#salemin').datepicker("getDate");
					var salemax = $('#salemax').datepicker("getDate");
					var startDate = new Date(data[12]);
					if (salemin == null && salemax == null) { return true; }
					if (salemin == null && startDate <= salemax) { return true;}
					if(salemax == null && startDate >= salemin) {return true;}
					if (startDate <= salemax && startDate >= salemin) { return true; }
					return false;
				}
				);

			$("#salemin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			$("#salemax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			var table = $('#closingTable').DataTable( {
				dom: 'Bfrtip',
				buttons: [ { extend: 'print', 
				customize: function ( win ) { 
					$(win.document.body) .css( 'font-size', '10pt' ) 
					.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
					$(win.document.body).find( 'table' ) 
					.addClass( 'compact' ) 
					.css( 'font-size', 'inherit' ); }
				}]
			});
			// Event listener to the two range filtering inputs to redraw on input
			$('#salemin, #salemax').change(function () {
				table.draw();
			});
			
		});
		</script>
		<script type="text/javascript">
		$(document).ready(function () {

			$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					var usermin = $('#usermin').datepicker("getDate");
					var usermax = $('#usermax').datepicker("getDate");
					var startDate = new Date(data[7]);
					if (usermin == null && usermax == null) { return true; }
					if (usermin == null && startDate <= usermax) { return true;}
					if(usermax == null && startDate >= usermin) {return true;}
					if (startDate <= usermax && startDate >= usermin) { return true; }
					return false;
				}
				);

			$("#usermin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			$("#usermax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			var table = $('#userTable').DataTable( {
				dom: 'Bfrtip',
				buttons: [ { extend: 'print', 
				customize: function ( win ) { 
					$(win.document.body) .css( 'font-size', '10pt' ) 
					.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
					$(win.document.body).find( 'table' ) 
					.addClass( 'compact' ) 
					.css( 'font-size', 'inherit' ); }
				}]
			});
			// Event listener to the two range filtering inputs to redraw on input
			$('#usermin, #usermax').change(function () {
				table.draw();
			});
			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
		$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					var projmin = $('#projmin').datepicker("getDate");
					var projmax = $('#projmax').datepicker("getDate");
					var startDate = new Date(data[5]);
					if (projmin == null && projmax == null) { return true; }
					if (projmin == null && startDate <= projmax) { return true;}
					if(projmax == null && startDate >= projmin) {return true;}
					if (startDate <= projmax && startDate >= projmin) { return true; }
					return false;
				}
				);

			$("#projmin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			$("#projmax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			var table = $('#housetable').DataTable( {
				dom: 'Bfrtip',
				buttons: [ { extend: 'print', 
				customize: function ( win ) { 
					$(win.document.body) .css( 'font-size', '10pt' ) 
					.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
					$(win.document.body).find( 'table' ) 
					.addClass( 'compact' ) 
					.css( 'font-size', 'inherit' ); }
				}]
			});
			// Event listener to the two range filtering inputs to redraw on input
			$('#projmin, #projmax').change(function () {
				table.draw();
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
		$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					var semmin = $('#semmin').datepicker("getDate");
					var semmax = $('#semmax').datepicker("getDate");
					var startDate = new Date(data[4]);
					if (semmin == null && semmax == null) { return true; }
					if (semmin == null && startDate <= semmax) { return true;}
					if(semmax == null && startDate >= semmin) {return true;}
					if (startDate <= semmax && startDate >= semmin) { return true; }
					return false;
				}
				);

			$("#semmin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			$("#semmax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			var table = $('#semtable').DataTable( {
				dom: 'Bfrtip',
				buttons: [ { extend: 'print', 
				customize: function ( win ) { 
					$(win.document.body) .css( 'font-size', '10pt' ) 
					.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
					$(win.document.body).find( 'table' ) 
					.addClass( 'compact' ) 
					.css( 'font-size', 'inherit' ); }
				}]
			});
			// Event listener to the two range filtering inputs to redraw on input
			$('#semmin, #semmax').change(function () {
				table.draw();
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
		$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					var tripmin = $('#tripmin').datepicker("getDate");
					var tripmax = $('#tripmax').datepicker("getDate");
					var startDate = new Date(data[6]);
					if (tripmin == null && tripmax == null) { return true; }
					if (tripmin == null && startDate <= tripmax) { return true;}
					if(tripmax == null && startDate >= tripmin) {return true;}
					if (startDate <= tripmax && startDate >= tripmin) { return true; }
					return false;
				}
				);

			$("#tripmin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			$("#tripmax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
			var table = $('#triptable').DataTable( {
				dom: 'Bfrtip',
				buttons: [ { extend: 'print', 
				customize: function ( win ) { 
					$(win.document.body) .css( 'font-size', '10pt' ) 
					.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
					$(win.document.body).find( 'table' ) 
					.addClass( 'compact' ) 
					.css( 'font-size', 'inherit' ); }
				}]
			});
			// Event listener to the two range filtering inputs to redraw on input
			$('#tripmin, #tripmax').change(function () {
				table.draw();
			});
		});
	</script>
<!-- 	<script>
	$('#clear').click(function() {
				$('#usermin, #usermax').val(' ');
				table.search('').draw(); //required after
			});
		</script> -->
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>

	<script>
	$(document).ready(function(){
		$('#errMsg').fadeOut(5000); 

		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});

	});
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src='js/fullcalendar.min.js'></script>
</body>

</html>