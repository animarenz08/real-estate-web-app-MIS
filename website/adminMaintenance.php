<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 15;
    $userGroup = 1;

  $user = new User();
  $status = 'Active';

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $myUsers = $user->getAllUserDetailsAdmin($_SESSION['user_session']);
  $allUser = $user->getAllUserInfo($_SESSION['user_session']);
  $mySales = $user->getAllClosingSales();
  $getDevelopers = $user->getDevelopers();
  $alluserData = $user->getAllUser($status);
  $alluserData2 = $user->getActiveUsersExceptEmpAdmin();
  $allSalesDirector = $user->getAllActiveSalesDirector();
  $allDivisionManager = $user->getAllDivisionManagerwithDivisionName();
  $division = $user->getAllDivision();
  $recruitedByUser = $user->getAllRecruiter();
  $subd = $user->getAllSubdivision();
  $group = $user->getAlluserGroups();

  $mySeminar = $user->getAllSeminar();
  $myTrip = $user->getAllVehicleTrip();

  $prop = new Property();
  $myHouse = $prop->getAllHouseProjects();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
    <?php include 'mainHeader.php'; ?>

  <div class="content">
    <?php if(isset($Message)){ ?>
        <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
      &nbsp; <?php echo $Message; ?>!</div>
    <?php unset($_SESSION["Message"]); } ?>

    <br>
    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#myHouse"> House Project Record </a></li>
              <li><a data-toggle="tab" href="#myUser"> Employee Record </a></li>
              <li><a data-toggle="tab" href="#mySales"> Closing Sales Record </a></li>
              <li><a data-toggle="tab" href="#mySem"> Seminars Record </a></li>
              <li><a data-toggle="tab" href="#myTrip"> Vehicle Tripping Record </a></li>
              <li><a data-toggle="tab" href="#newuser"> Add New User </a></li>
              <li><a data-toggle="tab" href="#houseProjects">Add New House Projects </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <div id="myHouse" class="tab-pane fade in active" style="padding:25px;">
            <h2> All House Project </h2>
              <hr>
              <div class="transaction-table">
              <table id="tableproject" class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> House Name </th>
                    <th> House Description </th>
                    <th> House Sample </th>
                    <th> House Location </th>
                    <th> House Promos </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($myHouse) > 0) { ?> 
                  <?php foreach ($myHouse as $houseData) { ?>
                    <tr>
                        <input type="hidden" class="houseId" value="<?php echo $houseData['dh_house_proj_id']; ?>">
                        <td class="houseName"> <?php echo $houseData['dh_house_name']; ?> </td>
                        <td> <?php echo $houseData['dh_description']; ?> </td>
                        <td> <img src="<?php echo $houseData['dh_image_path']; ?>" style="width: 90%;" /> </td>
                        <td> <?php echo $houseData['dh_location']; ?> </td>
                        <td> <?php echo $houseData['dh_promos']; ?> </td>
                        <td>
                          <a href="admin_edit_project.php?id=<?php echo $houseData['dh_house_proj_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a> 
                          <button class="btn btn-sm btn-danger deleteProject"> Delete <span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="7" style="text-align: center; color:red;"> 
                        <h2> No House Project</h2>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- End of House Project Record -->

          <div id="myUser" class="tab-pane fade" style="padding:25px;">
            <h2> All Employees </h2>
              <hr>
              <div class="transaction-table">
              <table id="tableuser" class="table table-condensed table-hover table-striped">
              <thead>
                <tr>
                  <th> Full Name </th>
                  <th> User Type </th>
                  <th> Email Address </th>
                  <th> Contact # </th>
                  <th> Gender </th>
                  <th> Birth Date </th>
                  <th> Age </th>
                  <th> Date Created </th>
                  <th> User Status </th>
                  <th> Action </th>
                </tr>
              </thead>
              <tbody>
                  <?php if(count($myUsers) > 1) { ?> 
                  <?php foreach ($myUsers as $userData) { ?>
                    <tr>
                        <input type="hidden" class="empId" value="<?php echo $userData['dh_user_id']; ?>">
                        <td class="fullName"> <?php echo $userData['dh_firstName'].' '.$userData['dh_middleName'].' '.$userData['dh_lastName']; ?> </td>
                        <td> <?php echo $userData['dh_user_group_name']; ?> </td>
                        <td> <?php echo $userData['dh_email_address']; ?> </td>
                        <td> <?php echo $userData['dh_contact_no']; ?> </td>
                        <td> <?php echo $userData['dh_gender']; ?> </td>
                        <td> <?php echo $userData['dh_bday']; ?> </td>
                        <td> <?php echo $userData['dh_age']; ?> </td>
                        <td> <?php echo $userData['dh_date_created']; ?> </td>
                        <td> <?php echo $userData['dh_status']; ?> </td>
                        <td> 
                          <button class="btn btn-sm btn-primary viewClosing"> View Closing Sales </button> <a href="admin_edit_user.php?id=<?php echo $userData['dh_user_id']; ?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a>  
                          <button class="btn btn-sm btn-danger deleteUser" id="deleteUser"> Delete <span class="glyphicon glyphicon-trash"></span></button> 
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr> 
                      <td colspan="10" style="text-align: center; color:red;"> 
                        <h2> No Employees </h2>
                      </td>
                    </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
          </div>

          

          <!-- Start of closing sales Record -->
          <div id="mySales" class="tab-pane fade" style="padding:25px;">
          <h2> All Closing Sales </h2>
              <hr>
              <table class="table table-condensed table-hover table-striped" id="tableclosing">
              <thead>
                <tr>
                  <th> Full Name </th>
                  <th> Phone No </th>
                  <th> Telephone No </th>
                  <th> Complete Address </th>
                  <th> Developer </th>
                  <th> House Model </th>
                  <th> Floor & Lot Area </th>
                  <th> TCP/Checked w/ Dev </th>
                  <th> Reservation Fee </th>
                  <th> Division </th>
                  <th> Prepared By </th>
                  <th> Closing Sale Date </th>
                  <th> Date Created </th>
                  <th> Action </th>

                </tr>
              </thead>
              <tbody>
                  <?php if(count($mySales) > 0) { ?> 
                  <?php foreach ($mySales as $salesData) { ?>
                    <tr>
                        <input type="hidden" class="salesId" value="<?php echo $salesData['dh_closing_id']; ?>">
                        <td class="fullName"> <?php echo $salesData['dh_Firstname'].' '.$salesData['dh_familyName']; ?> </td>
                        <td> <?php echo $salesData['dh_phoneNo']; ?> </td>
                        <td> <?php echo $salesData['dh_telNo']; ?> </td>
                        <td> <?php echo 'Blk '.$salesData['dh_block'].' Lot '.$salesData['dh_lot'].' Phase '.$salesData['dh_phase'].', '.$salesData['dh_subdivision'].', '.$salesData['dh_location']; ?> </td>
                        <td> <?php echo $salesData['dh_developer']; ?> </td>
                        <td> <?php echo $salesData['dh_houseModel']; ?> </td>
                        <td> <?php echo $salesData['dh_floorArea'].'sqm - '.$salesData['dh_lotArea'].'sqm'; ?> </td>
                        <td>₱ <?php echo $salesData['dh_tcpDev']; ?> </td>
                        <td>₱ <?php echo $salesData['dh_reservationFee']; ?> </td>
                         <td> <?php echo $salesData['dh_division']; ?> </td>
                        <td> <?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName']; ?> </td>
                        <td> <?php echo $salesData['dh_closingDate']; ?> </td>
                         <td> <?php echo $salesData['dh_created_date']; ?> </td>
                        <td>
                          <a href="admin_edit_closing.php?id=<?php echo $salesData['dh_closing_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a>
                          <button class="btn btn-sm btn-danger deleteSales"> Delete <span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr> 
                      <td colspan="14" style="text-align: center; color:red;"> 
                        <h2> No Closing Sales </h2>
                      </td>
                    </tr>
                  <?php } ?>
              </tbody>
            </table>                          
          </div>
          <!-- End of Closing Sales Record -->

          <!-- start of events record -->
          <div id="mySem" class="tab-pane fade" style="padding:25px;">
                <h2> All Seminar Schedule </h2>
                <hr>
                <table class="table table-condensed table-hover table-striped" id="tablesem">
                  <thead>
                    <tr>
                      <th> Seminar Name </th>
                      <th> Seminar Date </th>
                      <th> Start Time </th>
                      <th> End time </th>
                      <th> Seminar Location </th>
                      <th> Date Created </th>
                      <th> Status </th>
                      <th> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($mySeminar) > 0) { ?> 
                  <?php foreach($mySeminar as $semData) { ?>
                    <tr>
                      <input type="hidden" class="seminarId" value="<?php echo $semData['dh_seminar_id']; ?>">
                      <td> <?php echo $semData['dh_seminar_name']; ?> </td>
                      <td> <?php echo $semData['dh_seminar_date']; ?> </td>
                      <td> <?php echo $semData['dh_seminar_time_start']; ?> </td>
                      <td> <?php echo $semData['dh_seminar_time_end']; ?> </td>
                      <td> <?php echo $semData['dh_seminar_location']; ?> </td>
                      <td> <?php echo $semData['dh_date_created']; ?> </td>
                      <td <?php if($semData['dh_seminar_status'] == 'Declined'){ ?> style="vertical-align: middle; color:red;" <?php } elseif($semData['dh_seminar_status'] == 'For Approval') { ?> style="vertical-align: middle; color:#337ab7;" <?php }else{ ?> style="vertical-align: middle; color:#5cb85c;" <?php } ?> > <?php echo $semData['dh_seminar_status']; ?> </td>
                      
                      <td><a href="admin_edit_seminar.php?id=<?php echo $semData['dh_seminar_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a>
                          <button class="btn btn-sm btn-danger deleteSeminar"> Delete <span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="8" style="color:red;text-align: center;"><h2> No Pending Request </h2></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
          </div>
         <!--  end of events record -->

         <!-- start of events record -->
          <div id="myTrip" class="tab-pane fade" style="padding:25px;">
              
                <h2> All Vehicle Tripping Schedule </h2>
                <hr>
                <table class="table table-condensed table-hover table-striped" id="tabletrip">
                  <thead>
                    <tr>
                      <th> Trip Date </th>
                      <th> Location Pick Up</th>
                      <th> Pick Up Time </th>
                      <th> Destination </th>
                      <th> Destination Time </th>
                      <th> Driver Name </th>
                      <th> Driver Contact No. </th>
                      <th> Vehicle Plate No. </th>
                      <th> Date Created </th>
                      <th> Status </th>
                      <th> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($myTrip) > 0) { ?>
                  <?php foreach($myTrip as $tripData) { ?>
                    <tr>
                      <input type="hidden" class="tripId" value="<?php echo $tripData['dh_vehicle_trip_id']; ?>">
                      <td> <?php echo $tripData['dh_trip_date']; ?> </td>
                      <td> <?php echo $tripData['dh_location_pickup']; ?> </td>
                      <td> <?php echo $tripData['dh_location_pickup_time']; ?> </td>
                      <td> <?php echo $tripData['dh_location_destination']; ?> </td>
                      <td> <?php echo $tripData['dh_location_destination_time']; ?> </td>
                      <td> <?php echo $tripData['dh_driver_name']; ?> </td>
                      <td> <?php echo $tripData['dh_driver_contact']; ?> </td>
                      <td> <?php echo $tripData['dh_plateno']; ?> </td>
                      <td> <?php echo $tripData['dh_date_created']; ?> </td>
                      <td <?php if($tripData['dh_trip_status'] == 'Declined'){ ?> style="vertical-align: middle; color:red;" <?php } elseif($tripData['dh_trip_status'] == 'For Approval') { ?> style="vertical-align: middle; color:#337ab7;" <?php }else{ ?> style="vertical-align: middle; color:#5cb85c;" <?php } ?> > <?php echo $tripData['dh_trip_status']; ?> </td>
                      <td><!-- <a href="admin_edit_trip.php?id=<?php echo $tripData['dh_vehicle_trip_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a> -->
                          <button class="btn btn-sm btn-danger deleteTrip"> Delete <span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="8" style="color:red;text-align: center;"><h2> No Pending Request </h2></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
          </div>
         <!--  end of events record -->

           <!-- Start For New User -->
        <div id="newuser" class="tab-pane fade" style="padding:25px">
            <h5>* - Means Required Field</h5>
              <hr>
                <form method="post" action="commonFunctions.php">

                    <div style="clear:both;"></div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Position *</label>
                                <select name="userType" required title="Please select one of this option" class="form-control">
                                    <?php foreach ($group as $groupuser) { ?>
                                    <option value="<?php echo $groupuser['dh_user_group_id']; ?>"><?php echo $groupuser['dh_user_group_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-3">
                                <label>ID Number *</label>
                                <input type="text" required class="form-control" pattern="[0-9]{7,10}" title="Must contain numbers only, minimum of 7 numbers and maximum of 10 numbers" name="userId" placeholder="Enter your ID/RFID Number">
                            </div>
                            <div class="col-sm-3">
                                <label>First Name *</label>
                                <input type="text" required class="form-control" name="firstName" pattern="[a-zA-Z][a-zA-Z ]{2,}" title="Must contain characters only and atleast 2 letters" placeholder="Enter your First Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Middle Name</label>
                                <input type="text" class="form-control" name="middleName" pattern="[a-zA-Z][a-zA-Z ]{,2}" title="Must contain characters only" placeholder="Middle Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Last Name *</label>
                                <input type="text" required class="form-control" name="lastName" pattern="[a-zA-Z][a-zA-Z ]{2,}" title="Must contain characters only and atleast 2 letters" placeholder="Enter your Last Name">
                            </div>
                            
                        </div>
                    </div>
                    <div style="clear:both;">
                    </div>

                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-3">
                                <label>Nickname</label>
                                <input type="text" class="form-control" name="nickName" pattern="[a-zA-Z][a-zA-Z ]{2,}" title="Must contain characters only" placeholder="Enter your Nickname">
                            </div>
                            <!-- <div class="col-sm-3">
                                <label>Name of Spouse, if married</label>
                                <input type="text" class="form-control" name="spouseName" pattern="[a-zA-Z][a-zA-Z ]{3,}" title="Must contain characters only and atleast 3 letters" placeholder="Enter your Spouse Name">
                            </div>
 -->
                            <div class="col-sm-4">
                                <label>Birthdate</label>
                                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="birthDate" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="5" name="DOB" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="birthDate" name="DOB" value="" /><br/>
                            </div>
                
                            <div class="col-sm-2">
                                <label>Age *</label>
                                    <input type="text" required class="form-control" pattern="[0-9]{2,3}" title="Numbers only! please input a valid age" name="Age" id="Age" placeholder="Age">
                            </div>

                            <div class="col-sm-3" style="text-align: center;">
                                <label>Gender *</label>
                                <div class="row">
                                    <input type="radio" name="gender" id="1" required value="Male"  />
                                    <span class="radiotext">Male</span>

                                    <input type="radio" name="gender" value="Female" />
                                    <span class="radiotext">Female</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>TIN(Tax Identication Number)</label>
                                <input type="text" class="form-control" name="tinNumber"  pattern="[0-9]{5,9}" title="please input required numbers" placeholder="Enter your Tin">
                            </div>

                            <div class="col-sm-4">
                                <label>Email Address *</label>
                                <input type="email" required class="form-control" name="emailAddress" placeholder="Email Address">
                         <!--  <span class="error"><?php $emailErr;?></span> -->
                            </div>
                            <div class="col-sm-4">
                                <label>Contact Number *</label>
                                <input type="text" required class="form-control" name="contactNo" pattern="[0-9]{11,13}" title="Number Only! Must contain 11 numbers e.g(09999999999)" placeholder="Contact Number">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Address (Blk# Lot#, Ph#, Street, brgy, Municipality, City) *</label>
                                <input type="text" name="homeAddress" required class="form-control"  placeholder="Blk# Lot#, Ph#, Street, brgy, Municipality, City">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Username *</label>
                                <input type="text" name="username" required title="Must contain atleast 8 characters" class="form-control"  minlength="8" placeholder="Enter your username">
                            </div>

                            <div class="col-sm-4">
                                <label>Password *</label>
                                <input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" id="password" placeholder="Enter your password">
                            </div>
                            <div class="col-sm-4">
                                <label>Confirm Password *</label>
                                <input type="password" class="form-control" required minlength="8" id="confirm_password" name='confirm_password' placeholder="confirm password" />
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <h3><strong>EDUCATIONAL QUALIFICATION</strong></h3>
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>College </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Location</label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label>Attainment</label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label>From:</label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label>To:</label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Highschool </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Elementary  </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Vocational </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <h3><strong> OCCUPATION </strong></h3>
                            <div class="col-sm-6 col-sm-offset-1">
                                <label style="text-transform: uppercase;">Present Occupation</label>
                                <input type="text" class="form-control" name="occupation"  pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Your Present occupation">
                                <span class="">*Optional</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <h4><strong>Real Estate Selling Experience</strong></h4>
                            <h5>This is Optional Only</h5>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Name of Realty</label>
                                <input type="text" class="form-control" name="occ_name" pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Name of Realty">
                            </div>

                            <div class="col-sm-3">
                                <label>Position</label>
                                <input type="text" class="form-control" name="occ_pos" pattern="[a-zA-Z][a-zA-Z ]{2,}" placeholder="Your Position">
                            </div>
                             
                            <div class="col-sm-2">
                                <label>From:</label>
                                <input type="text" class="form-control" name="occ_from" pattern="[0-9]{4,4}" title="Please Input a valid year" placeholder="From Year">
                            </div>
                            <div class="col-sm-2">
                                <label>To:</label>
                                <input type="text" class="form-control" name="occ_to" pattern="[0-9]{4,4}" itle="Please Input a valid year" placeholder="To Year">
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <h4><strong>I hereby certify that the above information is true and correct to the best of my knowledge and belief.</strong></h4>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-1">
                                <label>Date of Seminar</label>
                                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="5" name="seminarDate" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            <input type="hidden" id="dtp_input2" name="seminarDate" value="" /><br/>
                            </div>

                            <div class="col-sm-4">
                                <label>Venue of Seminar *</label>
                                <input type="text" class="form-control" required name="seminarVenue" placeholder="Venue of seminar">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Recruited By *</label>
                                <select name="recruitedBy" required id="recruitedBy" class="form-control">
                                    <option value=""> Choose Recruiter </option> 
                                    <?php foreach($recruitedByUser as $rUserData) { ?>
                                    <option value="<?php echo $rUserData['dh_user_id']; ?>" >  <?php echo $rUserData['dh_firstName'].' '.$rUserData['dh_lastName']; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Position</label>
                                <input type="text" readonly id="recuitedByPosition" class="form-control">
                                <input type="hidden" name="recruitedByPos" id="recruitedByPos">
                            </div>

                            <div class="col-sm-2">
                                <label>Division</label>
                                <input type="text" readonly id="recuitedByDivision" class="form-control">
                                <input type="hidden" name="recruitedByDiv" id="recruitedByDiv">
                            </div>
                            <div class="col-sm-3">
                                <label>Name of Trainor/Facilitator</label>
                                <input type="text" class="form-control" name="trainorName" pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Name of the Trainor/Facilitator">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-3 col-sm-offset-1">
                            <label>Sales Director</label>
                            <select name="salesDirector" class="form-control">
                              <option value=" "> Select Sales Director </option>
                              <?php foreach($allSalesDirector as $saleDirData) { ?>
                                <option value="<?php echo $saleDirData['dh_user_id']; ?>" >  <?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName'].' - '.$saleDirData['dh_division_name']; ?> </option>
                              <?php } ?>
                            </select>
                        </div>
                            <div class="col-sm-3">
                                <label>Division Manager</label>
                                <select name="divManager" class="form-control">
                                  <option value=" "> Select Division Manager </option>
                                  <?php foreach($allDivisionManager as $allDivManager) { ?>
                                    <option value="<?php echo $allDivManager['dh_user_id']; ?>" >  <?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName'].' - '.$allDivManager['dh_division_name']; ?> </option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-4">
                                <label>Executive Broker</label>
                                <label style="display:block;vertical-align: middle;position: relative;margin-top: 5px;"> RODELIO D. PARAFINA </label>
                            </div>
                        </div>
                    </div>

                    <br>
                        
                    <div class="col-sm-3 col-sm-offset-5">
                        <div class="form-group clearfix">
                            <input type="hidden" name="userstatus" value="Active">
                            <button type="submit" name="newUser" class="btn btn-primary btn-lg" >Register</button>
                        </div>
                    </div>
                </form>    
        </div>
          <!-- End For New User -->

          <!-- Start For House Projects -->
        <div id="houseProjects" class="tab-pane fade" style="padding:25px;">
            <h2> House Projects </h2>
            <hr>
            <form method="post" role="form" action="commonFunctions.php" enctype="multipart/form-data">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <label>House Name</label>
                        <input type="text" class="form-control" required name="houseName" pattern="[a-zA-Z][a-zA-Z ]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Name">
                     </div>
                     <div class="col-sm-4">
                        <label>House Selling Price</label>
                        <input type="text" class="form-control" required name="houseprice" pattern="[0-9]{6,}" title="Must contain a Numbers Only!" placeholder="House Project Selling Price">
                     </div>
                 </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-8 col-sm-offset-2">
                        <label> Description </label>
                        <span><textarea rows="4" cols="4" class="form-control" name="description"></textarea></span>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-8 col-sm-offset-2 newinput">
                        <label> House Project Finish </label>
                        <button type="button" class="btn btn-info btn-sm addinput"> <span class="glyphicon glyphicon-plus-sign"></span> Add New Input</button>
                        <input type="text" class="form-control" name="finish[]" pattern="[a-zA-Z][a-zA-Z,.- &()]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Finish">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-2 control-label">House Images </label>
                    <div class="col-sm-7">
                        <input type="file" id="houseImageFile" name="houseImageFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-2 control-label"> Floor Plan </label>
                    <div class="col-sm-7">
                        <input type="file" id="floorPlanImgFile" name="floorPlanImgFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <!-- <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-1 control-label"> Amenities </label>
                    <div class="col-sm-8">
                        <input type="file" id="amenitiesFile" name="amenitiesFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div> -->
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <label>Location</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z,. ]{3,}" title="Must contain characters only and atleast minimum of 3 characters" name="location" placeholder="Enter Location">
                     </div>
                     <div class="col-sm-4">
                        <label>Promos</label>
                        <input type="text" class="form-control" title="Must contain characters only and atleast minimum of 3 characters" name="promos" placeholder="Promos" />
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <label>Subdivisions</label>
                        <select name="subd" class="form-control">
                            <?php foreach ($subd as $subdData) { ?>
                            <option value="<?php echo $subdData['dh_subd_id']; ?>"><?php echo $subdData['dh_subd_name']; ?></option>
                            <?php } ?>
                        </select>
                        
                     </div>
                     <div class="col-sm-4">
                        <label> Posted By / Encoded By </label>
                        <input type="text" disabled class="form-control" value="<?php echo $_SESSION['user_fullName']; ?>">
                        <input type="hidden" name="userId" value="<?php echo $_SESSION['user_session']; ?>">
                     </div>
                  </div>
               </div>

               <div class="form-group clearfix">
                  <br>
                  <div class="col-sm-4 col-sm-offset-4">
                     <button type="submit" name="addNewHouseAdmin" class="btn btn-primary btn-lg" style="width:100%"> Submit House Project </button>
                  </div>
               </div>
            </form>            
            <hr>
        </div>
          <!-- End for House Projects -->

        </div>
      </div>
    </div>
    
  </div>

  <!-- Modal -->
  <div id="closingSales" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:85%;top:15%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <table class="table table-condensed table-hover table-striped" id="closingSalesTable">
              <thead>
                <tr>
                  <th> Full Name </th>
                  <th> Phone No </th>
                  <th> Telephone No </th>
                  <th> Complete Address </th>
                  <th> Developer </th>
                  <th> House Model </th>
                  <th> Floor & Lot Area </th>
                  <th> TCP/Checked w/ Dev </th>
                  <th> Reservation Fee </th>
                  <th> Division </th>
                  <th> Closing Sale Date </th>
                  <th> Date Created </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table> 
        </div>
        <div class="modal-footer">
          <div>
        </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="closingSalesEmpty" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:75%; top:15%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <table class="table table-condensed table-hover table-striped">
              <thead>
                <tr>
                  <th> Full Name </th>
                  <th> Phone No </th>
                  <th> Telephone No </th>
                  <th> Complete Address </th>
                  <th> Developer </th>
                  <th> House Model </th>
                  <th> Floor & Lot Area </th>
                  <th> TCP/Checked w/ Dev </th>
                  <th> Reservation Fee </th>
                  <th> Division </th>
                  <th> Closing Sale Date </th>
                  <th> Date Created </th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                      <td colspan="12" style="text-align:center;">
                        <h2 style="color:red;"> No Closing Sales </h2>
                      </td>
                  </tr>
              </tbody>
            </table> 
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <div id="userModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="usermodalText"> </span></b> </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="empId" name="empId">
            <button type="submit" name="userDelete" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="projModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="projmodalText"> </span></b> </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="propFunctions.php">
            <input type="hidden" id="houseId" name="houseId">
            <button type="submit" name="delete_project" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="salesModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="salesmodalText"> </span></b>Closing Sales? </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="salesId" name="salesId">
            <button type="submit" name="salesdelete" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="tripModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="tripmodalText"> </span></b>This Vehicle Tripping Schedule? </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="tripId" name="tripId">
            <button type="submit" name="tripdelete" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

    <div id="semModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="semmodalText"> </span></b>This Seminar Schedule? </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="seminarId" name="seminarId">
            <button type="submit" name="seminardelete" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
    $('#tableproject').DataTable();

    $('#tableuser').DataTable();

    $('#tableclosing').DataTable();

    $('#tabletrip').DataTable();

    $('#tablesem').DataTable();
  });

  $("#tableuser").on('click','.deleteUser',function(){
    var _this = $(this);
    var empId = _this.parent().parent().find('.empId').val();
    var fullName = _this.parent().parent().find('.fullName').text();
    $('#empId').val(empId);
    $('#usermodalText').text(' "DELETE" '+ fullName+'?');
    $('#userModal').modal('show');
   });

  $("#tableproject").on('click','.deleteProject',function(){
    var _this = $(this);
    var houseId = _this.parent().parent().find('.houseId').val();
    var houseName = _this.parent().parent().find('.houseName').text();
    $('#houseId').val(houseId);
    $('#projmodalText').text(' "DELETE" '+ houseName+'?');
    $('#projModal').modal('show');
   });

  $("#tableclosing").on('click','.deleteSales',function(){
    var _this = $(this);
    var salesId = _this.parent().parent().find('.salesId').val();
    var fullName = _this.parent().parent().find('.fullName').text();
    $('#salesId').val(salesId);
    $('#salesmodalText').text(' "DELETE" '+ fullName);
    $('#salesModal').modal('show');
   });

  $("#tabletrip").on('click','.deleteTrip',function(){
    var _this = $(this);
    var tripId = _this.parent().parent().find('.tripId').val();
    var fullName = _this.parent().parent().find('.fullName').text();
    $('#tripId').val(tripId);
    $('#tripmodalText').text(' "DELETE" ');
    $('#tripModal').modal('show');
   });

  $("#tablesem").on('click','.deleteSeminar',function(){
    var _this = $(this);
    var seminarId = _this.parent().parent().find('.seminarId').val();
    var fullName = _this.parent().parent().find('.fullName').text();
    $('#seminarId').val(seminarId);
    $('#semmodalText').text(' "DELETE" ');
    $('#semModal').modal('show');
   });

</script>
<script type="text/javascript">
   $(document).ready(function(){
      $('.addinput').on('click', function(){
         $('.newinput').append('<input type="text" class="form-control" name="finish[]" pattern="[a-zA-Z][a-zA-Z,.- &()]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Finish">')
      })


   });
     
  </script>
<script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
        $('.form_date').datetimepicker({
                language:  'ar',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
            });
    </script>
    <script>
    $(document).ready(function(){
        $("#tableuser").on('click','.viewClosing',function(){
          var _this = $(this);
          var empId = _this.parent().parent().find('.empId').val();
            $.ajax({
          type: 'POST',
          url: '../website/commonFunctions.php',
          data: {
              'getClosing': empId
          },
          dataType: 'json',
          success: function(data){
              
              if(data.length > 0){
                  $('#closingSalesTable tbody tr').remove();
                  for (var i = 0; i <= data.length - 1; i++) {
                    $('#closingSalesTable tbody').append("<tr><td>"+ data[i].dh_Firstname +' '+ data[i].dh_familyName + "</td><td>"+ data[i].dh_phoneNo + "</td><td>" + data[i].dh_telNo + "</td><td>" +'blk '+ data[i].dh_block +' Lot ' + data[i].dh_lot + ' ph ' + data[i].dh_phase + ', ' + data[i].dh_subdivision + ', ' + data[i].dh_location + "</td><td>" + data[i].dh_developer + "</td><td>" + data[i].dh_houseModel + "</td><td>" + data[i].dh_floorArea +'sqm - ' + data[i].dh_lotArea +'sqm' + "</td><td>₱" + data[i].dh_tcpDev + "</td><td>₱" + data[i].dh_reservationFee + "</td><td>" + data[i].dh_division + "</td><td>" + data[i].dh_closingDate + "</td><td>" + data[i].dh_created_date + "</td></tr>");
                  }
                  $('#closingSales').modal('show');
              } else {
                  $('#closingSalesEmpty').modal('show');
              }
            }
          });

       
      });

        $('#recruitedBy').on('change',function(){
          var _this = $(this).val();
          $.ajax({
            type: 'POST',
            url: '../website/commonFunctions.php',
            data: {
              'id': _this
            },
            dataType: 'json',
            success: function(data){
              $('#recruitedByPos').val(data.dh_user_group_id);
              $('#recuitedByPosition').val(data.dh_user_group_name);

              $('#recruitedByDiv').val(data.dh_division_id);
              $('#recuitedByDivision').val(data.dh_division_name);
            }
          });
        });
    });
    </script>
    <script>
    $(document).ready(function(){
      $('#errMsg').fadeOut(5000); 
      // hide #back-top first
      $("#back-top").hide();
      
      // fade in #back-top
      $(function () {
        $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
            $('#back-top').fadeIn();
          } else {
            $('#back-top').fadeOut();
          }
        });

        // scroll body to 0px on click
        $('#back-top a').click(function () {
          $('body,html').animate({
            scrollTop: 0
          }, 800);
          return false;
        });
      });

    });
    </script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>