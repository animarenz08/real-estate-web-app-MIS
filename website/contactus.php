<?php
	session_start();
	$currentMenu = 8;

?>

<?php
if(isset($_POST['contactform'])){
  $msg = $_POST['userMsg'];
  $name = $_POST['userName'];
  $email = $_POST['userEmail'];
  $phone = $_POST['userPhone'];
$to = 'animarenz08@gmail.com'; // note the comma

// Subject
$subject = 'Message Feedback';

// Message
$message = '
<html>
<head>
  <title>Message Feedback</title>
</head>
<body>
  <h4> Message From Mr/Ms. ' . $name . '</h4>
  <p>'. $msg .'</p>
  <h5>My Contact Number:'. $phone .'</h5>

</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers

$headers[] = 'From: Dream House Realty <' . $email . '>';

// Mail it
mail($to, $subject, $message, implode("\r\n", $headers));
}
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
  <?php include 'mainHeader.php'; ?>


	<div class="container">
  		<div class="row">
  			<div class="col-sm-4">
            <h2>Address :</h2>
				<h3>MARIKINA BUSINESS OFFICE:</h3>
				<p>No. 7 Maroon St., Bonita </p>
				<p>Homes Concepcion II, Marikina City</p>
				<p>Telefax: (02)948-1288</p>
				<h3>TANZA, CAVITE BUSINESS OFFICE:</h3>
				<p>B3 L1 Villa Tanza Subdivision, Biga Tanza Cavite </p>
				<p>Tel. No.:(046)402-3250</p>
				<h3>IMUS, CAVITE BUSINESS OFFICE:</h3>
				<p>Unit#9 Pillars Executive Homes Carsadong Bago, Alapaan, Imus Cavite </p>
				<p>Tel. No.:(02)546-6396</p>	
			</div>
  		<div class="col-sm-7 col-sm-offset-1">
      	<h3>Find Us Here</h3>
      	<div class="map">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3864.317014199137!2d120.92309651326548!3d14.408879989924797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d31c31e89c79%3A0x5aad153b42510447!2sPillars+Executive+Homes!5e0!3m2!1sen!2sph!4v1503933153643" width="100%" height="410" frameborder="0" style="border:0" allowfullscreen></iframe>
         </div>
      </div>
    	</div>

   <div style="clear: both;"></div>
    	<br><br>

   <div class="col-sm-4 col-sm-offset-5">
   	<h3>Contact Us</h3>
   </div>
      <form method="post" action="">
      	<div class="form-group">
        		<div class="row">
        			<div class="col-sm-6 col-sm-offset-3">
        				<span><label>NAME</label></span>
						<span><input name="userName" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{3,}" title="Must contain characters only and atleast 3 letters" type="text" class="textbox"></span>
					</div>
				</div>
         </div>
         <div class="form-group">
          	<div class="row">
          		<div class="col-sm-6 col-sm-offset-3">
						<span><label>E-MAIL</label></span>
						<span><input name="userEmail" class="form-control" type="email" class="textbox"></span>
              	</div>
            </div>
			</div>
			<div class="form-group">
          	<div class="row">
          		<div class="col-sm-6 col-sm-offset-3">
						<span><label>MOBILE NO.</label></span>
						<span><input name="userPhone" class="form-control" type="text" pattern="[0-9]{11,13}" title="Number Only! Must contain 11 numbers e.g(09999999999)" class="textbox"></span>
              	</div>
            </div>
			</div>
			<div class="form-group">
          	<div class="row">
          		<div class="col-sm-6 col-sm-offset-3">
						<span><label>Message</label></span>
						<span><textarea style="height:200px;" class="form-control" name="userMsg"> </textarea></span>
              	</div>
            </div>
			</div>
			<div class="form-group clearfix">
          	<div class="col-sm-7 col-sm-offset-4">
					<button style="width: 50%;" type="submit" name="contactform" class="btn btn-primary btn-lg" >Submit</button>
            </div>
			</div>
		</form>
	</div>


    <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>