<?php
  include '../common/class.users.php';
	session_start();
	$currentMenu = 54;
    $userGroup = 5;


  $user = new User();
  $status = 'For Activation';

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $getDevelopers = $user->getDevelopers();
  $alluserData = $user->getAllUser($status);
  $alluserData2 = $user->getActiveUsersExceptEmpAdmin();
  $allSalesDirector = $user->getAllActiveSalesDirector();
  $allSalesAgent = $user->getAllActiveSalesAgent();
  $allDivisionManager = $user->getAllActiveDivisionManager();
  $division = $user->getAllDivision();
  $recruitedByUser = $user->getAllSalesAndDiv();
  $subd = $user->getAllSubdivision();
  $group = $user->getAlluserGroupsexceptempAdmin();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
    <?php if(isset($Message)){ ?>
        <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
      &nbsp; <?php echo $Message; ?>!</div>
    <?php unset($_SESSION["Message"]); } ?>

    <h2 style="text-align:center; text-transform: uppercase;margin:0;">  </h2>
    <br>
    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#newUser"> Add New User </a></li>
              <li><a data-toggle="tab" href="#houseProjects"> House Projects </a></li>
              <li><a data-toggle="tab" href="#news"> News / Article </a></li>
              <li><a data-toggle="tab" href="#closingForm"> Closing Form </a></li>
          </ul>
        </div>
        <div class="tab-content">

          <!-- Start For Sales Director -->
        <div id="newUser" class="tab-pane fade in active" style="padding:25px">

              <hr>
                <form method="post" action="commonFunctions.php">

                    <div style="clear:both;"></div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Position *</label>
                                <select name="userType" required title="Please select one of this option" class="form-control">
                                    <?php foreach ($group as $groupuser) { ?>
                                    <?php if($groupuser['dh_user_group_id'] < '5') { ?>
                                    <option value="<?php echo $groupuser['dh_user_group_id']; ?>"><?php echo $groupuser['dh_user_group_name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>ID Number</label>
                                <input type="text" required class="form-control" pattern="[0-9]{7,10}" title="Must contain numbers only, minimum of 7 numbers and maximum of 10 numbers" name="userId" placeholder="Enter your ID/RFID Number">
                            </div>
                            <div class="col-sm-3">
                                <label>First Name</label>
                                <input type="text" required class="form-control" name="firstName" pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" placeholder="Enter your First Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Middle Name</label>
                                <input type="text" class="form-control" name="middleName" pattern="[a-zA-Z][a-zA-Z ]{2,15}" title="Must contain characters only" placeholder="Middle Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Last Name</label>
                                <input type="text" required class="form-control" name="lastName" pattern="[a-zA-Z][a-zA-Z ]{2,15}" title="Must contain characters only and atleast 2 letters" placeholder="Enter your Last Name">
                            </div>
                            <!-- <div class="col-sm-3">
                                <label>Name of Spouse, if married</label>
                                <input type="text" class="form-control" name="spouseName" pattern="[a-zA-Z][a-zA-Z ]{3,}" title="Must contain characters only and atleast 3 letters" placeholder="Enter your Spouse Name">
                            </div> -->
                        </div>
                    </div>
                    <div style="clear:both;">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Nickname</label>
                                <input type="text" class="form-control" name="nickName" pattern="[a-zA-Z][a-zA-Z ]{2,}" title="Must contain characters only" placeholder="Enter your Nickname">
                            </div>

                            <div class="col-sm-4">
                                <label>Birthdate</label>
                                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="5" name="DOB" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="dtp_input1" name="DOB" value="" /><br/>
                            </div>
                
                            <div class="col-sm-2">
                                <label>Age</label>
                                    <input type="text" required class="form-control" pattern="[0-9]{2,2}" title="Numbers only! please input a valid age" name="Age" id="Age" placeholder="Age">
                            </div>

                            <div class="col-sm-3" style="text-align: center;">
                                <label>Gender</label>
                                <div class="row">
                                    <input type="radio" name="gender" id="1" required value="Male"  />
                                    <span class="radiotext">Male</span>

                                    <input type="radio" name="gender" value="Female" />
                                    <span class="radiotext">Female</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>TIN(Tax Identication Number)</label>
                                <input type="text" class="form-control" name="tinNumber"  pattern="[0-9]{5,9}" title="please input required numbers" placeholder="Enter your Tin">
                            </div>

                            <div class="col-sm-4">
                                <label>Email Address</label>
                                <input type="email" required class="form-control" name="emailAddress" placeholder="Email Address">
                         <!--  <span class="error"><?php $emailErr;?></span> -->
                            </div>
                            <div class="col-sm-4">
                                <label>Contact Number</label>
                                <input type="text" required class="form-control" name="contactNo" pattern="[0-9]{10,11}" title="Must contain numbers only and minimum of 10 numbers and maximum of 11 numbers" placeholder="Contact Number">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Address</label>
                                <input type="text" name="homeAddress" required class="form-control"  placeholder="Blk# Lot#, Ph#, Street, brgy, Municipality, City">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Employee Username</label>
                                <input type="text" name="username" required class="form-control"  minlength="8" placeholder="Enter your username">
                            </div>

                            <div class="col-sm-4">
                                <label>Password</label>
                                <input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" id="password" placeholder="Enter your password">
                            </div>
                            <div class="col-sm-4">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" required minlength="8" id="confirm_password" name='confirm_password' placeholder="confirm password" />
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <h3><strong>EDUCATIONAL QUALIFICATION</strong></h3>
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>College </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label>Location</label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label>Attainment</label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label>From:</label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label>To:</label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Highschool </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Elementary  </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label> Vocational </label>
                                <input type="text" class="form-control" name="educSchool[]" pattern="[a-zA-Z][a-zA-Z ]{5,}" placeholder="School Name">
                            </div>
                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educLocation[]" placeholder="Location of your School">
                            </div>
                            <div class="col-sm-2">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educAttainment[]" placeholder="Course/Graduate/Undergraduate">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearFrom[]" maxlength="4" pattern="[0-9]{4,4}" title="Must contain a numbers only" placeholder="From">
                            </div>
                            <div class="col-sm-1">
                                <label> &nbsp; </label>
                                <input type="text" class="form-control" name="educYearTo[]" title="Must contain a numbers only" pattern="[0-9]{4,4}" maxlength="4" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <h3><strong> OCCUPATION </strong></h3>
                            <div class="col-sm-6 col-sm-offset-1">
                                <label style="text-transform: uppercase;">Present Occupation</label>
                                <input type="text" class="form-control" name="occupation"  pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Your Present occupation">
                                <span class="">*Optional</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <h4><strong>Real Estate Selling Experience</strong></h4>
                            <h5>This is Optional Only</h5>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Name of Realty</label>
                                <input type="text" class="form-control" name="occ_name" pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Name of Realty">
                            </div>

                            <div class="col-sm-3">
                                <label>Position</label>
                                <input type="text" class="form-control" name="occ_pos" pattern="[a-zA-Z][a-zA-Z ]{2,}" placeholder="Your Position">
                            </div>
                             
                            <div class="col-sm-2">
                                <label>From:</label>
                                <input type="text" class="form-control" name="occ_from" pattern="[0-9]{4,4}" placeholder="From Year">
                            </div>
                            <div class="col-sm-2">
                                <label>To:</label>
                                <input type="text" class="form-control" name="occ_to" pattern="[0-9]{4,4}" placeholder="To Year">
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <h4><strong>I hereby certify that the above information is true and correct to the best of my knowledge and belief.</strong></h4>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-1">
                                <label>Date of Seminar</label>
                                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd yyyy" data-link-field="dtp_input2" data-link-format="mm-dd-yyyy">
                                    <input class="form-control" size="5" name="seminarDate" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            <input type="hidden" id="dtp_input2" name="seminarDate" value="" /><br/>
                            </div>

                            <div class="col-sm-4">
                                <label>Venue of Seminar</label>
                                <input type="text" class="form-control" name="seminarVenue" placeholder="Venue of seminar">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Recruited By</label>
                                <select name="recruitedBy" id="recruitedBy" class="form-control">
                                    <option value=""> Choose Recruiter </option> 
                                    <?php foreach($recruitedByUser as $rUserData) { ?>
                                    <option value="<?php echo $rUserData['dh_user_id']; ?>" >  <?php echo $rUserData['dh_firstName'].' '.$rUserData['dh_lastName']; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Position</label>
                                <input type="text" readonly id="recuitedByPosition" class="form-control">
                                <input type="hidden" name="recruitedByPos" id="recruitedByPos">
                            </div>

                            <div class="col-sm-2">
                                <label>Division</label>
                                <input type="text" readonly id="recuitedByDivision" class="form-control">
                                <input type="hidden" name="recruitedByDiv" id="recruitedByDiv">
                            </div>
                            <div class="col-sm-3">
                                <label>Name of Trainor/Facilitator</label>
                                <input type="text" class="form-control" name="trainorName" pattern="[a-zA-Z][a-zA-Z ]{4,}" placeholder="Name of the Trainor/Facilitator">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Sales Director</label>
                                <select name="salesDirector" class="form-control">
                                  <option value=" "> Select Sales Director </option>
                                  <?php foreach($allSalesDirector as $saleDirData) { ?>
                                    <option value="<?php echo $saleDirData['dh_user_id']; ?>" >  <?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName'].' - '.$saleDirData['dh_division_name']; ?> </option>
                                  <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Division Manager</label>
                                <select name="divManager" class="form-control">
                                  <option value=" "> Select Division Manager </option>
                                  <?php foreach($allDivisionManager as $allDivManager) { ?>
                                    <option value="<?php echo $allDivManager['dh_user_id']; ?>" >  <?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName'].' - '.$allDivManager['dh_division_name']; ?> </option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-4">
                                <label>Executive Broker</label>
                                <label style="display:block;vertical-align: middle;position: relative;margin-top: 5px;"> RODELIO D. PARAFINA </label>
                            </div>
                        </div>
                    </div>

                    <br>
                        
                    <div class="col-sm-3 col-sm-offset-5">
                        <div class="form-group clearfix">
                            <button type="submit" name="empnewUser" class="btn btn-primary btn-lg" >Register</button>
                        </div>
                    </div>
                </form>    
        </div>
          <!-- End For Sales Director -->

          <!-- Start For House Projects -->
        <div id="houseProjects" class="tab-pane fade" style="padding:25px;">
            <h2> House Projects </h2>
            <hr>
            <form method="post" role="form" action="commonFunctions.php" enctype="multipart/form-data">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <label>House Name</label>
                        <input type="text" class="form-control" required name="houseName" pattern="[a-zA-Z][a-zA-Z ]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Name">
                     </div>
                     <div class="col-sm-4">
                        <label>House Selling Price</label>
                        <input type="text" class="form-control" required name="houseprice" pattern="[0-9]{6,}" title="Must contain a Numbers Only!" placeholder="House Project Selling Price">
                     </div>
                 </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-8 col-sm-offset-2">
                        <label> Description </label>
                        <span><textarea rows="4" cols="4" class="form-control" name="description"></textarea></span>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-8 col-sm-offset-2 newinput">
                        <label> House Project Finish </label>
                        <button type="button" class="btn btn-info btn-sm addinput"> <span class="glyphicon glyphicon-plus-sign"></span> Add New Input</button>
                        <input type="text" class="form-control" name="finish[]" pattern="[a-zA-Z][a-zA-Z,.- &()]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Finish">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-2 control-label">House Images </label>
                    <div class="col-sm-7">
                        <input type="file" id="houseImageFile" name="houseImageFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-2 control-label"> Floor Plan </label>
                    <div class="col-sm-7">
                        <input type="file" id="floorPlanImgFile" name="floorPlanImgFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <!-- <div class="form-group">
                  <div class="row">
                    <label class="col-sm-3 col-sm-offset-1 control-label"> Amenities </label>
                    <div class="col-sm-8">
                        <input type="file" id="amenitiesFile" name="amenitiesFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div> -->
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <label>Location</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z,. ]{3,}" title="Must contain characters only and atleast minimum of 3 characters" name="location" placeholder="Enter Location">
                     </div>
                     <div class="col-sm-4">
                        <label>Promos</label>
                        <input type="text" class="form-control" title="Must contain characters only and atleast minimum of 3 characters" name="promos" placeholder="Promos" />
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <label>Subdivisions</label>
                        <select name="subd" class="form-control">
                            <?php foreach ($subd as $subdData) { ?>
                            <option value="<?php echo $subdData['dh_subd_id']; ?>"><?php echo $subdData['dh_subd_name']; ?></option>
                            <?php } ?>
                        </select>
                        
                     </div>
                     <div class="col-sm-4">
                        <label> Posted By / Encoded By </label>
                        <input type="text" disabled class="form-control" value="<?php echo $_SESSION['user_fullName']; ?>">
                        <input type="hidden" name="userId" value="<?php echo $_SESSION['user_session']; ?>">
                     </div>
                  </div>
               </div>

               <div class="form-group clearfix">
                  <br>
                  <div class="col-sm-4 col-sm-offset-4">
                     <button type="submit" name="addNewHouse" class="btn btn-primary btn-lg" style="width:100%"> Submit House Project </button>
                  </div>
               </div>
            </form>            
            <hr>
        </div>
          <!-- End for House Projects -->

          <!-- Start For News Articles -->
        <div id="news" class="tab-pane fade" style="padding:25px;">
            <h2> News Articles </h2>
            <hr>
            <form method="post" role="form" action="commonFunctions.php" enctype="multipart/form-data">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-4">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Enter News Title">
                     </div>
                 </div>
             </div>
             <div class="form-group">
                <div class="row">
                     <div class="col-sm-4 col-sm-offset-4">
                        <label>Content</label>
                        <span><textarea rows="8" cols="4" class="form-control" name="content"></textarea></span>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label class="col-sm-2 col-sm-offset-4 control-label"> Add News Images </label>
                     <div class="col-sm-3">
                        <input type="file" id="newsImgFile" name="newsImgFile[]" multiple="multiple" accept="image/*" required/>
                    </div>
                  </div>
               </div>
               <br>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 col-sm-offset-4">
                        <label> Posted By / Encoded By </label>
                        <input type="text" disabled class="form-control" value="<?php echo $_SESSION['user_fullName']; ?>">
                        <input type="hidden" name="userId" value="<?php echo $_SESSION['user_session']; ?>">
                     </div>
                  </div>
               </div>
               <div class="form-group clearfix">
                  <br>
                  <div class="col-sm-4 col-sm-offset-4">
                     <button type="submit" name="addNews" class="btn btn-primary btn-lg" style="width:100%"> Submit News </button>
                  </div>
               </div>
            </form>
        </div>
          <!-- End for News Articles -->

          <!-- Start For Closing Form -->
        <div id="closingForm" class="tab-pane fade" style="padding:25px;">
            <h2> Closing Form </h2>
            <hr>
            <form method="post" role="form" action="commonFunctions.php">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Family Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="familyname" placeholder="Enter your Last Name">
                     </div>
                     <div class="col-sm-3">
                        <label>First Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="firstname" placeholder="Enter your First Name">
                     </div>
                     <div class="col-sm-3">
                        <label>Cellphone #</label>
                        <input type="text" class="form-control" pattern="[0-9]{10,11}" title="Must contain numbers only and minimum of 10 numbers and maximum of 11 numbers" name="phoneno" placeholder="Enter your Phone No">
                     </div>
                     <div class="col-sm-3">
                        <label>Telephone #</label>
                        <input type="text" class="form-control" pattern="[0-9]{7,7}" title="Must contain 7 numbers only" name="telno" placeholder="Enter your Tel No">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Subdivision</label>
                       <select id="subdivision" name="subdivision" required class="form-control" onchange="subdname()">
                            <option value=""> Select Subdivision </option>
                            <?php foreach ($subd as $subdData) { ?>
                            <option value="<?php echo $subdData['dh_subd_name']; ?>"><?php echo $subdData['dh_subd_name']; ?></option>
                            <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-3">
                        <label>Location</label>
                        <input type="text" class="form-control" id="location" name="location" placeholder="Choose a subdivision" readonly />
                     </div>
                     <div class="col-sm-3">
                        <label>Developer</label>
                        <input type="text" class="form-control" id="developer" name="developer" placeholder="Choose a subdivision" readonly />
                     </div>
                     <div class="col-sm-3">
                        <label>House Model</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{3,30}" itle="Must contain characters only and atleast 3 letters" name="housemodel" placeholder="Enter the House model">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     
                     <div class="col-sm-2">
                        <label>Phase #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="phase" placeholder="phase number" />
                     </div>
                     <div class="col-sm-2">
                        <label>Blk #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="block" placeholder="block mumber" />
                     </div>
                     <div class="col-sm-2">
                        <label>Lot #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="lot" placeholder="lot number" />
                     </div>
                     <div class="col-sm-3">
                        <label>Floor Area</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="floorarea" placeholder="Enter the floor area" />(sqm.)
                     </div>
                     <div class="col-sm-3">
                        <label>Lot Area</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="lotarea" placeholder="Enter the lot area" />(sqm.)
                     </div>                     
                  </div>
               </div>
                <br>
                <br>
               <div class="form-group">
                  <div class="row">
                    <div class="col-sm-3">
                        <label>TCP/Checked w/ Dev</label>
                        <input type="text" class="form-control" name="tcpdev" required pattern="[0-9]{5,}" title="Must contain numbers only" placeholder="TCP/Checked w/ Dev">
                     </div>
                     <div class="col-sm-2">
                        <label>Reservation Fee</label>
                        <input type="text" class="form-control" pattern="[0-9]{4,}" title="Must contain numbers only" name="reservationfee" placeholder="Amount of reservation">
                     </div>
                     <div class="col-sm-2">
                        <label>OI/PR #</label>
                        <input type="text" class="form-control" pattern="[0-9]{4,}" title="Must contain numbers only" name="oipr" placeholder="OI/PR #" />
                     </div>
                     <div class="col-sm-3">
                        <label> Closing Date </label>
                            <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="closingDateInput" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="5" name="closingDate" type="text" value="" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        <input type="hidden" id="closingDateInput" name="closingDate" value="" /><br/>    
                     </div>
                     <div class="col-sm-2">
                        <label>Division</label>
                        <select name="division" class="form-control">
                            <option value=''> Select Division </option>
                            <?php foreach($division as $divData){ ?>
                            <option value="<?php echo $divData['dh_division_id']; ?>"> <?php echo $divData['dh_division_name']; ?> </option>
                            <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <br>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Division Manager</label>
                        <select name="divisionmanager" class="form-control">
                          <option value=''> Select Division Manager </option>
                          <?php foreach($allDivisionManager as $allDivManager) { ?>
                            <option value="<?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName']; ?>" >  <?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName'].' - '.$allDivManager['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="divcomrate" placeholder="e.g 0.01%">
                    </div>
                     <div class="col-sm-3 col-sm-offset-1">
                        <label>Sales Director</label>
                        <select name="salesdirector" class="form-control">
                          <option value=''> Select Sales Director </option>
                          <?php foreach($allSalesDirector as $saleDirData) { ?>
                            <option value="<?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName']; ?>" >  <?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName'].' - '.$saleDirData['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="dircomrate" placeholder="e.g 0.01%">
                    </div>
                 </div>
             </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Sales Trainee/Agent/Referral</label>
                         <select name="salestrainee" class="form-control">
                          <option value=''> Select Sales Trainee/Agent/Referral </option>
                          <?php foreach($allSalesAgent as $salesData) { ?>
                            <option value="<?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName']; ?>" >  <?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName'].' - '.$salesData['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="salescomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Division Manager</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdiv" placeholder="Enter RC Division Manager">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdivcomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RC Sales Director</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdir" placeholder="Enter RC Sales Director">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdircomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Sales Trainee</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcsales" placeholder="Enter RC Sales Trainee">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcsalescomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RFA</label>
                        <input type="text" class="form-control" name="rfa" placeholder="Enter RFA">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rfacomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>Booking Assistant</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="bookassist" placeholder="Enter Booking Assistant">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="bookcomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Assistant Agent</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" title="Must contain characters only and atleast 4 letters" name="aassist" placeholder="Enter A Assistant">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="acomrate" placeholder="e.g 0.01%">
                    </div>
                     <div class="col-sm-3 col-sm-offset-1">
                        <label>Prepared By</label>
                        <select name="preparedBy" required class="form-control">
                          <option value=''> Select User </option>
                          <?php foreach($alluserData2 as $userData2) { ?>
                            <option value="<?php echo $userData2['dh_user_id']; ?>" >  <?php echo $userData2['dh_firstName'].' '.$userData2['dh_lastName']; ?> </option>
                          <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-group clearfix">
                  <br>
                  <div class="col-sm-4 col-sm-offset-4">
                     <input type="hidden" name="userId" value="<?php echo $_SESSION['user_session']; ?>" >
                     <button type="submit" name="closing" class="btn btn-primary btn-lg" style="width:100%">Closing Submit</button>
                  </div>
               </div>
            </form>
        </div>
          <!-- End for Closing Form -->
        </div>
      </div>
    </div>
    
  </div>

  <!-- Modal -->
  <div id="ApprovalModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="modalText"> </span></b></p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="empId">
            <button type="submit" name="approvalAccount" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
  <script type="text/javascript">
   $(document).ready(function(){
      $('.addinput').on('click', function(){
         $('.newinput').append('<input type="text" class="form-control" name="finish[]" pattern="[a-zA-Z][a-zA-Z,.- &()]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Finish">')
      })


   });
     
  </script>
    <script type="text/javascript">
        function subdname() {
            var x = document.getElementById("subdivision").value;
            $.ajax({
                type: 'POST',
                url: '../website/commonFunctions.php',
                data: {
                    'subdname': x
                    },
                dataType: 'json',
                success: function(data){
                    $('#location').val(data.dh_subd_location);
                    $('#developer').val(data.dh_dev_name);
                    $('#subdsid').val(data.dh_subd_id);
                }
            });
        }
    </script>
  <script type="text/javascript">
        $('.form_date').datetimepicker({
                language:  'ar',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
            });
    </script>
    <script>
      $(document).ready(function(){
        $('#recruitedBy').on('change',function(){
            var _this = $(this).val();
            $.ajax({
                type: 'POST',
                url: '../website/commonFunctions.php',
                data: {
                    'id': _this
                },
                dataType: 'json',
                success: function(data){
                    $('#recruitedByPos').val(data.dh_user_group_id);
                    $('#recuitedByPosition').val(data.dh_user_group_name);

                    $('#recruitedByDiv').val(data.dh_division_id);
                    $('#recuitedByDivision').val(data.dh_division_name);
                }
            });
        });

        $('.approval').each(function(){
          var _this = $(this);
          _this.on('click',function(){
            var fullName = _this.parent().parent().find('.fullNameText').text();
            var empId = _this.parent().parent().find('.empId').val();
            $('#empId').val(empId);
            $('#modalText').text('Approve '+ fullName +' as Sales Trainee / Referral? ');
            $('#ApprovalModal').modal('show');
            
          });
        });

        $('.decline').each(function(){
          var _this = $(this);
          _this.on('click',function(){
            var fullName = _this.parent().parent().find('.fullNameText').text();
            var empId = _this.parent().parent().find('.empId').val();
            $('#empId').val(empId);
            $('#modalText').text('Decline '+ fullName +' Sales Trainee / Referral registration? ');
            $('#ApprovalModal').modal('show');
            
          });
        });

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>