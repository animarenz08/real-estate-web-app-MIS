<?php 
$getCurrentMenu = $currentMenu;

  if(isset($_SESSION['Message'])){
    $Message = $_SESSION['Message'];
    if(isset($_SESSION['MsgCode'])) {
      $MsgCode = $_SESSION['MsgCode'];
    } else {
      $MsgCode = 0;
    }
  }

  if(isset($_SESSION['user_type']) != ""){
    $userSession = $_SESSION['user_session'];
    $userType = $_SESSION['user_type'];
    $userName = $_SESSION['user_fullName'];
  } else {
    $userType = '';
    $userSession = '';
    $userName = '';
  }
?> 
<div class="wrap">
    <div class="top-head-grids">
      <img src="img/logodhrs1.jpg" />
    </div>
      <div class="top-head-grid">
        <h1>DREAM HOUSE REALTY</h1>
        <h2>The Real Estate Marketing Network</h2>
      </div>
  </div>

  <div style="clear:both">
  </div>

  <?php if($userType == '') { ?>

    <div class="wraps">
      <div class="social-icons">
        <div class="col-sm-offset-9">
          <ul id="horizontal-list">
            <li><a href="#"><img src="img/fb.png" /></a></li>
            <li><a href="#"><img src="img/twit.png" /></a></li>
            <li><a href="#"><img src="img/gp.png" /></a></li>
          </ul>
        </div>
      </div>
    </div>

       <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">
      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 2){ ?> class="active" <?php }; ?> ><a href="aboutus.php">About Us</a></li>
            <li <?php if($getCurrentMenu == 3){ ?> class="active" <?php }; ?> ><a href="affildeveloper.php">Affiliated developers</a></li>
            <li <?php if($getCurrentMenu == 4){ ?> class="active" <?php }; ?> ><a href="projects.php">Projects</a></li>
            <li <?php if($getCurrentMenu == 5){ ?> class="active" <?php }; ?> ><a href="calculator.php">Calculator</a></li>
            <li <?php if($getCurrentMenu == 8){ ?> class="active" <?php }; ?> ><a href="contactus.php">Contact Us</a></li>
            <li <?php if($getCurrentMenu == 9){ ?> class="active" <?php }; ?> ><a href="login.php">Login</a></li>
            <li <?php if($getCurrentMenu == 10){ ?> class="active" <?php }; ?> ><a href="rfidscan.php">Rfid Scan</a></li>
            <li>
            </li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
          </div>
        </div>
    </nav>
  <?php } else { ?>
    <div class="wraps">
      <div class="social-icons">
        <div class="col-sm-offset-8">
          <ul id="horizontal-list">
            
            <li><?php echo date("F j, Y"); ?> | <strong>Welcome!</strong></li>
              <li class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-animations="rubberBand fadeInRight fadeInUp fadeInLeft"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($userName); ?> <span class="glyphicon glyphicon-chevron-down rotate"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="logout.php">Log Out</a></li>
                </ul>
              </li>
          </ul>
        </div>
      </div>
    </div>
    <div style="clear:both"></div>
  <?php } ?>

  <?php if ($userType == '4') { ?>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 42){ ?> class="active" <?php }; ?> ><a href="salesView.php">View</a ></li>
            <li <?php if($getCurrentMenu == 43){ ?> class="active" <?php }; ?> ><a href="salesRequestSched.php">Request Schedule</a ></li>
            <li <?php if($getCurrentMenu == 44){ ?> class="active" <?php }; ?> ><a href="salesEncode.php">Add/Submit</a ></li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
        </div>
      </div>
    </nav>

  <?php } else if ($userType == '3') { ?>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">
      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 32){ ?> class="active" <?php }; ?> ><a href="dirView.php"> View </a></li>
            <li <?php if($getCurrentMenu == 33){ ?> class="active" <?php }; ?> ><a href="dirRequestSched.php"> Request Schedule </a></li>
            <li <?php if($getCurrentMenu == 34){ ?> class="active" <?php }; ?> ><a href="dirEncode.php"> Add/Submit </a></li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
        </div>
      </div>
    </nav>

  <?php } else if ($userType == '2') { ?>

    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 22){ ?> class="active" <?php }; ?> ><a href="divView.php"> View </a></li>
            <li <?php if($getCurrentMenu == 23){ ?> class="active" <?php }; ?> ><a href="divRequestSched.php"> Request Schedule </a></li>
            <li <?php if($getCurrentMenu == 24){ ?> class="active" <?php }; ?> ><a href="divSendSMS.php"> Send SMS </a></li>
            <li <?php if($getCurrentMenu == 25){ ?> class="active" <?php }; ?> ><a href="divEncode.php"> Add/Submit </a></li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
        </div>
      </div>
    </nav>

    <?php } else if ($userType == '1') { ?>

    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
           <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
           <li <?php if($getCurrentMenu == 12){ ?> class="active" <?php }; ?> ><a href="adminApproval.php">Approvals</a></li>
           <li <?php if($getCurrentMenu == 13){ ?> class="active" <?php }; ?> ><a href="adminView.php">View</a></li>
           <li <?php if($getCurrentMenu == 14){ ?> class="active" <?php }; ?> ><a href="adminSchedule.php">Schedules</a></li>
           <li <?php if($getCurrentMenu == 15){ ?> class="active" <?php }; ?> ><a href="adminMaintenance.php">File Maintenance</a></li>
           <!-- <li <a href="adminReport.php">Reports</a></li> -->
           <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="rubberBand fadeInRight fadeInUp fadeInLeft" href="#">Reports <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li <?php if($getCurrentMenu == 16){ ?> class="active" <?php }; ?> ><a href="reportbyDate.php">By Date Range</a></li>
                <li <?php if($getCurrentMenu == 17){ ?> class="active" <?php }; ?> ><a href="reportbyAnnual.php">By Annual</a></li>
                <li <?php if($getCurrentMenu == 18){ ?> class="active" <?php }; ?> ><a href="reportbyDaily.php">By Daily</a></li>
              </ul/>
            </li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
        </div>
      </div>
    </nav>

  <?php } else if ($userType == '5') { ?>
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 52){ ?> class="active" <?php }; ?> ><a href="empView.php"> View </a></li>
            <li <?php if($getCurrentMenu == 53){ ?> class="active" <?php }; ?> ><a href="empApproval.php"> Approvals </a></li>
            <li <?php if($getCurrentMenu == 54){ ?> class="active" <?php }; ?> ><a href="empEncode.php"> Add New </a></li>
            <li <?php if($getCurrentMenu == 55){ ?> class="active" <?php }; ?> ><a href="empMaintenance.php"> File Maintenance </a></li>
          </ul>
          <form method="POST" action="search.php" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" name="searchbtn" class="btn btn-warning"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form><!--/.nav-collapse -->
        </div>
      </div>
    </nav>


  <?php } ?>
 <div class="clear"> </div>
  <!--End-wrap-->

