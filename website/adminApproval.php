<?php
  include '../common/class.users.php';
	session_start();
	$currentMenu = 12;
  $userGroup = 1;


  $user = new User();
  $status = 'For Activation';

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $alluserData = $user->getAllUsers($status);
  $getSeminar = $user->getAllSeminarRequest('For Approval');
  $getVehicleTrip = $user->getAllVehicleTripRequest('For Approval');


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
    <?php if(isset($Message)){ ?>
        <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
      &nbsp; <?php echo $Message; ?>!</div>
    <?php unset($_SESSION["Message"]); } ?>


    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#userdetails"> All User Approvals </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <div id="userdetails" class="tab-pane fade in active">
            <h2> All Users </h2>
            <div class="transaction-table">
              <table class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> Full Name </th>
                    <th> User Type </th>
                    <th> Email </th>
                    <th> Created Date </th>
                    <th> Status </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($alluserData) > 0) { ?> 
                  <?php foreach($alluserData as $userData) { ?>
                    <tr>
                      <input type="hidden" class="empId" value="<?php echo $userData['dh_user_id']; ?>">
                      <?php if($userData['dh_user_group_id']=="2"){ ?>
                        <td style="display: none;" class="groupname">Division Manager</td>
                     <?php } elseif($userData['dh_user_group_id']=="3"){ ?>
                        <td style="display: none;" class="groupname">Sales Director</td>
                     <?php } ?>
                      <td class="fullNameText"> <?php echo $userData['dh_firstName'].' '.$userData['dh_lastName']; ?> </td>
                      <td> <?php echo $userData['dh_user_group_name']; ?> </td>
                      <td class="useremaildiv"> <?php echo $userData['dh_email_address']; ?> </td>
                      <td> <?php echo $userData['dh_date_created']; ?> </td>
                      <td> <?php echo $userData['dh_status']; ?> </td>
                      <td> <button class="btn btn-sm btn-info approval"> Approve </button>  <button class="btn btn-sm btn-danger decline"> Decline </button> </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="6" style="color:red;text-align: center;"><h2> No Pending Data To Approve / Decline </h2></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
    
  </div>

  <!-- Modal -->
  <div id="ApprovalModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="modalText"> </span></b></p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="empId" name="salesId">
            <input type="hidden" id="useremaildiv" name="emailuser">
            <input type="hidden" id="fullName" name="fullnameuser">
            <input type="hidden" id="status" name="status">
            <button type="submit" name="adminapprovalAccount" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footerFiles.php'; ?>
<script src="js/jquery.js"></script>
<script>
  $(document).ready(function(){
    $('#errMsg').fadeOut(5000);
    userApprove_decline();

    // hide #back-top first
    $("#back-top").hide();
    
    // fade in #back-top
    $(function () {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
          $('#back-top').fadeIn();
        } else {
          $('#back-top').fadeOut();
        }
      });

      // scroll body to 0px on click
      $('#back-top a').click(function () {
        $('body,html').animate({
          scrollTop: 0
        }, 800);
        return false;
      });
    });
  });

  function userApprove_decline(){
    $('.approval').each(function(){
      var _this = $(this);
      _this.on('click',function(){
        var fullName = _this.parent().parent().find('.fullNameText').text();
        var groupname = _this.parent().parent().find('.groupname').text();
        var useremaildiv = _this.parent().parent().find('.useremaildiv').text();
        var empId = _this.parent().parent().find('.empId').val();
        $('#status').val('Active');
        $('#empId').val(empId);
        $('#useremaildiv').val(useremaildiv);
        $('#fullName').val(fullName);
        $('#modalText').text('Approve '+ fullName +' as '+ groupname + '?');
        $('#ApprovalModal').modal('show');
        
      });
    });

    $('.decline').each(function(){
      var _this = $(this);
      _this.on('click',function(){
        var fullName = _this.parent().parent().find('.fullNameText').text();
        var groupname = _this.parent().parent().find('.groupname').text();
        var empId = _this.parent().parent().find('.empId').val();
        $('#empId').val(empId);
        $('#status').val('Declined');
        $('#modalText').text('Decline '+ fullName +' as '+ groupname + '?');
        $('#ApprovalModal').modal('show');
        
      });
    });
  }


</script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>

<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>