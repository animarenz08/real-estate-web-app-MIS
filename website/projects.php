<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 4;

    $prop = new Property();

  $myProj = $prop->getAllHouseProjects();
  $mySub = $prop->getAllSubdivision();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css"> 
  img{
  width: 370px;
 }
  
</style>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
    <div class="row">
      <div class="col-md-11  col-md-offset-1">
        <h1 style="font-size:40px;color:#197319;"> Subdivisions </h1>
        <div class="project-container">
          <ul class="nav nav-tabs">

             <?php 
             $tab = 1;
             $subdct = 0;
             $sub;

             foreach ($mySub as $subd) {

              echo '<li ';

              if ($tab == 1)
              { echo 'class="active"';
                $tab = 0;}

              echo '><a data-toggle="tab" href="#'. $subd['dh_subd_id'] . '">';
              echo  $subd['dh_subd_name'] . '</a></li>';

              $sub[$subdct][0] = $subd['dh_subd_id'];
              $sub[$subdct][1] = $subd['dh_subd_name'];
              $subdct++;
             }
           
             ?>

          </ul>
        </div>
        <div class="tab-content">
         <?php 
           $tab = 1;
           $house = 0;
           for ($ctr = 0; $ctr<$subdct; $ctr++)
           {

           echo '<div class = "tab-pane fade';

           if ($tab==1)
           {
            echo ' in active';
            $tab=0;
           }

           echo '" id = "' . $sub[$ctr][0] .'" >';
           echo '<h2>' . $sub[$ctr][1] . ' Houses</h2>';
           echo '<br>';
           ?>
           <div class="row">
           <div class ="col-md-11">
           <?php
           // echo '<div class = "col-md-9">';
           $s = $sub[$ctr][0];
           $display=0;
           foreach ($myProj as $proj) {
            if($proj['dh_subd_id'] == $s && $proj['isHouseSample']==1){ ?>
            
            <div class = "col-sm-4">
            <img src = "<?php echo $proj['dh_image_path']; ?>" style="width:100%; " alt="" />
            <h3><?php echo $proj['dh_house_name']; ?></h3>
            <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
            <p><?php echo $proj['dh_description']; ?> </p>
            <div class = "viewbutton">
                <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details"></div><br><br>
                </form>
                     <?php
            $_SESSION['house'][$house] = $proj['dh_house_proj_id'];
            $house++; ?>
            </div>

            <?php
             $display=1;
           }  ?>
           <?php
           }
           $_SESSION['housetotal']=$house;
           if ($display==0)
           {  
           echo '<div class = "col-md-11" align = center> <h3>Nothing to show</h3> <br> </div>'; 
           } ?>
         </div>
       </div>
     </div>
          <?php
        }
        ?>
      </div>
      
      
      </div>
      </div>
      </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>