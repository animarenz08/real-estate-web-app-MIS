<?php
  include '../common/class.users.php';
    session_start();
    $currentMenu = 9;

    $user = new User();
    if( isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['password']) && !empty($_GET['password']) )
    {

      $email = $_GET['email'];
      $password = $_GET['password'];
      $myreset = $user->getUsertoResetPassword($email,$password);

    }
  


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
    <?php include 'mainHeader.php'; ?>

  <div class="content">
    <?php if(isset($Message)){ ?>
        <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
      &nbsp; <?php echo $Message; ?>!</div>
    <?php unset($_SESSION["Message"]); } ?>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3" style="color: #ffffff;">
        <br>
        <div class="panel panel-default" style="background: #13232f;">
          <h3 style="text-align: center;">Input Your New Password</h3>
          <br>

        <form method="post" role="form" action="commonFunctions.php">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <label>New Password</label>
            <input type="password" class="form-control" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" id="newpassword" name="newpassword" autocomplete="off">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <label>Confirm New Password</label>
            <input type="password" class="form-control" required minlength="8" id="confirmpassword" name="confirmpassword" autocomplete="off">
              </div>
            </div>
          </div>
          <?php foreach($myreset as $reset){ ?>
            <input type="hidden" name="email" value="<?php echo $reset['dh_email_address']; ?>">    
           
          <?php } ?>
          <br>
          <br>
          <div class="form-group clearfix">
            <div class="row">
              <div class="col-sm-4 col-sm-offset-5">
                 <button name="setpassword" class="btn btn-primary btn-lg">Apply</button>
              </div>
            </div>
          </div>
          <br>
        </form>
      </div>
      </div>
   
  </div>




  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
    <script>
        var password = document.getElementById("newpassword")
        , confirm_password = document.getElementById("confirmpassword");

        function validatePassword(){
        if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords don't match");
        } else {
        confirm_password.setCustomValidity('');
        }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>