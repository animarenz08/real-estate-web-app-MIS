<?php 
$getCurrentMenu = $currentMenu;

?>
<div class="wrap">
    <div class="top-head-grids">
      <img src="img/logodhrs1.jpg" />
    </div>
      <div class="top-head-grid">
        <h1>DREAM HOUSE REALTY</h1>
        <h2>The Real Estate Marketing Network</h2>
      </div>
  </div>

  <div style="clear:both">
  </div>

  <?php if($developerid == '2') { ?>

    <div class="wraps">
      <div class="social-icons">
        <div class="col-sm-offset-9">
          <ul id="horizontal-list">
            <li><a href="#"><img src="img/fb.png" /></a></li>
            <li><a href="#"><img src="img/twit.png" /></a></li>
            <li><a href="#"><img src="img/gp.png" /></a></li>
          </ul>
        </div>
      </div>
    </div>

       <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">
      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 203){ ?> class="active" <?php }; ?> ><a href="devaboutus.php">About Us</a></li>
            <li <?php if($getCurrentMenu == 204){ ?> class="active" <?php }; ?> ><a href="devhouses.php">Houses</a></li>
            <li <?php if($getCurrentMenu == 205){ ?> class="active" <?php }; ?> ><a href="#">Amenities</a></li>
            <li <?php if($getCurrentMenu == 206){ ?> class="active" <?php }; ?> ><a href="devcontactus.php">Contact Us</a></li>
            <li <?php if($getCurrentMenu == 207){ ?> class="active" <?php }; ?> ><a href="devlocation.php">Location</a></li>
          </ul>
          </div>
        </div>
    </nav>

  <?php } else if ($developerid == '1') { ?>

    <div class="wraps">
      <div class="social-icons">
        <div class="col-sm-offset-9">
          <ul id="horizontal-list">
            <li><a href="#"><img src="img/fb.png" /></a></li>
            <li><a href="#"><img src="img/twit.png" /></a></li>
            <li><a href="#"><img src="img/gp.png" /></a></li>
          </ul>
        </div>
      </div>
    </div>

       <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-static-top" style="margin-bottom:0px;">
      <div class="navbar-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($getCurrentMenu == 1){ ?> class="active" <?php }; ?> ><a href="index.php">Home</a></li>
            <li <?php if($getCurrentMenu == 303){ ?> class="active" <?php }; ?> ><a href="devaboutus.php">About Us</a></li>
            <li <?php if($getCurrentMenu == 304){ ?> class="active" <?php }; ?> ><a href="devhouses.php">Houses</a></li>
            <li <?php if($getCurrentMenu == 305){ ?> class="active" <?php }; ?> ><a href="#">Amenities</a></li>
            <li <?php if($getCurrentMenu == 306){ ?> class="active" <?php }; ?> ><a href="devcontactus.php">Contact Us</a></li>
            <li <?php if($getCurrentMenu == 307){ ?> class="active" <?php }; ?> ><a href="devlocation.php">Location</a></li>
          </ul>
          </div>
        </div>
    </nav>

<?php } ?>
 <div class="clear"> </div>