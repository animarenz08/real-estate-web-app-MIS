<?php
include '../config/config.php';
include '../common/class.properties.php';
include '../common/class.ajax.php';

session_start();

if(isset($_POST['delete_project'])){
	$prop = new Property();

	$propId = $_POST['houseId'];

	$result = $prop->deleteHouseProject($propId);
	if($result){
		$prop->redirect('adminMaintenance.php');
	} else {
		$_SESSION['Message'] = "Something went wrong please try again.";
		$_SESSION['MsgCode'] = 2;
		$prop->redirect('adminMaintenance.php');
		}		
	}

if(isset($_POST['empDeleteproject'])){
	$prop = new Property();

	$propId = $_POST['houseId'];

	$result = $prop->deleteHouseProject($propId);
	if($result){
		$prop->redirect('empMaintenance.php');
	} else {
		$_SESSION['Message'] = "Something went wrong please try again.";
		$_SESSION['MsgCode'] = 2;
		$prop->redirect('empMaintenance.php');
		}		
	}