
<?php
include '../common/class.users.php';

$user = new User();

$id = $_POST['id'];
$anId = $_POST['id'];
$result = $user->getUserFullDetailsbyUserId($id);
$educ = $user->getUserEducbyUserId($id);
$sales = $user->getUserSalesbyUserId($id);
?>
<?php
	echo"<script type='text/javascript'>$('#labels').hide()</script>";
	echo"<script type='text/javascript'>$('#brs').hide()</script>";
?>
<br>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<h3> Account Details </h3>
		<table class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th> Account ID </th>
					<th> Account Username </th>
					<th> User Type </th>
					<th> First Name </th>
					<th> Middle Name </th>
					<th> Last Name </th>
					<th> Birthdate </th>
					<th> Age </th>
					<th> Gender </th>
					<th> Tin Number </th>
					<th> Email Address </th>
					<th> Contact No </th>
					<th> Home Address </th>
					<th> Account Status </th>
					<th> Date Created </th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($result) > 0) { ?> 
				<?php foreach($result as $data) { ?>
				<tr>
					<td><?php echo $data['dh_user_id']; ?></td>
					<td><?php echo $data['dh_user_name']; ?></td>
					<td><?php echo $data['dh_user_group_name']; ?></td>
					<td><?php echo $data['dh_firstName']; ?></td>
					<td><?php echo $data['dh_middleName']; ?></td>
					<td><?php echo $data['dh_lastName']; ?></td>
					<td><?php echo $data['dh_bday']; ?></td>
					<td><?php echo $data['dh_age']; ?></td>
					<td><?php echo $data['dh_gender']; ?></td>
					<td><?php echo $data['dh_tin_number']; ?></td>
					<td><?php echo $data['dh_email_address']; ?></td>
					<td><?php echo $data['dh_contact_no']; ?></td>
					<td><?php echo $data['dh_home_address']; ?></td>
					<td><?php echo $data['dh_status']; ?></td>
					<td><?php echo $data['dh_date_created']; ?></td>
				</tr> 
				<?php } ?>
				<?php } else { ?>
                    <tr> 
                      <td colspan="15" style="text-align: center; color:red;"> 
                        <h2> No Data Result </h2>
                      </td>
                    </tr>
                  <?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-sm-4 col-sm-offset-1">
		<h4> Account Education Details </h4>
		<table class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th> School Name </th>
					<th> Location </th>
					<th> Attainment </th>
					<th> From </th>
					<th> To </th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($educ) > 0) { ?> 
				<?php foreach($educ as $educdata){ ?>
					<tr>
						<td><?php echo $educdata['dh_school_name']; ?></td>
						<td><?php echo $educdata['dh_school_location']; ?></td>
						<td><?php echo $educdata['dh_attainment']; ?></td>
						<td><?php echo $educdata['dh_year_from']; ?></td>
						<td><?php echo $educdata['dh_year_to']; ?></td>
					</tr>
				<?php } ?>
				<?php } else { ?>
                    <tr> 
                      <td colspan="5" style="text-align: center; color:red;"> 
                        <h2> No Data Result </h2>
                      </td>
                    </tr>
                  <?php } ?>
			</tbody>
		</table>
	</div>
	<div class="col-sm-6">
		<h4> My Closing Sales </h4>
		<table class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th> Full Name </th>
					<th> Phone No </th>
					<th> Telephone No </th>
					<th> Complete Address </th>
					<th> Developer </th>
					<th> House Model </th>
					<th> Floor & Lot Area </th>
					<th> TCP/Checked w/ Dev </th>
					<th> Reservation Fee </th>
					<th> Division </th>
					<th> Closing Sale Date </th>
					<th> Date Created </th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($sales) > 0) { ?> 
				<?php foreach($sales as $salesData){ ?>
					<tr>
						<td> <?php echo $salesData['dh_Firstname'].' '.$salesData['dh_familyName']; ?> </td>
						<td> <?php echo $salesData['dh_phoneNo']; ?> </td>
						<td> <?php echo $salesData['dh_telNo']; ?> </td>
						<td> <?php echo 'Blk '.$salesData['dh_block'].' & Lot '.$salesData['dh_lot'].' Phase '.$salesData['dh_phase'].', '.$salesData['dh_subdivision'].', '.$salesData['dh_location']; ?> </td>
						<td> <?php echo $salesData['dh_developer']; ?> </td>
						<td> <?php echo $salesData['dh_houseModel']; ?> </td>
						<td> <?php echo $salesData['dh_floorArea'].'sqm - '.$salesData['dh_lotArea'].'sqm'; ?> </td>
						<td>₱ <?php echo $salesData['dh_tcpDev']; ?> </td>
						<td>₱ <?php echo $salesData['dh_reservationFee']; ?> </td>
						<td> <?php echo $salesData['dh_division']; ?> </td>
						<td> <?php echo $salesData['dh_closingDate']; ?> </td>
						<td> <?php echo $salesData['dh_created_date']; ?> </td>
					</tr>
				<?php } ?>
				<?php } else { ?>
                    <tr> 
                      <td colspan="13" style="text-align: center; color:red;"> 
                        <h2> No Data Result </h2>
                      </td>
                    </tr>
                  <?php } ?>
			</tbody>
		</table>
	</div>

</div>