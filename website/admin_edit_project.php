<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

	session_start();
	$currentMenu = 54;
  $userGroup = 1;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);

  $prop = new Property();
if(isset($_GET['id']))
{
  $id=$_GET['id'];
  $editProj = $prop->HouseProjectsbyProjId($id);
  $myfinish = $prop->HouseFinishbyProjId($id);
  
}
foreach($editProj as $val){
echo '<input type="hidden" value="'.$val["dh_subd_id"].'"> ';
$subd = $prop->getAllSubdivisionbyEdit($val["dh_subd_id"]);
}


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

  <div class="content">

    <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Edit House project </h2>
    <br>
    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <form method="post" role="form" action="commonFunctions.php">
          <?php foreach($editProj as $row) { ?>
          <input type="hidden" class="houseprojId" name="houseprojId" value="<?php echo $row['dh_house_proj_id']; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4 col-sm-offset-2">
                <label for="" class="control-label">House Project Name</label>
                <input type="text" name="houseName" class="form-control" value="<?php echo $row['dh_house_name']; ?>">
              </div>
              <div class="col-sm-4">
                <label for="" class="control-label">House Project Selling Price</label>
                <input type="text" name="houseprice" class="form-control" value="<?php echo $row['dh_house_price']; ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <label for="" class="control-label">House Description</label>
                <span><textarea rows="4" cols="4" class="form-control" name="description"><?php echo $row['dh_description']; ?></textarea></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 newinput">
                <label for="" class="control-label">House Project Finish</label>
                <button type="button" class="btn btn-info btn-sm addinput"> <span class="glyphicon glyphicon-plus-sign"></span> Add New Input</button>
                <?php foreach($myfinish as $finishData){ ?>
                <input type="text" name="finish[]" class="form-control" value="<?php echo $finishData['dh_finish_description']; ?>">
                <input type="hidden" name="finishId[]" value="<?php echo $finishData['dh_finish_id']; ?>">
                <input type="hidden" name="finishhouseprojId[]" value="<?php echo $finishData['dh_house_proj_id']; ?>">
                <?php } ?>
                <input type="hidden" name="finishhouseprojId[]" value="<?php echo $row['dh_house_proj_id']; ?>">

              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
                <label class="col-sm-2 col-sm-offset-2 control-label">House Image</label>
                <div class="col-sm-5">
                  <img src="<?php echo $row['dh_image_path']; ?>" style="width: 60%;"/>
                  <br>
                  <br>
                  <input type="file" id="houseImageFile" name="houseImageFile[]" multiple="multiple" accept="image/*"/>
                  <input type="hidden" name="imageId" value="<?php echo $row['dh_image_id']; ?>">
                </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-2">
                <label for="" class="control-label">Subdivision</label>
                <select name="subd" class="form-control">
                  <option value="<?php echo $row['dh_subd_id']; ?>"><?php echo $row['dh_subd_name']; ?></option>
                  <?php foreach ($subd as $subdData) { ?>
                  <option value="<?php echo $subdData['dh_subd_id']; ?>"><?php echo $subdData['dh_subd_name']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-sm-3">
                <label for="" class="control-label">House Location</label>
                <input type="text" name="location" value="<?php echo $row['dh_location']; ?>" class="form-control">
              </div>
              <div class="col-sm-3">
                <label for="" class="control-label">House Promos</label>
                <input type="text" name="promos" value="<?php echo $row['dh_promos']; ?>" class="form-control">
              </div>
            </div>
            </div>
          
          <div class="form-group clearfix">
            <div class="row">
               <div class="col-sm-4 col-sm-offset-5">
                
                  <button type="submit" name="adminupdateHouse" class="btn btn-primary">Update</button>
                </div>
              </div>
            </div>
          <?php } ?>
        </form>
      </div>
    </div>

  </div>
  <?php include 'footerFiles.php'; ?>
<script src="js/jquery.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
      $('.addinput').on('click', function(){
         $('.newinput').append('<input type="text" class="form-control" name="finish[]" pattern="[a-zA-Z][a-zA-Z ]{4,}" title="Must contain characters only and atleast minimum of 4 characters" placeholder="House Project Finish">')
      })


   });
     
  </script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>

<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>