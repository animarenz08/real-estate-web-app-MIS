<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

    $prop = new Property();
    
    $myProj = $prop->getAllHouseProjects();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<div class="content">

  <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"> Camella Homes </a></li>
            <li><a data-toggle="tab" href="#about"> About Camella </a></li>
            <li><a data-toggle="tab" href="#house"> Camella House Project </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For home antel -->
          <div id="home" class="tab-pane fade in active" style="padding:25px;">
            <div class="antelhead">
              <img src="subimg/cams.jpg" alt="">
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                <img src="subimg/banner.jpg" style="width: 100%;">
              </div>
              <div class="col-sm-5">
                <h2>Camella Homes</h2>
                <h3>Bacoor, Cavite</h3>
                Vista Land and Lifescapes, Inc. has created a self-sustaining and integrated masterplanned city development called Communicity. Camella is the residential component of the Communicity. We are not just providing the Filipino family a mere residence but blending the bond of a community and the exciting bustle of the city.

                <br>
                <br>
                It is life when we feel the joy of waking up to familiar faces and the excitement to the smell of a hearty breakfast with our loved ones. Having the energy to accomplish a day’s tasks and enjoying the robust commerce of the city, wandering the beautiful paths in the community and meeting new people and making friends are life’s simple blessings.
              </div>
            </div>
          </div>
          <!-- end of home -->
          <!-- start of about antel -->
          <div id="about" class="tab-pane fade" style="padding: 25px;">
            <div class="buyer-wrap">
              <div class="g-grids">
                <h3>Company Profile</h3>

                <p> Camella is the largest homebuilder in Philippines real estate industry.
                  As builders of top quality yet affordable housing in the Philippines, Camella has an enviable reputation. With 40 years and counting, Camella has already built more than 400,000 homes in 39 provinces, and 104 cities and municipalities across the country.
                  <br>
                Through the years, Camella has created an immense selection of affordable, high-quality Camella homes with world-class settings and exquisitely themed master planned house and lot communities across the country  – each one carrying Vista Land's expertise in space planning, carefully thought out and sustainable architecture, and an innate knack for selecting the most accessible and attractive locations. </p>
                <br>
              </div>
              <div class="g-grids">
                <h3>Creating Adventure</h3>
                <p>Camella's sophisticated and modern facilities offer an experience that can withstand the test of time. It creates a strong bond among family and friends and takes them in an adventure in the convenience of your well-maintained and luscious community.</p>
                <br>

              </div>

              <div class="g-grids">
                <h3>Exploring Ideas</h3>
                <p>Camella's sophisticated and modern facilities offer an experience that can withstand the test of time. It creates a strong bond among family and friends and takes them in an adventure in the convenience of your well-maintained and luscious community.</p>
                <br>
              </div>

              <div class="g-grids">
                <h3>Finding Purpose</h3>
                <p>Man’s instinct focuses on survival and seizing the day. But life is too short to put all your energy into uncertainty. That is why, man, capable of doing things and achieving dreams, connects with other people and forms a relationship bound in love.
                Wherever life takes you, remember that there's always a place for you to come home. Living the life at Camella. All that you need is here! </p>
                <br>
              </div>
            </div>
          </div>
          <!-- end of about -->
          <!-- start of houses -->
          <div id="house" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($myProj as $proj) { ?>
            <?php if($proj['dh_subd_id'] == '1') { ?>
            <div class="col-sm-4">
                <a href="<?php echo $proj['dh_image_path']; ?>"><img src="<?php echo $proj['dh_image_path']; ?>" style="width: 100%;" alt=""></a>
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
                <p><?php echo $proj['dh_description']; ?></p>
                <div class = "viewbutton">
                  <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details">
                  </form>
                </div>
                <br>
              </div>
              <?php } ?>
              <?php } ?>
          </div>
          <!-- end of houses -->
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>