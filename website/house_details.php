<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 000;
    $prop = new Property();
    $myProj = $prop->getAllHouseProjects2();
    $myfinish = $prop->getAllHouseFinish();
   
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
  .modelunits img{
    width: 200px;
    height: 120px;
}

.styleul{
  list-style-type: square;
}
</style>
</head>
<body>
 
  <?php include 'mainHeader.php'; 

     $house = 0 ;
     for( $total = 0; $total<$_SESSION['housetotal']; $total++)
     { 
      if ($sender = isset($_POST[$_SESSION['house'][$total]]))
      {
       $house = $_SESSION['house'][$total]; 
      }
     }

     $getid = $prop->HouseProjectsbyProjId($house);
     foreach ($getid as $data){
      echo "<input type='hidden' value='".$data['dh_subd_id']."'>";
      echo "<input type='hidden' value='".$data['dh_house_proj_id']."'>";
      $myhouse = $prop->getAllProjbySubdNotProjId($data['dh_subd_id'],$data['dh_house_proj_id']);
     }
  ?>  

<div class="container">
      <div class="row" >
        <div class="col-sm-6">
          <div class="houseshead">
         <?php 
            foreach ($myProj as $proj) {
            if($proj['dh_house_proj_id'] == $house && $proj['isHouseSample'] == 1)
            { 
              $name = $proj['dh_house_name'];
              echo '<img src = "' . $proj['dh_image_path']  . '" class = "img-responsive" style="width:80%;" alt = "">';
            }
          }
         ?>
          </div>
        </div>
        <div class="col-sm-6">
          <h3>Model Units</h3>
          <div class="myhouse">
            <?php foreach ($myhouse as $myhaus) { ?>
              <div class="col-sm-5">
              <img src="<?php echo $myhaus['dh_image_path']; ?>" style="width: 80%;">
              <h4><?php echo $myhaus['dh_house_name']; ?></h4>
              <div class = "viewbutton">
                  <form method = "post" action = "house_details.php">
                  <input name = "<?php echo $myhaus['dh_house_proj_id']; ?>" class = "btn btn-info btn-sm" type = "submit" value = "View House Details">
                  </form>
                </div>
                <br>
            </div>
            <?php } ?>
          </div>
        </div>
        
    </div>

    <div style="clear:both;">
    </div>
      <div class="row" style = "margin-top: 10px;">
        <div class="housescontent">
          <div class="col-sm-6" >
          <?php 
            foreach ($myProj as $proj) {
            if($proj['dh_house_proj_id'] == $house && $proj['isHouseSample']==1)
            { 
              echo '<h2>' . $proj['dh_house_name']  . ' House</h2>';
              echo '<h4>Selling Price: ₱' . $proj['dh_house_price']  . '</h4>';
              echo '<br>' . $proj['dh_description'] . '<br>';
            }
          }
         ?>
            <br>
            <h3>Floor Plan</h3>
            <div class="col-md-12">
           <?php 
            $floor=0;
            foreach ($myProj as $proj) {
            if($proj['dh_house_proj_id'] == $house && $proj['isFloorPlan'] == 1)
            { 
              echo '<img src = "' . $proj['dh_image_path']  . '" class = "img-responsive" style="width:70%;" alt = ""><br>';
              $floor=1;
            }
           }
           if ($floor==0)
           {  
           echo '<h1>Nothing to show</h1>'; 
           }          
         ?> 
           </div>
          </div>
          <div class="col-sm-6" style = "padding:10px;">
            <h4><?php echo $name; ?> Finish:</h4>
            <ul type="disc" class="styleul">
         <?php 
            foreach ($myfinish as $fin) {
            if($fin['dh_house_proj_id'] == $house)
            { 
              echo '<li>' . $fin['dh_finish_description'] .'</li>';
            }
          }
         ?>
           
         </ul>
        </div>
        </div>
        
      </div>

      <div style="clear:both">
      </div>

    </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>