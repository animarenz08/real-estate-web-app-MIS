<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

	session_start();
	$currentMenu = 54;
  $userGroup = 5;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
if(isset($_GET['id']))
{
  $id=$_GET['id'];
  $editNews = $user->getNewsbyId($id);
}

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

  <div class="content">

    <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Edit News/Activities/Achievements </h2>

    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <form method="post" role="form" action="commonFunctions.php">
          <?php foreach($editNews as $row) { ?>
          <input type="hidden" name="newsId" value="<?php echo $row['dh_news_id']; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-3 col-sm-offset-4">
                <label for="" class="control-label">Title</label>
                <input type="text" name="title" class="form-control" value="<?php echo $row['dh_title']; ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-3 col-sm-offset-4">
                <label for="" class="control-label">News Image</label><br>
                <img src="<?php echo $row['dh_image_path']; ?>" style="width: 100%;"/>

                <br>
                <br>
                <input type="file" id="newsImageFile" name="newsImageFile[]" multiple="multiple" accept="image/*"/>
                <input type="hidden" name="imageId" value="<?php echo $row['dh_image_id']; ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-3 col-sm-offset-4">
                <label for="" class="control-label">Content</label>
                <span><textarea rows="10" cols="20" class="form-control" name="content"><?php echo $row['dh_content']; ?></textarea></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-5">
                  <button type="submit" name="updateNews" class="btn btn-primary update">Update</button>
                </div>
              </div>
            </div>
          <?php } ?>
        </form>
      </div>
    </div>

  </div>
  
  <?php include 'footerFiles.php'; ?>
<script src="js/jquery.js"></script>


<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>

<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>