<?php
  include '../common/class.users.php';
	session_start();
	$currentMenu = 24;
	$userGroup = 2;
	$division = $_SESSION['user_division'];

  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $myDownlines = $user->getDownlinesPerDivision($division, $_SESSION['user_session']);
  ?>

<?php
require 'src/Clockwork.php';
require 'src/ClockworkException.php';

if(isset($_POST['sendsms'])){
  $to = $_POST['tonumber'];
  $msg = $_POST['msg'];


  try
  {
  // Create a Clockwork object using your API key
    $options = array( 'ssl' => false );
    $clockwork = new mediaburst\ClockworkSMS\Clockwork( '2cd8aba7d3aec1b9174cf17ab75870725d5728d3',$options );

    foreach($to as $data){
    // Setup and send a message
    $message = array( 'to' => $data, 'message' => $msg );
    $result = $clockwork->send( $message );
	}
  }
  catch (mediaburst\ClockworkSMS\ClockworkException $e)
  {
    echo 'Exception sending SMS: ' . $e->getMessage();
  }
}
?>
<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
	<?php if(isset($Message)){ ?>
		<div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
	  &nbsp; <?php echo $Message; ?>!</div>
	<?php unset($_SESSION["Message"]); } ?>
	<div class="row">

		<h2 style="text-align:center; text-transform: uppercase;margin:0;"> Send SMS to Downlines </h2>
	  	<br>
	  	<br>
	  	<div class="col-sm-5 col-sm-offset-1">
	  		<form method="POST" action="">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-10 newinput">
							<label>To:</label>

							<button type="button" class="btn btn-info btn-sm addinput"> <span class="glyphicon glyphicon-plus-sign"></span> Add New Input</button>
							<select name="tonumber[]" class="form-control">
								<option selected disabled> Choose any Downline </option>
								<option active hidden>Choose any Downline</option>
								<?php foreach($myDownlines as $userData) { ?>
									<option value="<?php echo $userData['dh_contact_no']; ?>"><?php echo $userData['dh_firstName'].' '.$userData['dh_lastName']; ?></option>
								<?php } ?>
							</select>
							<!-- <input class="form-control" type="text" name="tonumber[]" placeholder="Input Number"> -->
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-10">
							<label>Message:</label>
							<span><textarea rows="10" cols="4" class="form-control" name="msg"></textarea></span>
						</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-2 col-sm-offset-3">
							<div class="form-group clearfix">
								<button type="submit" name="sendsms" class="btn btn-primary btn-lg">Send Message</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-6">
	  		<div class="transaction-table">
              <table id="tabledownlines" class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> Full Name </th>
                    <th> User Type </th>
                    <th> Email </th>
                    <th> Contact Number </th>
                    <th> Status </th>
                  </tr>
                </thead>
                <tbody>
                	<?php if(count($myDownlines) > 0) { ?> 
                		<?php foreach($myDownlines as $userData) { ?>
                			<tr>
                				<td style="vertical-align: middle;" class="fullNameText"> <?php echo $userData['dh_firstName'].' '.$userData['dh_lastName']; ?> </td>
                				<td style="vertical-align: middle;" > <?php echo $userData['dh_user_group_name']; ?> </td>
								<td style="vertical-align: middle;"> <?php echo $userData['dh_email_address']; ?> </td>
								<td style="vertical-align: middle;" > <?php echo $userData['dh_contact_no']; ?> </td>
								<td style="vertical-align: middle;" > <?php echo $userData['dh_status']; ?> </td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr> 
								<td colspan="6" style="text-align: center; color:red;"> 
									<h2> No Downlines </h2>
								</td>
							</tr>
							<?php } ?>
                </tbody>
              </table>
            </div>
		</div>
	</div>

</div>

	<?php include 'footerFiles.php'; ?>
	<script src="js/jquery.js"></script>
  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
  <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript">
   $(document).ready(function(){
      $('.addinput').on('click', function(){
         $('.newinput').append('<select name="tonumber[]" class="form-control"><option selected disabled> Choose any Downline </option><option active hidden>Choose any Downline</option><?php foreach($myDownlines as $userData) { ?>
         	<option value="<?php echo $userData['dh_contact_no']; ?>"><?php echo $userData['dh_firstName'].' '.$userData['dh_lastName']; ?></option><?php } ?></select>')
      })


   });
     
  </script>
  	<script>
	  $(document).ready(function(){
	  	$('#tabledownlines').DataTable();

		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
		  $(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
			  $('#back-top').fadeIn();
			} else {
			  $('#back-top').fadeOut();
			}
		  });

		  // scroll body to 0px on click
		  $('#back-top a').click(function () {
			$('body,html').animate({
			  scrollTop: 0
			}, 800);
			return false;
		  });
		});

	  });
	</script>
	 <script type="text/javascript">
	  $('#errMsg').fadeOut(5000); 
	</script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
</body>

</html>