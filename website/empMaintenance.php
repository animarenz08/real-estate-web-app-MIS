<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

	session_start();
	$currentMenu = 55;
  $userGroup = 5;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $myNews = $user->getAllNews();
  $myArchive = $user->getAllArchiveNews();

  $prop = new Property();

  $myHouse = $prop->getAllHouseProjects();


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<script>
      function deleteconfig(){

      var del=confirm("Are you sure you want to delete this record?");
      if (del==true){
         alert ("record deleted")
      }
      return del;
      }
      </script>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
    <?php if(isset($Message)){ ?>
        <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
      &nbsp; <?php echo $Message; ?>!</div>
    <?php unset($_SESSION["Message"]); } ?>
    <br>
    <br>

    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#myProj"> House Projects </a></li>
            <li><a data-toggle="tab" href="#news"> News/Activities/Achievements </a></li>
            <li><a data-toggle="tab" href="#archive"> Archived News/Activities/Achievements </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For house projects -->
          <div id="myProj" class="tab-pane fade in active" style="padding:25px;">

            <hr>
            <div class="transaction-table">
              <table id="tableproject" class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> House Name </th>
                    <th> House Description </th>
                    <th> House Sample </th>
                    <th> House Location </th>
                    <th> House Promos </th>
                    <th> Date Created </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($myHouse) > 0) { ?> 
                  <?php foreach ($myHouse as $houseData) { ?>
                    <tr>
                        <input type="hidden" class="houseId" value="<?php echo $houseData['dh_house_proj_id']; ?>">
                        <td class="houseName"> <?php echo $houseData['dh_house_name']; ?> </td>
                        <td> <?php echo $houseData['dh_description']; ?> </td>
                        <td> <img src="<?php echo $houseData['dh_image_path']; ?>" style="width: 90%;" /> </td>
                        <td> <?php echo $houseData['dh_location']; ?> </td>
                        <td> <?php echo $houseData['dh_promos']; ?> </td>
                        <td> <?php echo $houseData['dh_date_created']; ?> </td>
                        <td>
                          <a href="edit_project.php?id=<?php echo $houseData['dh_house_proj_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span> </a> 
                          <button class="btn btn-sm btn-danger deleteProject"> Delete <span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="7" style="text-align: center; color:red;"> 
                        <h2> No House Project</h2>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- end for house projects -->
          <!-- Start For news -->
          <div id="news" class="tab-pane fade" style="padding:25px;">

            <hr>
            <div class="transaction-table">
              <table id="newstable" class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> Title </th>
                    <th> Content </th>
                    <th> Image </th>
                    <th> Date Created </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($myNews) > 0) { ?> 
                  <?php foreach ($myNews as $newsData) { ?>
                    <tr>
                      <input type="hidden" class="newsId" value="<?php echo $newsData['dh_news_id']; ?>">
                        <td class="title"> <?php echo $newsData['dh_title']; ?> </td>
                        <td> <?php echo $newsData['dh_content']; ?> </td>
                        <td> <img src="<?php echo $newsData['dh_image_path']; ?>" style="width: 90%;" /> </td>
                        <td> <?php echo $newsData['dh_date_created']; ?> </td>
                        <td>
                            <a href="edit_news.php?id=<?php echo $newsData['dh_news_id'];?>" title="Edit" data-toggle="tooltip" class="btn btn-sm btn-warning"> Edit <span class="glyphicon glyphicon-edit"></span></a>
                            <button class="btn btn-sm btn-info archive"> Archive <span class="glyphicon glyphicon-save-file"></span></button>
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="5" style="text-align: center; color:red;"> 
                        <h2> No News/Activities/Achievements </h2>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
            <!-- end for news -->
            <!-- Start For archived news -->
          <div id="archive" class="tab-pane fade" style="padding:25px;">

            <hr>
            <div class="transaction-table">
              <table id="archivetable" class="table table-condensed table-hover table-striped">
                <thead>
                  <tr>
                    <th> Title </th>
                    <th> Content </th>
                    <th> Image </th>
                    <th> Date Created </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($myArchive) > 0) { ?> 
                  <?php foreach ($myArchive as $archiveData) { ?>
                    <tr>
                      <input type="hidden" class="newsId" value="<?php echo $archiveData['dh_news_id']; ?>">
                        <td class="title"> <?php echo $archiveData['dh_title']; ?> </td>
                        <td> <?php echo $archiveData['dh_content']; ?> </td>
                        <td> <img src="<?php echo $archiveData['dh_image_path']; ?>" style="width: 90%;" /> </td>
                        <td> <?php echo $archiveData['dh_date_created']; ?> </td>
                        <td>
                            <button class="btn btn-sm btn-info unarchive"> Unarchive <span class="glyphicon glyphicon-open-file"></span></button>
                        </td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="5" style="text-align: center; color:red;"> 
                        <h2> No Archived News/Activities/Achievements </h2>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
            <!-- end for archived news -->
        </div>
      </div>
    </div>
  </div>

  <!-- Modals -->
  <div id="projModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="projmodalText"> </span></b> </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="propFunctions.php">
            <input type="hidden" id="houseId" name="houseId">
            <button type="submit" name="empDeleteproject" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="newsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="newsmodalText"> </span></b> </p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="commonFunctions.php">
            <input type="hidden" id="newsId" name="newsId">
            <input type="hidden" id="status" name="status">
            <button type="submit" name="newsarchive" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
<script src="js/jquery.js"></script>
<script>
  $(document).ready(function(){
    $('#errMsg').fadeOut(5000); 
  });
</script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
    $('#tableproject').DataTable();

    $('#newstable').DataTable();

    $('#archivetable').DataTable();
  });

  $("#tableproject").on('click','.deleteProject',function(){
    var _this = $(this);
    var houseId = _this.parent().parent().find('.houseId').val();
    var houseName = _this.parent().parent().find('.houseName').text();
    $('#houseId').val(houseId);
    $('#projmodalText').text(' "DELETE" '+ houseName+'?');
    $('#projModal').modal('show');
   });

  $("#newstable").on('click','.archive',function(){
    var _this = $(this);
    var newsId = _this.parent().parent().find('.newsId').val();
    var title = _this.parent().parent().find('.title').text();
    $('#newsId').val(newsId);
    $('#status').val('1');
    $('#newsmodalText').text(' "Archive" '+ title);
    $('#newsModal').modal('show');
   });

  $("#archivetable").on('click','.unarchive',function(){
    var _this = $(this);
    var newsId = _this.parent().parent().find('.newsId').val();
    var title = _this.parent().parent().find('.title').text();
    $('#newsId').val(newsId);
    $('#status').val('0');
    $('#newsmodalText').text(' "Unarchive" '+ title);
    $('#newsModal').modal('show');
   });
</script>


<script src="js/bootstrap.min.js"></script>
</body>

</html>