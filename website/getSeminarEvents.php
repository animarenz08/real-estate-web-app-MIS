<?php

include '../common/class.ajax.php';

$ajaxFunc = new ajaxFunction();

$result = $ajaxFunc->getAllSeminar();

$event_array = array();
foreach($result as $data){
	$originalDate = $data['dh_seminar_date'];
	$newDate = date($originalDate);

	$event_array[] = array(
		'title' => $data['dh_seminar_name'],
		'start' => $newDate,
		'description' => 'Location: '.$data['dh_seminar_location']." | Time: ".$data['dh_seminar_time_start'].' to '.$data['dh_seminar_time_end']
	);
}

echo json_encode($event_array);

?>