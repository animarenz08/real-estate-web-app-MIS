<?php
include '../common/class.users.php';
$currentMenu = 00;

$user = new User();
// Users search terms is saved in $_POST['q']
if(isset($_POST['search'])){
	$projname = $_POST['search'];
  $descrip = $_POST['search'];
  $location = $_POST['search'];
  $price = $_POST['search'];

// Prepare statement
	$search = $user->SearchAll("%$projname%","%$descrip%","%$location%","%$price%");
// Echo results
}
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
	<?php include 'mainHeader.php'; ?>
	<div class="content">

		<h3>Search Results For: <?php print_r($projname); ?></h3>
		<br>
		<?php if(count($search) > 0) { ?>
			<?php foreach($search as $searchData) { ?>
			<div class="row">
				<div class="col-sm-3">
					<img src="<?php echo $searchData['dh_image_path']; ?>" style="width: 100%;">
				</div>
				<div class="col-sm-7">
  					<h4><?php echo $searchData['dh_house_name']; ?></h4>

            <h5>Selling Price: ₱<?php echo $searchData['dh_house_price']; ?></h5>
  				  <h5><?php echo $searchData['dh_subd_name']; ?></h5>
            <h5><?php echo $searchData['dh_subd_location']; ?></h5>
  					<p><?php echo $searchData['dh_description']; ?></p>
            <div class = "viewbutton">
                  <form method = "post" action = "house_details.php">
                  <input name = "<?php echo $searchData['dh_house_proj_id']; ?>" class = "btn btn-info" type = "submit" value = "View More Details">
                  </form>
                </div>
  				</div>
  			</div>
  			<br>
		<?php } ?>
		<?php } else { ?>
			<h3> Empty Result </h3>
		<?php } ?>
		
	</div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>