 <!---start-footer-->
    <div class="footer">
      <div class="wrap">
        <div class="footer-grid">
          <h3>About Us</h3>
            <p>MR. RODELIO D. PARAFINA, CPA <br>
              Licensed Real Estate Broker - PRC Lic. # 01039<br>
              Certified Public Accountant (CPA) - PRC Lic. # 03525</p>
          </div>

        <div class="footer-grid center-grid">
          <h3>Contact Us</h3>
            <p>Phone: <br> 
              0918-913-6856 <br>
              Email: 
              dreamhouserealty@ymail.com</p>          
            </div>

        <div class="footer-grid flgrid">
          <h3>Address</h3>
            <p><label>MARIKINA BUSINESS OFFICE:</label>No. 7 Maroon St., Bonita Homes Concepcion II, Marikina City</p>
            <span>Telefax: 
            (02)948-1288
            </span>
            <p><label>TANZA, CAVITE BUSINESS OFFICE:</label>B3 L1 Villa Tanza Subdivision, Biga Tanza Cavite</p>
            <span>Tel. No.: 
            (046)402-3250</span>
        </div>
    
        <div class="clear"> </div>
      </div>
      <div class="clear"> 
      </div>

    </div>


  <div class="copy-right">
    <div class="top-to-page" id="back-top">
        <a href="#top" title="Back to top"><span class="glyphicon glyphicon-upload"></span></a>

      <div class="clear"> </div>
    </div>
  </div>
      <div class="copy-mid">
        <div class="col-sm-6 col-sm-offset-1">
          <p>Dream House Realty © <?php echo date("Y"); ?> All Rights Reserved. Powered by <a target="_blank" href="https://www.facebook.com/AnimaRenz08"> CAPSTONE DEVELOPMENT GROUP</a></p>
        </div>
        <div>
          <a href="terms-condition.php" style="padding-right: 10px;"> Terms & Condition </a>
          <a href="privacy.php"> Privacy Policy </a>
        </div>
      </div>

  <script src="js/bootstrap-dropdownhover.min.js"></script>
     <!---End-footer---->
     <!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9931095;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->