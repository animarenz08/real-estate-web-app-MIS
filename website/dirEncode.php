<?php
  include '../common/class.users.php';
	session_start();
	$currentMenu = 34;
	$userGroup = 3;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $myVehicleTripRequest = $user->getMyVehicleTripRequest($_SESSION['user_session']);
  $allSalesDirector = $user->getAllActiveSalesDirector();
  $allSalesAgent = $user->getAllActiveSalesAgent();
  $allDivisionManager = $user->getAllActiveDivisionManager();
  $division = $user->getAllDivision();
  $recruitedByUser = $user->getAllSalesAndDiv();
  $subd = $user->getAllSubdivision();
  $group = $user->getAlluserGroupsexceptempAdmin();


?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content">
	<?php if(isset($Message)){ ?>
		<div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
	  &nbsp; <?php echo $Message; ?>!</div>
	<?php unset($_SESSION["Message"]); } ?>

	<h2 style="text-align:center; text-transform: uppercase;margin:0;"> Add/Submit new Closing Sales </h2>
	<br>
	<div class="row">
	  <div class="col-md-10 col-sm-offset-1">
		<div class="encoder-container">
		  <ul class="nav nav-tabs">
		  </ul>
		</div>
		<div class="tab-content">
		  	          <!-- Start For Closing Form -->
        <div id="closingForm" class="tab-pane fade in active" style="padding:25px;">
            <h2> Closing Form </h2>
            <hr>
            <form method="post" role="form" action="commonFunctions.php">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Family Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="familyname" placeholder="Enter your Last Name">
                     </div>
                     <div class="col-sm-3">
                        <label>First Name</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{2,20}" title="Must contain characters only and atleast 2 letters" name="firstname" placeholder="Enter your First Name">
                     </div>
                     <div class="col-sm-3">
                        <label>Cellphone #</label>
                        <input type="text" class="form-control" pattern="[0-9]{10,11}" title="Must contain numbers only and minimum of 10 numbers and maximum of 11 numbers" name="phoneno" placeholder="Enter your Phone No">
                     </div>
                     <div class="col-sm-3">
                        <label>Telephone #</label>
                        <input type="text" class="form-control" pattern="[0-9]{7,7}" title="Must contain 7 numbers only" name="telno" placeholder="Enter your Tel No">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Subdivision</label>
                       <select id="subdivision" name="subdivision" required class="form-control" onchange="subdname()">
                            <option value=""> Select Subdivision </option>
                            <?php foreach ($subd as $subdData) { ?>
                            <option value="<?php echo $subdData['dh_subd_name']; ?>"><?php echo $subdData['dh_subd_name']; ?></option>
                            <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-3">
                        <label>Location</label>
                        <input type="text" class="form-control" id="location" name="location" placeholder="Choose a subdivision" readonly />
                     </div>
                     <div class="col-sm-3">
                        <label>Developer</label>
                        <input type="text" class="form-control" id="developer" name="developer" placeholder="Choose a subdivision" readonly />
                     </div>
                     <div class="col-sm-3">
                        <label>House Model</label>
                        <input type="text" class="form-control" required pattern="[a-zA-Z][a-zA-Z ]{3,30}" itle="Must contain characters only and atleast 3 letters" name="housemodel" placeholder="Enter the House model">
                     </div>
                  </div>
               </div>
               <div style="clear:both;">
               </div>
               <div class="form-group">
                  <div class="row">
                     
                     <div class="col-sm-2">
                        <label>Phase #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="phase" placeholder="phase number" />
                     </div>
                     <div class="col-sm-2">
                        <label>Blk #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="block" placeholder="block mumber" />
                     </div>
                     <div class="col-sm-2">
                        <label>Lot #</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="lot" placeholder="lot number" />
                     </div>
                     <div class="col-sm-3">
                        <label>Floor Area</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="floorarea" placeholder="Enter the floor area" />(sqm.)
                     </div>
                     <div class="col-sm-3">
                        <label>Lot Area</label>
                        <input type="text" class="form-control" pattern="[0-9]{1,2}" title="Must contain numbers only" name="lotarea" placeholder="Enter the lot area" />(sqm.)
                     </div>                     
                  </div>
               </div>
                <br>
                <br>
               <div class="form-group">
                  <div class="row">
                    <div class="col-sm-3">
                        <label>TCP/Checked w/ Dev</label>
                        <input type="text" class="form-control" name="tcpdev" required pattern="[0-9]{5,}" title="Must contain numbers only" placeholder="TCP/Checked w/ Dev">
                     </div>
                     <div class="col-sm-2">
                        <label>Reservation Fee</label>
                        <input type="text" class="form-control" pattern="[0-9]{4,}" title="Must contain numbers only" name="reservationfee" placeholder="Amount of reservation">
                     </div>
                     <div class="col-sm-2">
                        <label>OI/PR #</label>
                        <input type="text" class="form-control" pattern="[0-9]{4,}" title="Must contain numbers only" name="oipr" placeholder="OI/PR #" />
                     </div>
                     <div class="col-sm-3">
                        <label> Closing Date </label>
                            <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="closingDateInput" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="5" name="closingDate" type="text" value="" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        <input type="hidden" id="closingDateInput" name="closingDate" value="" /><br/>    
                     </div>
                     <div class="col-sm-2">
                        <label>Division</label>
                        <select name="division" class="form-control">
                            <option value=''> Select Division </option>
                            <?php foreach($division as $divData){ ?>
                            <option value="<?php echo $divData['dh_division_id']; ?>"> <?php echo $divData['dh_division_name']; ?> </option>
                            <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <br>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Division Manager</label>
                        <select name="divisionmanager" class="form-control">
                          <option value=''> Select Division Manager </option>
                          <?php foreach($allDivisionManager as $allDivManager) { ?>
                            <option value="<?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName']; ?>" >  <?php echo $allDivManager['dh_firstName'].' '.$allDivManager['dh_lastName'].' - '.$allDivManager['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="divcomrate" placeholder="e.g 0.01%">
                    </div>
                     <div class="col-sm-3 col-sm-offset-1">
                        <label>Sales Director</label>
                        <select name="salesdirector" class="form-control">
                          <option value=''> Select Sales Director </option>
                          <?php foreach($allSalesDirector as $saleDirData) { ?>
                            <option value="<?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName']; ?>" >  <?php echo $saleDirData['dh_firstName'].' '.$saleDirData['dh_lastName'].' - '.$saleDirData['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="dircomrate" placeholder="e.g 0.01%">
                    </div>
                 </div>
             </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Sales Trainee/Agent/Referral</label>
                         <select name="salestrainee" class="form-control">
                          <option value=''> Select Sales Trainee/Agent/Referral </option>
                          <?php foreach($allSalesAgent as $salesData) { ?>
                            <option value="<?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName']; ?>" >  <?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName'].' - '.$salesData['dh_division_name']; ?> </option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="salescomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Division Manager</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdiv" placeholder="Enter RC Division Manager">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdivcomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RC Sales Director</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcdir" placeholder="Enter RC Sales Director">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcdircomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>RC Sales Trainee</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="rcsales" placeholder="Enter RC Sales Trainee">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rcsalescomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>RFA</label>
                        <input type="text" class="form-control" name="rfa" placeholder="Enter RFA">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="rfacomrate" placeholder="e.g 0.01%">
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>Booking Assistant</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" itle="Must contain characters only and atleast 4 letters" name="bookassist" placeholder="Enter Booking Assistant">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="bookcomrate" placeholder="e.g 0.01%">
                    </div>
                </div>
            </div>
             <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Assistant Agent</label>
                        <input type="text" class="form-control" pattern="[a-zA-Z][a-zA-Z ]{4,30}" title="Must contain characters only and atleast 4 letters" name="aassist" placeholder="Enter A Assistant">
                    </div>
                    <div class="col-sm-2">
                        <label>Commission Rate (%)</label>
                        <input type="text" class="form-control" pattern="[0-9]+(\.[0-9]{0,2})?%?"
                        title="This must be a number with up to 2 decimal places and/or %" name="acomrate" placeholder="e.g 0.01%">
                    </div>
                     <div class="col-sm-3 col-sm-offset-1">
                        <label>Prepared By</label>
                        <input type="text" readonly class="form-control" value="<?php echo $_SESSION['user_fullName']; ?>">
                        <input type="hidden" name="preparedBy" value="<?php echo $_SESSION['user_session']; ?>">
                     </div>
                  </div>
               </div>
               <div class="form-group clearfix">
                  <br>
                  <div class="col-sm-4 col-sm-offset-4">
                     <input type="hidden" name="userId" value="<?php echo $_SESSION['user_session']; ?>" >
                     <button type="submit" name="dirClosing" class="btn btn-primary btn-lg" style="width:100%">Closing Submit</button>
                  </div>
               </div>
            </form>
        </div>
          <!-- End for Closing Form -->
		</div>
	  </div>
	</div>
	
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

    <script type="text/javascript">
        function subdname() {
            var x = document.getElementById("subdivision").value;
            $.ajax({
                type: 'POST',
                url: '../website/commonFunctions.php',
                data: {
                    'subdname': x
                    },
                dataType: 'json',
                success: function(data){
                    $('#location').val(data.dh_subd_location);
                    $('#developer').val(data.dh_dev_name);
                    $('#subdsid').val(data.dh_subd_id);
                }
            });
        }
    </script>
  <script type="text/javascript">
        $('.form_date').datetimepicker({
                language:  'ar',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
            });
    </script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>