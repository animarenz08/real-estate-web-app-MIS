<?php
    include '../common/class.users.php';
  include '../common/class.properties.php';

  session_start();
  $currentMenu = 54;
  $userGroup = 5;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);

  $prop = new Property();

    $houseid = $_GET['id'];

    $getSeminar = $user->getHouseProject($houseid);

?>
<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<body>
<form method="post" action="commonFunctions.php" role="form">
  <div class="modal-body">
    <div class="form-group">
      <label for="id">ID</label>
      <input type="text" class="form-control" id="id" name="id" value="<?php echo $getSeminar['dh_house_proj_id'];?>" readonly="true"/>

    </div>
    <div class="form-group">
      <label for="houseName">House Name</label>
      <input type="text" class="form-control" id="houseName" name="houseName" value="<?php echo $getSeminar['dh_house_name'];?>" />
    </div>
    <div class="form-group">
      <label for="location">Phone</label>
        <input type="text" class="form-control" id="location" name="location" value="<?php echo $getSeminar['dh_location'];?>" />
    </div>
    </div>
    <div class="modal-footer">
      <input type="submit" class="btn btn-primary" name="submit" value="Update" />&nbsp;
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </form>
</body>
</html>