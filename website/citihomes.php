<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

    $prop = new Property();
    
    $myProj = $prop->getAllHouseProjects();
    $subd = $prop->getAllSubdivision();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<div class="content">

  <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"> MyCitiHomes </a></li>
            <li><a data-toggle="tab" href="#about"> About CitiHomes </a></li>
            <li><a data-toggle="tab" href="#subd"> CitiHomes Subdivisions </a></li>
            <li><a data-toggle="tab" href="#house"> CitiHomes House Project </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For home antel -->
          <div id="home" class="tab-pane fade in active" style="padding:25px;">
            <div class="antelhead">
              <img src="subimg/headerciti.jpg" alt="">
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                <img src="subimg/giar.jpg" style="width:100%;">
              </div>
              <div class="col-sm-5">
                <h2>MyCitiHomes</h2>
                <h3>General Trias, Cavite</h3>
                

                <br>
                <br>
                
              </div>
            </div>
          </div>
          <!-- end of home -->
          <!-- start of about antel -->
          <div id="about" class="tab-pane fade" style="padding: 25px;">
            <div class="buyer-wrap">
              <div class="g-grids">
                <h3>Company Profile</h3>

                <p>CitiHomes Builder and Development, Inc. (MyCitiHomes) is a domestic corporation established on March 1983 to engage in the business of real estate development. The organization focuses on developing raw lands into residential and commercial communities. </p>
                <br>
              </div>
              <div class="g-grids">
                <h3>Affordable Homes</h3>
                <p>With our financially-practical products, you can choose your home that fits your savings.</p>
                <br>

              </div>

              <div class="g-grids">
                <h3>Innovative Design</h3>
                <p>Built and supplied by established companies, your property is structured with the quality your home needs.</p>
                <br>
              </div>

              <div class="g-grids">
                <h3>Flexible Financing Support</h3>
                <p>We prepared multiple payment options based on your financial preference because we believe that you deserve to own your dream home.</p>
                <br>
              </div>
            </div>
          </div>
          <!-- end of about -->
          <!-- start of subdivisions -->
          <div id="subd" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($subd as $subdData) { ?>
            <?php if($subdData['dh_subd_dev_id'] == '3') { ?>
            <div class="col-sm-4">
              <img src="<?php echo $subdData['dh_subd_logo']; ?>" style="width: 100%;" alt="">
              <h3><?php echo $subdData['dh_subd_name']; ?></h3>
              <h4><?php echo $subdData['dh_subd_location']; ?></h4>
              <p><?php echo $subdData['dh_subd_description']; ?></p>
            </div>
            <?php } ?>
            <?php } ?>
          </div>
          <!-- end of subdivisions -->
          <!-- start of houses -->
          <div id="house" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($myProj as $proj) { ?>
            <?php if($proj['dh_subd_id'] == '6') { ?>
            <div class="col-sm-4">
                <a href="<?php echo $proj['dh_image_path']; ?>"><img src="<?php echo $proj['dh_image_path']; ?>" style="width: 100%;" alt=""></a>
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
                <p><?php echo $proj['dh_description']; ?></p>
                <div class = "viewbutton">
                  <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details">
                  </form>
                </div>
                <br>
              </div>
              <?php } ?>
              <?php } ?>
          </div>
          <!-- end of houses -->
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>