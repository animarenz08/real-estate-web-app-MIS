<?php
	include '../common/class.users.php';
	include '../common/class.properties.php';
		session_start();
		$currentMenu = 17;
		$userGroup = 1;

	$user = new User();

	$user->isPageAccessible($_SESSION['user_type'], $userGroup);
	$myUsers = $user->getAllUserDetailsAdmin($_SESSION['user_session']);
	$mySem = $user->getAllSeminar();
	$mySales = $user->getAllClosingSales();
	$myTrip = $user->getAllVehicleTrip();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style>
.dataTables_wrapper .dt-buttons {
  float:none;  
  text-align:right;

}
.dataTables_filter, .dataTables_info { display: none; }
</style>
</head>
<body>
		<?php include 'mainHeader.php'; ?>

	<div class="content">

		<br>
		<div class="row">
			<div class="col-md-10 col-sm-offset-1">
				<div class="encoder-container">
					<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#closesales"> Closing Sales Report </a></li>
							<li><a data-toggle="tab" href="#sem"> Seminar Report </a></li>
							<li><a data-toggle="tab" href="#trip"> Vehicle Trip Report </a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div id="closesales" class="tab-pane fade in active" style="padding:25px;">
						<h2> All Annual Closing Saless </h2>
							<hr>
								<p id="table-filter" style="display: none;" >
									Filter By Year: 
									<select class="form-control">
										<option value="">All</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
									</select>
								</p>						

						<table id="closingTable" class="table table-condensed table-hover table-striped" id="myClosingSales">
							<thead>
								<tr>
									<th> Full Name </th>
									<th> Phone No </th>
									<th> Telephone No </th>
									<th> Complete Address </th>
									<th> Developer </th>
									<th> House Model </th>
									<th> Floor & Lot Area </th>
									<th> TCP/Checked w/ Dev </th>
					                <th> Reservation Fee </th>
					                <th> Division </th>
					                <th> Prepared By </th>
									<th> Closing Sale Date </th>
									<th> Date Created </th>

								</tr>
							</thead>
							<tbody>
									<?php if(count($mySales) > 0) { ?> 
									<?php foreach ($mySales as $salesData) { ?>
										<tr>
												<td> <?php echo $salesData['dh_Firstname'].' '.$salesData['dh_familyName']; ?> </td>
												<td> <?php echo $salesData['dh_phoneNo']; ?> </td>
												<td> <?php echo $salesData['dh_telNo']; ?> </td>
												<td> <?php echo 'Blk '.$salesData['dh_block'].' & Lot '.$salesData['dh_lot'].' Phase '.$salesData['dh_phase'].', '.$salesData['dh_subdivision'].', '.$salesData['dh_location']; ?> </td>
												<td> <?php echo $salesData['dh_developer']; ?> </td>
												<td> <?php echo $salesData['dh_houseModel']; ?> </td>
												<td> <?php echo $salesData['dh_floorArea'].'sqm - '.$salesData['dh_lotArea'].'sqm'; ?> </td>
												<td> ₱ <?php echo $salesData['dh_tcpDev']; ?> </td>
						                        <td> ₱ <?php echo $salesData['dh_reservationFee']; ?> </td>
						                        <td> <?php echo $salesData['dh_division']; ?> </td>
						                        <td> <?php echo $salesData['dh_firstName'].' '.$salesData['dh_lastName']; ?> </td>
												<td> <?php echo $salesData['dh_closingDate']; ?> </td>
												<td> <?php echo $salesData['dh_created_date']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="13" style="text-align: center; color:red;"> 
												<h2> No Closing Sales </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>
					</div>
					<div id="sem" class="tab-pane fade" style="padding:25px;">
					<h2> All Annual Seminar </h2>
							<hr>
							<p id="table-filter1" style="display: none;" >
									Filter By Year: 
									<select class="form-control">
										<option value="">All</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
									</select>
								</p>		

							<table id="semtable" class="table table-condensed table-hover table-striped" id="mySeminars">
							<thead>
								<tr>
									<th> Seminar Name </th>
									<th> Seminar Date </th>
									<th> Seminar Time Start & End </th>
									<th> Seminar Location </th>
									<th> Date Created </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($mySem) > 0) { ?> 
									<?php foreach ($mySem as $semData) { ?>
										<tr>
												<td> <?php echo $semData['dh_seminar_name']; ?> </td>
												<td> <?php echo $semData['dh_seminar_date']; ?> </td>
												<td> <?php echo $semData['dh_seminar_time_start'].' - '.$semData['dh_seminar_time_end'];  ?> </td>
												<td> <?php echo $semData['dh_seminar_location']; ?> </td>
												<td> <?php echo $semData['dh_date_created']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="5" style="text-align: center; color:red;"> 
												<h2> No Seminars </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>
					</div>

					<div id="trip" class="tab-pane fade" style="padding:25px;">
					<h2> All Annual Vehicle Tripping </h2>
							<hr>
							<p id="table-filter2" style="display: none;" >
									Filter By Year: 
									<select class="form-control">
										<option value="">All</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
									</select>
								</p>		

							<table id="triptable" class="table table-condensed table-hover table-striped" id="myTrip">
							<thead>
								<tr>
									<th> Vehicle Trip Date </th>
									<th> Location Pickup & Pickup Time </th>
									<th> Location Destination & Destination Time </th>
									<th> Driver Name </th>
									<th> Plate No </th>
									<th> Driver Contact No </th>
									<th> Date Created </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($myTrip) > 0) { ?> 
									<?php foreach ($myTrip as $tripData) { ?>
										<tr>
												<td> <?php echo $tripData['dh_trip_date']; ?> </td>
												<td> <?php echo $tripData['dh_location_pickup'].' - '.$tripData['dh_location_pickup_time']; ?> </td>
												<td> <?php echo $tripData['dh_location_destination'].' - '.$tripData['dh_location_destination_time'];  ?> </td>
												<td> <?php echo $tripData['dh_driver_name']; ?> </td>
												<td> <?php echo $tripData['dh_plateno']; ?> </td>
												<td> <?php echo $tripData['dh_driver_contact']; ?> </td>
												<td> <?php echo $tripData['dh_date_created']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="8" style="text-align: center; color:red;"> 
												<h2> No Vehicle Tripping </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>
					</div>

				</div>

			</div>
		</div>
	</div>

	<?php include 'footerFiles.php'; ?>
	<script src="js/jquery.js"></script>

	<script src='js/moment.min.js'></script>
	<script src='js/jquery.min.js'></script>
	<script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="js/buttons.print.min.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
		var table = $('#semtable').DataTable( {
					dom: 'Bfr<"table-filter-container">tip',
					buttons: [ { extend: 'print', 
					customize: function ( win ) { 
						$(win.document.body) .css( 'font-size', '10pt' ) 
						.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
						$(win.document.body).find( 'table' ) 
						.addClass( 'compact' ) 
						.css( 'font-size', 'inherit' ); }
					}],
					initComplete: function(settings){
						var api = new $.fn.dataTable.Api( settings );
						$('.table-filter-container', api.table().container()).append(
							$('#table-filter1').detach().show()
							);
						$('#table-filter1 select').on('change', function(){
							table.search(this.value).draw();   
						});       
					}
				});
	});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			var table = $('#closingTable').DataTable( {
					dom: 'Bfr<"table-filter-container">tip',
					buttons: [ { extend: 'print', 
					customize: function ( win ) { 
						$(win.document.body) .css( 'font-size', '10pt' ) 
						.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
						$(win.document.body).find( 'table' ) 
						.addClass( 'compact' ) 
						.css( 'font-size', 'inherit' ); }
					}],
					initComplete: function(settings){
						var api = new $.fn.dataTable.Api( settings );
						$('.table-filter-container', api.table().container()).append(
							$('#table-filter').detach().show()
							);
						$('#table-filter select').on('change', function(){
							table.search(this.value).draw();   
						});       
					}
				});
		});
	</script>
	<script type="text/javascript">
		var table = $('#triptable').DataTable( {
					dom: 'Bfr<"table-filter-container">tip',
					buttons: [ { extend: 'print', 
					customize: function ( win ) { 
						$(win.document.body) .css( 'font-size', '10pt' ) 
						.prepend( '<img src="http://www.dreamhouserealty.com.ph/images/DHR%20logo.jpg" style="width: 102px; position:absolute; top:0; right:60;" />' ); 
						$(win.document.body).find( 'table' ) 
						.addClass( 'compact' ) 
						.css( 'font-size', 'inherit' ); }
					}],
					initComplete: function(settings){
						var api = new $.fn.dataTable.Api( settings );
						$('.table-filter-container', api.table().container()).append(
							$('#table-filter2').detach().show()
							);
						$('#table-filter2 select').on('change', function(){
							table.search(this.value).draw();   
						});       
					}
				});
	</script>

	<script>
	$(document).ready(function(){
		$('#errMsg').fadeOut(5000); 

		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});

	});
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src='js/fullcalendar.min.js'></script>
</body>

</html>