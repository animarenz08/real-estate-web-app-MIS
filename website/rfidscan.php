<?php
include '../common/class.users.php';
	session_start();
	$currentMenu = 10;
   $userGroup = 0;

  $user = new User();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<script type="text/javascript" src="js/jquery.scannerdetection.js"></script>
  <script type="text/javascript">
$(document).scannerDetection({
  timeBeforeScanTest: 200, // wait for the next character for upto 200ms
  avgTimeByChar: 100, // it's not a barcode if a character takes longer than 100ms
  onComplete: function(rfid, qty){
    var content = rfid;
      $.ajax({
        url: "loaddata.php",
        global: false,
        type: "POST",
        data: ({ id : content }),
        dataType: "html",
        async:false,
        success: function(data) {
           $('div#result').html(data);
        }
    });
    } // main callback function 
});
</script>
</head>
<body>
	<?php include 'mainHeader.php'; ?>

  <div class="content" style="padding:0px">
    
      <h2 style="text-align: center;">Tap your card in RFID Reader to show your Account Information</h2>

  </div>
   
      <div id="result"></div>
   

  </div>
  <div id="brs">
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
  </div>
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  <?php include 'footerFiles.php'; ?>

  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>