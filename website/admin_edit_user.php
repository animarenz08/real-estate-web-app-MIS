<?php
  include '../common/class.users.php';
  include '../common/class.properties.php';

	session_start();
	$currentMenu = 54;
  $userGroup = 1;


  $user = new User();

  $user->isPageAccessible($_SESSION['user_type'], $userGroup);

if(isset($_GET['id']))
{
  $id=$_GET['id'];
  $editUser = $user->getUserbyUserId($id);
  $educ = $user->getUserEducbyUserId($id);
}

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
  <?php include 'mainHeader.php'; ?>

  <div class="content">

    <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Edit User Details </h2>
    <br>

    <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <br>
        <form method="post" role="form" action="commonFunctions.php">
          <?php foreach($editUser as $row) { ?>
          <input type="hidden" class="userId" name="idUser" value="<?php echo $row['dh_user_id']; ?>">
                    <div style="clear:both;"></div>

                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>ID Number</label>
                            <input type="text" required class="form-control" name="userId" value="<?php echo $row['dh_user_id']; ?>">
                          </div>
                            <div class="col-sm-3">
                                <label>First Name</label>
                                <input type="text" required class="form-control" name="firstName" pattern="[a-zA-Z][a-zA-Z ]{2,}" value="<?php echo $row['dh_firstName']; ?>">
                            </div>
                            <div class="col-sm-3">
                                <label>Middle Name</label>
                                <input type="text" class="form-control" name="middleName" pattern="[a-zA-Z][a-zA-Z ]{,2}" value="<?php echo $row['dh_middleName']; ?>">
                            </div>
                            <div class="col-sm-3">
                                <label>Last Name</label>
                                <input type="text" required class="form-control" name="lastName" pattern="[a-zA-Z][a-zA-Z ]{2,}" value="<?php echo $row['dh_lastName']; ?>">
                            </div>
                            <!-- <div class="col-sm-2">
                                <label>Name of Spouse, if married</label>
                                <input type="text" class="form-control" name="spouseName" pattern="[a-zA-Z][a-zA-Z ]{3,}" value="<?php echo $row['dh_user_spousename']; ?>">
                            </div> -->
                        </div>
                    </div>
                    <div style="clear:both;">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Nickname</label>
                                <input type="text" class="form-control" name="nickName" pattern="[a-zA-Z][a-zA-Z ]{2,}" value="<?php echo $row['dh_user_nickname']; ?>">
                            </div>

                            <div class="col-sm-4">
                                <label>Birthdate</label>
                                <div class="input-group date form_date col-sm-12" data-date="" data-date-format="MM dd, yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="5" name="DOB" type="text" value="<?php echo $row['dh_bday']; ?>" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="dtp_input1" name="DOB" value="<?php echo $row['dh_bday']; ?>" /><br/>
                            </div>
                
                            <div class="col-sm-2">
                                <label>Age</label>
                                    <input type="text" required class="form-control" pattern="[0-9]{2,3}" name="Age" id="Age" value="<?php echo $row['dh_age']; ?>">
                            </div>

                            <div class="col-sm-3" style="text-align: center;">
                                <label>Gender</label>
                                <div class="row">
                                  <input type="hidden" value="<?php echo $row['dh_gender']; ?>">
                                  <input type="radio" name="gender" <?php if ($row['dh_gender'] == 'Male' ){ ?> checked="checked" <?php } ?> id="1" required value="Male"  />
                                  <span class="radiotext">Male</span>

                                  <input type="radio" name="gender" <?php if ($row['dh_gender'] == 'Female' ){ ?> checked="checked" <?php } ?> value="Female" />
                                  <span class="radiotext">Female</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>TIN(Tax Identication Number)</label>
                                <input type="text" class="form-control" name="tinNumber" value="<?php echo $row['dh_tin_number']; ?>">
                            </div>

                            <div class="col-sm-4">
                                <label>Email Address</label>
                                <input type="email" required class="form-control" name="emailAddress" value="<?php echo $row['dh_email_address']; ?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Contact Number</label>
                                <input type="text" required class="form-control" name="contactNo" pattern="[0-9]{11,13}" value="<?php echo $row['dh_contact_no']; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Address</label>
                                <input type="text" name="homeAddress" required class="form-control"  value="<?php echo $row['dh_home_address']; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Employee Username</label>
                                <input type="text" name="username" required class="form-control"  minlength="8" value="<?php echo $row['dh_user_name']; ?>">
                            </div>

                        </div>
                    </div>

                    <?php foreach($educ as $educData) { ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-1">
                                <label>Education</label>
                                <input type="text" name="educSchool[]" class="form-control" value="<?php echo $educData['dh_school_name']; ?>">
                            </div>
                            <div class="col-sm-2">
                                <label>Location</label>
                                <input type="text" name="educLocation[]" class="form-control" value="<?php echo $educData['dh_school_location']; ?>">
                            </div>
                            <div class="col-sm-2">
                                <label>Attainment</label>
                                <input type="text" name="educAttainment[]" class="form-control" value="<?php echo $educData['dh_attainment']; ?>">
                            </div>
                            <div class="col-sm-1">
                                <label>From:</label>
                                <input type="text" name="educYearFrom[]" class="form-control" value="<?php echo $educData['dh_year_from']; ?>">
                            </div>
                            <div class="col-sm-1">
                                <label>To:</label>
                                <input type="text" name="educYearTo[]" class="form-control" value="<?php echo $educData['dh_year_to']; ?>">
                            </div>
                            <input type="hidden" name="educId[]" value="<?php echo $educData['dh_user_educ_id']; ?>">
                        </div>
                    </div>
                        <?php } ?>

                    <br>
                    <br>
                    <div class="form-group clearfix">
                      <div class="row">
                        <div class="col-sm-4 col-sm-offset-5">
                          <button type="submit" name="updateUser" class="btn btn-primary btn-md">Update</button>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>
              </div>
            </div>

  <div id="UpdateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p> Are you sure you want to <b><span id="modalText"> </span></b></p>
        </div>
        <div class="modal-footer">
          <form method="post" role="form" action="">
            <input type="hidden" id="houseprojId" name="houseprojId">
            <input type="hidden" id="houseName" name="houseName">
            <button type="submit" name="updateHouse" class="btn btn-success btn-md" > Submit </button>
            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
 <script src="js/jquery.js"></script>
  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
  <script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
  <script type="text/javascript">
        $('.form_date').datetimepicker({
                language:  'ar',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
            });
    </script>

<!-- Bootstrap Core JavaScript -->
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>