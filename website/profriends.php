<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

    $prop = new Property();
    
    $myProj = $prop->getAllHouseProjects();
    $subd = $prop->getAllSubdivision();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<div class="content">

  <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"> Pro Friends </a></li>
            <li><a data-toggle="tab" href="#about"> About Pro Friends </a></li>
            <li><a data-toggle="tab" href="#subd"> Pro Friends Subdivisions </a></li>
            <li><a data-toggle="tab" href="#house"> Pro Friends House Project </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For home antel -->
          <div id="home" class="tab-pane fade in active" style="padding:25px;">
            <div class="antelhead">
              <img src="subimg/lancasterheader.jpg" alt="">
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                <img src="subimg/probelle.jpg" style="width:100%;">
              </div>
              <div class="col-sm-5">
                <h2>Pro Friends</h2>
                <h3>Tanzang Luma, Imus, Cavite</h3>
                Property Company of Friends, Inc., also known as PRO-FRIENDS, is one of the fastest growing property developers in the Philippines, with over 42,000 quality and affordable housing units built since its inception in 1999.
                Having completed and delivered 17 projects with nine more on-going, our company has grown from being a provider of houses in small, pocket developments, to later include medium rise condominiums, estates for low, middle and higher end markets, and, now, a flourishing township in our portfolio.

                <br>
                <br>
                Professionalism and customer service are key elements of our business philosophy. First-time home buyers or those looking to upgrade their quality of life will find that PRO-FRIENDS is a suitable partner to begin the process. Our portfolio offers a wide array of house models – there is one sure to match your family’s lifestyle and budget.
              </div>
            </div>
          </div>
          <!-- end of home -->
          <!-- start of about antel -->
          <div id="about" class="tab-pane fade" style="padding: 25px;">
            <div class="buyer-wrap">
              <div class="g-grids">
                <h3>Company Profile</h3>

                <p>Property Company of Friends, Inc., also known as PRO-FRIENDS, is one of the fastest growing property developers in the Philippines. Pro-Friends has grown from being a provider of homes for the low income group in small pocket developments, to medium rise condominiums and townhouses for the middle income earners, as well as single detached units in estate developments for the higher end market.
                <br>
                It was founded in 1999 by a group of friends who believed in creating communities and transforming lives of the Filipino families.
                PRO-FRIENDS aims to give the best dream home packaged with unmatched convenience in strategic locations. We are committed to provide quality homes at friendly rates. </p>
                <br>
              </div>
              <div class="g-grids">
                <h3>Service Excellence</h3>
                <p>Service Excellence is one significant distinction that we would like to make in the real estate industry. Service excellence implies going the extra mile to delight the customer and having the commitment to deliver "against the odds". It also requires innovation.</p>
                <br>

              </div>

              <div class="g-grids">
                <h3>Teamwork</h3>
                <p>The essence of Teamwork is synergy. More than the "Sense of Family", teamwork puts a premium on learning and growing as a team and relationships that build trust. This core value also implies getting rid of organizational "silos" and turf-mentality within PRO-FRIENDS family. Teamwork also involves the values of respect and dignity.</p>
                <br>
              </div>

              <div class="g-grids">
                <h3>Sense of Family</h3>
                <p>Learning together, having a sense of safety and security, an atmosphere of belongingness, and being able to entrust accountability – these make up a culture of shared responsibility in fostering positive developments among each other.</p>
                <br>
              </div>
            </div>
          </div>
          <!-- end of about -->
          <!-- start of subdivisions -->
          <div id="subd" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($subd as $subdData) { ?>
            <?php if($subdData['dh_subd_dev_id'] == '4') { ?>
            <div class="col-sm-4">
              <img src="<?php echo $subdData['dh_subd_logo']; ?>" style="width: 100%;" alt="">
              <h3><?php echo $subdData['dh_subd_name']; ?></h3>
              <h4><?php echo $subdData['dh_subd_location']; ?></h4>
              <p><?php echo $subdData['dh_subd_description']; ?></p>
            </div>
            <?php } ?>
            <?php } ?>
          </div>
          <!-- end of subdivisions -->
          <!-- start of houses -->
          <div id="house" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($myProj as $proj) { ?>
            <?php if($proj['dh_subd_id'] == '3' OR $proj['dh_subd_id'] == '4' OR $proj['dh_subd_id'] == '5') { ?>
            <div class="col-sm-4">
                <a href="<?php echo $proj['dh_image_path']; ?>"><img src="<?php echo $proj['dh_image_path']; ?>" style="width: 100%;" alt=""></a>
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
                <p><?php echo $proj['dh_description']; ?></p>
                <div class = "viewbutton">
                  <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details">
                  </form>
                </div>
                <br>
              </div>
              <?php } ?>
              <?php } ?>
          </div>
          <!-- end of houses -->
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>