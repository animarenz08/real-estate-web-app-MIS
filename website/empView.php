<?php
include '../common/class.users.php';
   session_start();
   $currentMenu = 52;
   $userGroup = 5;

   $user = new User();

   $user->isPageAccessible($_SESSION['user_type'], $userGroup);
  $getClosingSales = $user->getAllClosingSales();

?>

<!DOCTYPE html>
<html>
<head>
   <?php include 'headerFiles.php'; ?>
</head>
<body>
   <?php include 'mainHeader.php'; ?>

   <div class="content">
      <?php if(isset($Message)){ ?>
         <div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
            &nbsp; <?php echo $Message; ?>!</div>
            <?php unset($_SESSION["Message"]); } ?>

            <h2 style="text-align:center; text-transform: uppercase;margin:0;"> Closing Sales View </h2>
            <br>
            <div class="row">
               <div class="col-md-10 col-sm-offset-1">
                  <div class="encoder-container">
                     <ul class="nav nav-tabs">
                     </ul>
                  </div>
                  <div class="tab-content">
                     <div id="closingApproval" class="tab-pane fade in active">
                        <hr>
                        <table class="table table-condensed table-hover table-striped" id="closingtable">
                           <thead>
                              <tr>
                                 <th hidden> Closing Id </th>
                                 <th> Full Name </th>
                                 <th> Phone No </th>
                                 <th> Telephone No </th>
                                 <th> Complete Address </th>
                                 <th> Developer </th>
                                 <th> House Model </th>
                                 <th> Floor & Lot Area </th>
                                 <th> TCP/Checked w/ Dev </th>
                                 <th> Reservation Fee </th>
                                 <th> Division </th>
                                 <th> Prepared By </th>
                                 <th> Closing Sale Date </th>
                                 <th> Date Submitted </th>
                                 <th> Status </th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php if(count($getClosingSales) > 0) { ?> 
                                 <?php foreach ($getClosingSales as $saleData) { ?>
                                    <tr>
                                       <td hidden><?php echo $saleData['dh_closing_id']; ?></td>
                                       <td> <?php echo $saleData['dh_Firstname'].' '.$saleData['dh_familyName']; ?> </td>
                                       <td> <?php echo $saleData['dh_phoneNo']; ?> </td>
                                       <td> <?php echo $saleData['dh_telNo']; ?> </td>
                                       <td> <?php echo 'Blk '.$saleData['dh_block'].' & Lot '.$saleData['dh_lot'].' Phase '.$saleData['dh_phase'].', '.$saleData['dh_subdivision'].', '.$saleData['dh_location']; ?> </td>
                                       <td> <?php echo $saleData['dh_developer']; ?> </td>
                                       <td> <?php echo $saleData['dh_houseModel']; ?> </td>
                                       <td> <?php echo $saleData['dh_floorArea'].'sqm - '.$saleData['dh_lotArea'].'sqm'; ?> </td>
                                       <td>₱ <?php echo $saleData['dh_tcpDev']; ?> </td>
                                       <td>₱ <?php echo $saleData['dh_reservationFee']; ?> </td>
                                       <td> <?php echo $saleData['dh_division']; ?> </td>
                                       <td class="fullName"> <?php echo $saleData['dh_firstName'].' '.$saleData['dh_lastName']; ?> </td>
                                       <td> <?php echo $saleData['dh_closingDate']; ?> </td>
                                       <td> <?php echo $saleData['dh_created_date']; ?> </td>
                                       <td <?php if($saleData['dh_closing_status'] == 'Declined'){ ?> style="vertical-align: middle; color:red;" <?php } elseif($saleData['dh_closing_status'] == 'For Approval') { ?> style="vertical-align: middle; color:#337ab7;" <?php }else{ ?> style="vertical-align: middle; color:#5cb85c;" <?php } ?> > <?php echo $saleData['dh_closing_status']; ?> </td>
                                    </tr>
                                    <?php } ?>
                                    <?php } else { ?>
                                       <tr> 
                                          <td colspan="14" style="text-align: center; color:red;"> 
                                             <h2> No Closing Sales </h2>
                                          </td>
                                       </tr>
                                       <?php } ?>
                           </tbody>
                        </table>  
                     </div>
                  </div>
               </div>
            </div>
   </div>


   <?php include 'footerFiles.php'; ?>
   <script src="js/jquery.js"></script>
   <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
   <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
   <script>
      $(document).ready(function(){

         $('#closingtable').DataTable();
      });

   </script>
   <script>
      $(document).ready(function(){
         $('#errMsg').fadeOut(5000); 

         // hide #back-top first
         $("#back-top").hide();

         // fade in #back-top
         $(function () {
            $(window).scroll(function () {
               if ($(this).scrollTop() > 100) {
                  $('#back-top').fadeIn();
               } else {
                  $('#back-top').fadeOut();
               }
            });

            // scroll body to 0px on click
            $('#back-top a').click(function () {
               $('body,html').animate({
                  scrollTop: 0
               }, 800);
               return false;
            });
         });
      });
   </script>
</body>

</html>