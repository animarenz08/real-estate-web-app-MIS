<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 214;
    $developerid = 2;
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
  .modelunits img{
    width: 200px;
    height: 120px;
}
</style>
</head>
<body>
  <?php include 'devHeader.php'; ?>

<div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="houseshead">
            <a href="img/anastacia.jpg"><img src="subimg/anastacia.jpg" alt=""></a>
          </div>
        </div>
        
        <div class="col-sm-6">
        <h3>Model Units</h3>
          <div class="modelunits">
            <div class="col-sm-5">
              <a href="audrey.php"><img src="subimg/audrey.jpg" alt=""></a>
              <h4>Audrey</h4>
            </div>
            <div class="col-sm-5">
              <a href="karylle.php"><img src="subimg/karylle.jpg" alt=""></a>
              <h4>Karylle</h4>
            </div>
          </div>
        </div>
    </div>

    <div style="clear:both;">
    </div>

      <div class="row">
        <div class="housescontent">
          <div class="col-sm-6">
            <h2>Anastacia Model House</h2>
            <br>
            With a name that reminds one of timeless fairy tales, the grand facade of this house model is like a simple castle in a modern urban neighborhood. A huge balcony is always there to hold feasts and parties with friends and loved ones.
            <br>
            <br>
            <h4>Floor Plan</h4>
            <div class="col-sm-6">
              <h5>GROUND FLOOR</h5>
                Carport: &nbsp;20.60 sq.m<br>
                Entry: &nbsp;1.45 sq.m<br>
                Dining: &nbsp;12.00 sq.<br>
                Kitchen: &nbsp;8.50 sq.m<br>
                Laundry: &nbsp;7.40 sq.m<br>
                Powder Rm: &nbsp;3.00 sq.m<br>
                Lanai: &nbsp;8.00 sq.m<br> 
              
              </div>
              <div class="col-sm-6">
               <h5>SECOND FLOOR</h5>
               
                Stairs: &nbsp;4.12 sq.m<br>
                Hallway: &nbsp;3.79 sq.m<br>
                Master Bedroom: &nbsp;17.40 sq.m<br>
                Bedroom 2:14.57 sq.m<br>
                Bedroom 3: &nbsp;9.90 sq.m<br>
                Master T & B: &nbsp;3.51 sq.m<br>
                T & B: &nbsp;3.24 sq.m<br>
                Balcony: &nbsp;11.80 sq.m<br>
               
               </div>
          </div>
        </div>
        <div class="col-sm-6">
            <h3>Anastacia's Finish</h3>
            <ul class="circle">
              <li>Pre-painted roof on metal frames.</li>
              <li>Plain cement painted finish on external masonry walls.</li>
              <li>Concrete slab at 2nd floor with vinyl floor tiles.</li>
              <li>Ceramic wall and floor tiles on all toilets.</li>
              <li>Ceramic floor tiles on Living, Dining, Kitchen, Entry, Lanai, and Balcony area.</li>
              <li>Granite kitchen counter top with stainless steel sink, with base and hanging cabinet</li>
              <li>Panel Type Door at entry, PVC Door on all toilets and Flush type door on all other, with Cylindrical type lockset.</li>
              <li>Aluminum window w/ clear glass panel.</li>
              <li>Painted ficemboard or gypsum bd. partition on metal frames.</li>
              <li>Painted ficemboard of gypsum bd ceiling with metal frames.</li>
              <li>Plain cemant painted finish on interior and exterior masonry walls.</li>
              <li>PVC pipe plumbing roughing-ins.</li>
              <li>Hcg brand plumbing fixtures or approved equal.</li>
              <li>Steel stair & balcony railing.</li>
            </ul>
        </div>
      </div>

      <div style="clear:both">
      </div>

    </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>