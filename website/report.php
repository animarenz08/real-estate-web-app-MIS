<?php
	include '../common/class.users.php';
		session_start();
		$currentMenu = 12;
		$userGroup = 1;

	$user = new User();

	$user->isPageAccessible($_SESSION['user_type'], $userGroup);
	$myUsers = $user->getAllUserDetails($_SESSION['user_session']);
	$mySubd = $user->getAllSubdivision($_SESSION['user_session']);

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<link href="locales/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
		<?php include 'mainHeader.php'; ?>

	<div class="content">
		<?php if(isset($Message)){ ?>
				<div class="alert <?php if($MsgCode != 2){ ?> alert-success <?php } else { ?> alert-danger <?php } ?>" id="errMsg">
			&nbsp; <?php echo $Message; ?>!</div>
		<?php unset($_SESSION["Message"]); } ?>

		<br>
		<div class="row">
			<div class="col-md-10 col-sm-offset-1">
				<div class="encoder-container">
					<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#myUser"> User Report </a></li>
							<li><a data-toggle="tab" href="#mySubd"> Subdivision Report </a></li>
					</ul>
				</div>
				<div class="tab-content">
					<!-- Start For Closing Sales -->
					<div id="myUser" class="tab-pane fade in active" style="padding:25px;">
						<h2> All Users </h2>
							<hr>
							<table class="table table-condensed table-hover table-striped" id="myClosingSales">
							<thead>
								<tr>
									<th> Full Name </th>
									<th> Email Address </th>
									<th> Contact # </th>
									<th> Gender </th>
									<th> Birth Date </th>
									<th> Age </th>
									<th> Date Created </th>
									<th> User Status </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($myUsers) > 1) { ?> 
									<?php foreach ($myUsers as $userData) { ?>
										<tr>
												<td> <?php echo $userData['dh_firstName'].' '.$userData['dh_middleName'].' '.$userData['dh_lastName']; ?> </td>
												<td> <?php echo $userData['dh_email_address']; ?> </td>
												<td> <?php echo $userData['dh_contact_no']; ?> </td>
												<td> <?php echo $userData['dh_gender']; ?> </td>
												<td> <?php echo $userData['dh_bday']; ?> </td>
												<td> <?php echo $userData['dh_age']; ?> </td>
												<td> <?php echo $userData['dh_date_created']; ?> </td>
												<td> <?php echo $userData['dh_status']; ?> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="7" style="text-align: center; color:red;"> 
												<h2> No User Account </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table> 	
					</div>
					<!-- End for Closing Sales -->

					<!-- Start For Calendar Viewing -->
					<div id="mySubd" class="tab-pane fade" style="padding:25px;">
					<h2> All Subdivisions </h2>
							<hr>
							<table class="table table-condensed table-hover table-striped" id="myClosingSales">
							<thead>
								<tr>
									<th> Subdivision Name </th>
									<th> Subdivision Location </th>
									<th> Subdivision Description </th>
									<th> Subdivision Logo </th>
								</tr>
							</thead>
							<tbody>
									<?php if(count($mySubd) > 0) { ?> 
									<?php foreach ($mySubd as $subdData) { ?>
										<tr>
												<td> <?php echo $subdData['dh_subd_name']; ?> </td>
												<td> <?php echo $subdData['dh_subd_location']; ?> </td>
												<td> <?php echo $subdData['dh_subd_description']; ?> </td>
												<td><img src="<?php echo $subdData['dh_subd_logo']; ?>" style="height: 50%;" /> </td>
										</tr>
									<?php } ?>
									<?php } else { ?>
										<tr> 
											<td colspan="7" style="text-align: center; color:red;"> 
												<h2> No Subdivisions </h2>
											</td>
										</tr>
									<?php } ?>
							</tbody>
						</table>													
					</div>
					<!-- End for Calendar Viewing -->


				</div>
			</div>
		</div>
		
	</div>

	<?php include 'footerFiles.php'; ?>
	<script src="js/jquery.js"></script>
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="locales/jquery-1.8.3.min.js" charset="UTF-8"></script>
	<script type="text/javascript" src="locales/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
	<script type="text/javascript">
				$('.form_date').datetimepicker({
								language:  'ar',
								weekStart: 1,
								todayBtn:  1,
								autoclose: 1,
								todayHighlight: 1,
								startView: 2,
								minView: 2,
								forceParse: 0,
						});
		</script>
<script>
$(document).ready(function(){
	$('#errMsg').fadeOut(5000); 

	$('#seminarCalendar').fullCalendar({
	  header: {
	    left: 'prev,next ',
	    center: 'title',
	    right: 'month,agendaWeek,listWeek'
	  },
	  height: 470,
	  navLinks: true,
	  events: {
	    url: 'getSeminarEvents.php'
	  }
	});

	$('#vehicleCalendar').fullCalendar({
	  header: {
	    left: 'prev,next ',
	    center: 'title',
	    right: 'month,agendaWeek,listWeek'
	  },
	  height: 470,
	  navLinks: true,
	  events: {
	    url: 'getVehicleEvents.php'
	  }
	});
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

});
</script>
<script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>

<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src='js/fullcalendar.min.js'></script>
</body>

</html>