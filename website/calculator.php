<?php
	session_start();
	$currentMenu = 5;
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>

</head>
<body>
	<?php include 'mainHeader.php'; ?>
	<div class="content">
		<h3 style="text-align: center; color:green;">Bank Housing Loan Calculator</h3>
		
		<div class="col-sm-offset-5">
		<form method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label> Selling Price: </label>
						<input class="form-control" type="text" name="loan"/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label>Down Payment: </label>
						<select class="form-control" name="dp">
							<option selected=""></option>
						  	<option value=".2">20%</option>
						  	<option value=".3">30%</option>
						  	<option value=".4">40%</option>
						  	<option value=".5">50%</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label>Payment Terms:</label>
						<select class="form-control" name="month">
							<option selected=""></option>
						  	<option value="12">1 Year</option>
						  	<option value="24">2 Years</option>
						  	<option value="36">3 Years</option>
						  	<option value="48">4 Years</option>
						  	<option value="60">5 Years</option>
						  	<option value="72">6 Years</option>
						</select><br><br>
					</div>
				</div>
			</div>

			<div class="form-group clearfix">
				<div class="col-sm-2">
					<button type="submit" class="btn btn-primary btn-sm" style="width:100%" name="sub">Calculate</button>
				</div>
			</div>
		</form>
		
	</div>
	<?php

			if($_POST){
				$loan = $_POST['loan'];
				$dp = $_POST['dp'];
				$month = $_POST['month'];

				if($month=="12"){
					$rt = 0.0525;
				}elseif ($month=="24") {
					$rt = 0.06;
				}elseif ($month=="36") {
					$rt = 0.063;
				}elseif ($month=="48") {
					$rt = 0.065;
				}elseif ($month=="60") {
					$rt = 0.068;
				}elseif ($month=="72") {
					$rt = 0.075;
				}
				$dpay = $loan * $dp;
				$amount = $loan - $dpay;
				$rate = $amount / $month * $rt;
				$amort = $amount / $month + $rate;

				echo '<div class="row">';
				echo '<div class="col-sm-2 col-sm-offset-5" style="border: 1px solid; text-align: center;">';
				echo "<label>Selling Price</label>" . "<br><p>₱ " . $loan . "</p>";
				echo "<label>Downpayment</label>" . "<br><p>₱ " . $dpay . "</p>";
				echo "<label>Amount Financed</label>" . "<br><p>₱ " . $amount . "</p>";
				echo "<label>Monthly Amortization</label>" . "<br><p>₱ " . (round($amort,2)) . "</p>";
				echo "</div>";
				echo "</div>";
				
			}

		?>
	
	</div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>