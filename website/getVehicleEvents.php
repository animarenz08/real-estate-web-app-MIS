<?php

include '../common/class.ajax.php';

$ajaxFunc = new ajaxFunction();

$result = $ajaxFunc->getAllVehicleTrips();

$event_array = array();
foreach($result as $data){
	$originalDate = $data['dh_trip_date'];
	$newDate = date($originalDate);

	$event_array[] = array(
		'title' => $data['dh_location_pickup'].' - '.$data['dh_location_destination'] ,
		'start' => $newDate,
		'description' => 'Pickup Time: '.$data['dh_location_pickup_time'].' Destination Time: '.$data['dh_location_destination_time'].' Driver Name: '.$data['dh_driver_name'].' | Vehicle Plate No: '.$data['dh_plateno'].' | Driver Contact No: '.$data['dh_driver_contact']
	);
}

echo json_encode($event_array);

?>

