<?php
  include '../common/class.properties.php';
    session_start();
    $currentMenu = 3;

    $prop = new Property();
    
    $myProj = $prop->getAllHouseProjects();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'headerFiles.php'; ?>
<style type="text/css">
 
</style>
</head>
<body>
  <?php include 'mainHeader.php'; ?>

<div class="content">

  <div class="row">
      <div class="col-md-10 col-sm-offset-1">
        <div class="encoder-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"> Antel Grand Village </a></li>
            <li><a data-toggle="tab" href="#about"> About Antel </a></li>
            <li><a data-toggle="tab" href="#house"> Antel House Project </a></li>
          </ul>
        </div>
        <div class="tab-content">
          <!-- Start For home antel -->
          <div id="home" class="tab-pane fade in active" style="padding:25px;">
            <div class="antelhead">
              <img src="subimg/headantel.jpg" alt="">
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                <img src="subimg/antels.jpg">
              </div>
              <div class="col-sm-5">
                <h2>Antel Grand Village</h2>
                <h3>Brgy. Bacao 2, General Trias, Cavite</h3>
                The first and only self-contained island community, south of Metro Manila, with more than 100 hectares of carefully planned residential villages, social and commercial districts, and a 1.7- hectare clubhouse, all in the midst of verdant sceneries and breath-taking riverside vistas.

                <br>
                <br>
                AGV Island Community is strategically located between the urban municipalities of Kawit and General Trias, a few minutes away from the booming industrial zone CEZA, and a convenient stop-over on the way to Tagaytay and Batangas through the R-1 diversion road.
              </div>
            </div>
          </div>
          <!-- end of home -->
          <!-- start of about antel -->
          <div id="about" class="tab-pane fade" style="padding: 25px;">
            <div class="buyer-wrap">
              <div class="g-grids">
                <img src="uploads/agc.png" style="width:20%;" />
                <h3>Company Profile</h3>

                <p>A development that is designed around and is tempered by human and natural elements. Committed to the protection and management of the environment. Constantly conscious of our responsibility to care for the Earth and to help shape a living and working environment in which Filipinos live in harmony with nature. Strategically developing properties that will spur progress as well as add value to the communities in which they are located. </p>
                <br>
              </div>
              <div class="g-grids">
                <h3>Solid Company</h3>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
                <br>

              </div>

              <div class="g-grids">
                <h3>Construction Innovation</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit, orci eu facilisis efficitur, felis ipsum mollis ipsum, a iaculis lacus leo in libero. Duis vehicula turpis eu tincidunt viverra. Fusce vehicula augue suscipit, congue nisi sitamet, dapibus lectus. Morbi rutrum ipsum at pretium lacinia.</p>
                <br>
              </div>

              <div class="g-grids">
                <h3>Groundbreaking Concepts</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit, orci eu facilisis efficitur, felis ipsum mollis ipsum, a iaculis lacus leo in libero. Duis vehicula turpis eu tincidunt viverra. Fusce vehicula augue suscipit, congue nisi sitamet, dapibus lectus. Morbi rutrum ipsum at pretium lacinia.</p>
                <br>
              </div>
            </div>
          </div>
          <!-- end of about -->
          <!-- start of houses -->
          <div id="house" class="tab-pane fade" style="padding: 25px;">
            <?php foreach ($myProj as $proj) { ?>
            <?php if($proj['dh_subd_id'] == '2') { ?>
            <div class="col-sm-4">
                <a href="<?php echo $proj['dh_image_path']; ?>"><img src="<?php echo $proj['dh_image_path']; ?>" style="width: 100%;" alt=""></a>
                <h3><?php echo $proj['dh_house_name']; ?></h3>
                <h5>Selling Price: ₱<?php echo $proj['dh_house_price']; ?></h5>
                <p><?php echo $proj['dh_description']; ?></p>
                <div class = "viewbutton">
                  <form method = "post" action = "house_details.php" target = _blank>
                  <input name = "<?php echo $proj['dh_house_proj_id']; ?>" class = "sb-btn sb-btn-style-2" type = "submit" value = "View Details">
                  </form>
                </div>
                <br>
              </div>
              <?php } ?>
              <?php } ?>
          </div>
          <!-- end of houses -->
        </div>
      </div>
    </div>
  </div>

  <?php include 'footerFiles.php'; ?>
  <script src="js/jquery.js"></script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    </script>
     <script type="text/javascript">
      $('#errMsg').fadeOut(5000); 
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>