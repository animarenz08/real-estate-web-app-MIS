-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2018 at 11:49 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dh_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `dh_closing`
--

CREATE TABLE `dh_closing` (
  `dh_closing_id` int(11) NOT NULL,
  `dh_familyName` varchar(150) DEFAULT NULL,
  `dh_Firstname` varchar(45) DEFAULT NULL,
  `dh_phoneNo` varchar(45) DEFAULT NULL,
  `dh_telNo` varchar(45) DEFAULT NULL,
  `dh_subdivision` varchar(45) DEFAULT NULL,
  `dh_location` varchar(45) DEFAULT NULL,
  `dh_developer` varchar(45) DEFAULT NULL,
  `dh_phase` varchar(20) DEFAULT NULL,
  `dh_block` varchar(20) DEFAULT NULL,
  `dh_lot` varchar(20) DEFAULT NULL,
  `dh_houseModel` varchar(45) DEFAULT NULL,
  `dh_floorArea` varchar(45) DEFAULT NULL,
  `dh_lotArea` varchar(45) DEFAULT NULL,
  `dh_termsPayment` varchar(45) DEFAULT NULL,
  `dh_tcpDev` varchar(45) DEFAULT NULL,
  `dh_reservationFee` varchar(45) DEFAULT NULL,
  `dh_oiPR` varchar(45) DEFAULT NULL,
  `dh_division` varchar(45) DEFAULT NULL,
  `dh_divisionManager` varchar(45) DEFAULT NULL,
  `dh_div_comrate` varchar(10) DEFAULT NULL,
  `dh_salesDirector` varchar(45) DEFAULT NULL,
  `dh_dir_comrate` varchar(10) DEFAULT NULL,
  `dh_salesTrainee` varchar(45) DEFAULT NULL,
  `dh_sales_comrate` varchar(10) DEFAULT NULL,
  `dh_rc_divisionManager` varchar(45) DEFAULT NULL,
  `dh_rcdiv_comrate` varchar(10) DEFAULT NULL,
  `dh_rc_salesDirector` varchar(45) DEFAULT NULL,
  `dh_rcdir_comrate` varchar(10) DEFAULT NULL,
  `dh_rc_salesTrainee` varchar(45) DEFAULT NULL,
  `dh_rcsales_comrate` varchar(10) DEFAULT NULL,
  `dh_rfa` varchar(45) DEFAULT NULL,
  `dh_rfa_comrate` varchar(10) DEFAULT NULL,
  `dh_bookAssistant` varchar(45) DEFAULT NULL,
  `dh_book_comrate` varchar(10) DEFAULT NULL,
  `dh_aAssistant` varchar(45) DEFAULT NULL,
  `dh_a_comrate` varchar(10) DEFAULT NULL,
  `dh_closingDate` date NOT NULL,
  `dh_created_date` date NOT NULL,
  `dh_user_id` varchar(45) DEFAULT NULL,
  `dh_prepared_user_id` int(11) DEFAULT NULL,
  `dh_closing_status` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_closing`
--

INSERT INTO `dh_closing` (`dh_closing_id`, `dh_familyName`, `dh_Firstname`, `dh_phoneNo`, `dh_telNo`, `dh_subdivision`, `dh_location`, `dh_developer`, `dh_phase`, `dh_block`, `dh_lot`, `dh_houseModel`, `dh_floorArea`, `dh_lotArea`, `dh_termsPayment`, `dh_tcpDev`, `dh_reservationFee`, `dh_oiPR`, `dh_division`, `dh_divisionManager`, `dh_div_comrate`, `dh_salesDirector`, `dh_dir_comrate`, `dh_salesTrainee`, `dh_sales_comrate`, `dh_rc_divisionManager`, `dh_rcdiv_comrate`, `dh_rc_salesDirector`, `dh_rcdir_comrate`, `dh_rc_salesTrainee`, `dh_rcsales_comrate`, `dh_rfa`, `dh_rfa_comrate`, `dh_bookAssistant`, `dh_book_comrate`, `dh_aAssistant`, `dh_a_comrate`, `dh_closingDate`, `dh_created_date`, `dh_user_id`, `dh_prepared_user_id`, `dh_closing_status`) VALUES
(3, 'Chan', 'Helma', '09367661063', '', 'Wellington Place', 'Molino, Cavite', 'Camella Homes', '2', '8', '4', 'Unknown', '50', '150', NULL, '850000', '15000', '', '7', '90', '10', '90', '15390', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-31', '2017-08-31', '9', 10, 'Approve'),
(4, 'Delos Santos', 'Vince', '09367661063', NULL, 'Ponticelli Hills 2', 'Molino, Cavite', 'Camella Homes', '2', '8', '4', 'Unknown', '50', '150', NULL, '850000', '15000', NULL, '7', '', '7', '', '49725', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-30', '2018-03-30', '1', 7, 'Approve'),
(5, 'Cruz', 'Dominic', '09367661063', '', 'Ponticelli Hills 3', 'Molino, Cavite', 'Camella Homes', '2', '8', '4', 'Unknown', '50', '150', NULL, '850000', '15000', '', '7', '', '8', '', '34425', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-18', '2018-04-18', '14', 8, 'Approve'),
(6, 'Valiente', 'Juno', '09530003945', '454354', 'subdivision', 'locatioj', 'Duraville Realty & Devt Corp', '5', '3', '2', 'anastacia', '25', '60', NULL, '855000', '5000', '4441', '6', '7', '8', '8', '34627.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-01', '2018-05-01', '14', 8, 'Approve'),
(7, 'Green', 'Winston', '09530003945', '1111', 'subsss', 'sdadasdasdsad', 'Pro-Friends', '6', '5', '4', 'housess', '35', '45', NULL, '855000', '5000', '1235', '4', '12', '9', '8', '34627.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-18', '2018-04-10', '6', 9, 'Approve'),
(8, 'Bell', 'Jeff', '09520296203', '', 'Wellington Place', 'dfsfds', 'Duraville Realty & Devt Corp', '3', '3', '3', 'sdfdsfdsfdsf', '33', '21', NULL, '855000', '5000', '3244', '4', '7', '4', '9', '15390', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-03', '2018-04-03', '6', 18, 'Approve'),
(9, 'Thomas', 'Andre', '09520294203', '', 'Antel Grand Village', '', 'Pro-Friends', '', '', '', '', '', '', NULL, '855000', '3000', '3454', '6', '7', '18', '9', '34627.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-05', '2018-05-05', '6', 12, 'Approve'),
(10, 'Newgate', 'Edward', '09520114203', '', 'Antel Grand Village', '', 'Duraville Realty & Devt Corp', '3', '5', '5', 'daaan', '54', '43', NULL, '800000', '15000', '4352', '10', '2', '2', '', '46800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-08', '2018-05-07', '6', 18, 'Approve'),
(11, 'Marco', 'Fushichou', '09950994203', '', 'Camella Homes', 'testign', 'MyCitiHomes', '3', '1', '2', 'rerwsfdds', '21', '21', NULL, '850000', '4000', '', '9', '2', '9564370', '3', '34425', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-09', '2018-05-07', '6', 9, 'Approve'),
(12, 'White', 'Steve', '09950294108', '', 'Hamilton Homes', '', 'Antel Grand Village', '', '', '', '', '', '', NULL, '850000', '2213', '1235', '5', '7', '9', '', '34080.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23', '2018-05-13', '6', 21, 'Approve'),
(53, 'Galicha', 'Cris King', '09123029126', '573-333', 'Lancaster New City', 'General Trias, Cavite', 'Pro-Friends', '1', '12', '20', '', '45', '35', NULL, '850000', '6000', '5123', '4', 'Joshua Santos', '0.25%', 'Ana Marie Obon', '1%', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-10-17', '2017-10-20', '6', 11, 'Approve'),
(55, 'Ramos', 'Charles', '09120294553', '', 'Carmona Estates', 'Carmona, Cavite', 'Pro-Friends', '12', '10', '6', '', '54', '65', NULL, '850000', '5000', '1231', '9', 'Alex Manlapit', '0.50%', 'Sarah Roca', '0.20%', '', '', '', '', '', '', '', '', '', '', 'Brena ', '0.01%', '', '', '2018-05-24', '2018-05-19', '6', 9564370, 'Approve'),
(57, 'Castillo', 'merely', '09359106744', '', '', '', '', '', '', '', '', '', '', NULL, '855900', '', '', '', '7', '9', '9', '0.01%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-25', '2018-05-14', '6', 2, 'Approve'),
(58, 'Dea Vega', 'Grant', '09090909099', '1234567', 'IL Giardino Residences', 'General Trias, Cavite', 'MyCitiHomes', '8', '8', '8', 'anastacia', '45', '45', NULL, '850000', '5000', '1234', '5', 'Reuben Llanes', '0.01%', 'Grant Necia', '0.01%', 'Rustine Agbayani', '0.01%', '', '0.01%', 'grant necia', '0.01%', 'sales agent', '0.01%', 'rfa ', '0.01%', 'reuben', '0.01%', 'reuben llanes', '0.01%', '2018-05-19', '2018-05-18', '6', 9564370, 'Approve'),
(60, 'fdsfsfdsf', 'sfsdfdsfds', '09123029126', '1234567', 'Lancaster New City', 'General Trias, Cavite', 'Pro-Friends', '', '', '', 'anastacia', '', '', NULL, '850000', '5000', '3454', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-10', '2018-07-03', '10', 10, 'For Approval');

-- --------------------------------------------------------

--
-- Table structure for table `dh_developers`
--

CREATE TABLE `dh_developers` (
  `dh_developers_id` int(11) NOT NULL,
  `dh_dev_name` varchar(45) DEFAULT NULL,
  `dh_dev_logo` varchar(45) DEFAULT NULL,
  `dh_dev_link` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_developers`
--

INSERT INTO `dh_developers` (`dh_developers_id`, `dh_dev_name`, `dh_dev_logo`, `dh_dev_link`) VALUES
(1, 'Camella Homes', 'logo/Camella.png', 'camella.php'),
(2, 'Antel Grand Village', 'logo/Antel.png', 'antel.php'),
(3, 'MyCitiHomes', 'logo/Citihomes.png', 'citihomes.php'),
(4, 'Pro-Friends', 'logo/profriends.png', 'profriends.php'),
(5, 'Duraville Realty & Devt Corp', 'logo/duraville.png', 'duraville.php');

-- --------------------------------------------------------

--
-- Table structure for table `dh_divisions`
--

CREATE TABLE `dh_divisions` (
  `dh_division_id` int(11) NOT NULL,
  `dh_division_name` varchar(45) DEFAULT NULL,
  `dh_division_status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dh_divisions`
--

INSERT INTO `dh_divisions` (`dh_division_id`, `dh_division_name`, `dh_division_status`) VALUES
(1, 'Division 1', 0),
(2, 'Division 2', 0),
(3, 'Division 3', 0),
(4, 'Division 4', 0),
(5, 'Division 5', 0),
(6, 'Division 6', 0),
(7, 'Division 7', 0),
(8, 'Division 8', 0),
(9, 'Division 9', 0),
(10, 'Division 10', 0),
(11, 'Division 11', 0),
(12, 'Division 12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dh_driver_info`
--

CREATE TABLE `dh_driver_info` (
  `dh_driver_id` int(11) NOT NULL,
  `dh_driver_fullname` varchar(250) DEFAULT NULL,
  `dh_driver_contactNo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_driver_info`
--

INSERT INTO `dh_driver_info` (`dh_driver_id`, `dh_driver_fullname`, `dh_driver_contactNo`) VALUES
(1, 'Aljon Villanueva', '09361234567'),
(2, 'Marlon Hufina', '09123456789'),
(3, 'Ralph Dela Vega', '09750123959');

-- --------------------------------------------------------

--
-- Table structure for table `dh_finish`
--

CREATE TABLE `dh_finish` (
  `dh_finish_id` int(10) NOT NULL,
  `dh_house_proj_id` int(11) DEFAULT NULL,
  `dh_finish_description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_finish`
--

INSERT INTO `dh_finish` (`dh_finish_id`, `dh_house_proj_id`, `dh_finish_description`) VALUES
(1, 19, 'Pre-painted roof on metal frames.'),
(2, 19, 'Plain cement painted finish on external masonry walls.'),
(3, 19, 'Concrete slab at 2nd floor with vinyl floor tiles.'),
(4, 19, 'Ceramic wall and floor tiles on all toilets.'),
(5, 19, 'Ceramic floor tiles on Living, Dining, Kitchen, Entry, Lanai, and Balcony area.'),
(6, 19, 'Granite kitchen counter top with stainless steel sink, with base and hanging cabinet'),
(7, 19, 'Panel Type Door at entry, PVC Door on all toilets and Flush type door on all other, with Cylindrical'),
(8, 19, 'Aluminum window w/ clear glass panel.'),
(10, 19, 'Painted ficemboard or gypsum bd. partition on metal frames.'),
(11, 19, 'Painted ficemboard of gypsum bd ceiling with metal frames.'),
(12, 19, 'Plain cemant painted finish on interior and exterior masonry walls.'),
(13, 19, 'PVC pipe plumbing roughing-ins.'),
(14, 19, 'Hcg brand plumbing fixtures or approved equal.'),
(15, 19, 'Steel stair & balcony railing.'),
(16, 22, 'Glazed Tiles for living room, dining room , kitchen, toilet & bathroom.'),
(17, 22, 'Plain Cement Finish w/ V-cut Finish walls.'),
(18, 22, 'Aluminum Casement windows with 8mm square Bars grilles on 1/8”thk. Clear glass finish.'),
(19, 22, 'PVC Flush Doors & jamb w/o stained glass for the bathroom.Tanguile Panel for Main Door.'),
(20, 22, 'Royu Brand convenience outlet & switches.'),
(21, 22, 'Eslon Blue water piping Line.'),
(22, 22, 'Rib Type roofing with gutter on steel rafters type of trusses on 2 x 3” cee purlins.'),
(23, 22, '1/4” thk Marine Plywood ceiling.'),
(24, 20, 'Pre-painted rib type roof on metal frames.'),
(25, 20, 'Stucco 2 coats painted finish on exterior and interior walls.'),
(26, 20, 'Concrete slab at 2nd floor with 4\" x 36\" Apo brand vinyl floor tiles wood finish.'),
(27, 20, '30x30 cm Ceramic wall and floor tiles on all toilets.'),
(28, 20, '40x40 cm Ceramic floor tiles on Living, Dining, Kitchen & Entry Porch.'),
(29, 20, 'Granite kitchen counter top, stainless steel sink with 3/4\" thick plyboard base and hanging cabinet.'),
(30, 20, 'Panel type door at entry, PVC Door on all toilets and Flush type door with grooved design on all other using Cylindrical type lockset.'),
(31, 20, 'Aluminum window with brown glass panel.'),
(32, 20, '2 coats paint ficemboard or gupsum board partition on metal frames.'),
(33, 20, '2 coats paint ficemboard or gypsum board ceiling with metal frames.'),
(34, 20, 'HCG brand plumbing fixtures.'),
(35, 20, 'Wood stair steps on metal frames with steel railing.'),
(36, 20, 'Cultured stone on selected area.'),
(37, 20, 'Floor to ceiling height on ground and 2nd floor shall be 2.70 meter, except for drop ceiling on ground floor which will be 2.40 meter more or less.'),
(38, 20, 'Bedroom cabinet shall be 3/4’ thick plyboard 2.10 meter height.'),
(66, 20, '30x30 cm Ceramic wall and floor tiles on all toilets.'),
(67, 20, '40x40 cm Ceramic floor tiles on Living, Dining, Kitchen & Entry Porch.'),
(68, 20, 'Granite kitchen counter top, stainless steel sink with 3/4\" thick plyboard base and hanging cabinet.'),
(69, 20, '30x30 cm Ceramic wall and floor tiles on all toilets.'),
(70, 20, '40x40 cm Ceramic floor tiles on Living, Dining, Kitchen & Entry Porch.'),
(71, 20, 'Granite kitchen counter top, stainless steel sink with 3/4 thick plyboard base and hanging cabinet.'),
(72, 20, '30x30 cm Ceramic wall and floor tiles on all toilets.'),
(73, 20, '40x40 cm Ceramic floor tiles on Living, Dining, Kitchen & Entry Porch.'),
(74, 20, 'Granite kitchen counter top, stainless steel sink with 3/4\" thick plyboard base and hanging cabinet.'),
(75, 40, 'Ceramic Tiles Floor Finish'),
(76, 40, 'CHB Wall rough Plastered Cement Painted Finish'),
(77, 40, 'Plain Cement Painted Finish'),
(78, 40, 'Solid HDF Doors with Netal Jam B & Lockset'),
(79, 40, 'Steel Casement Windows'),
(80, 40, 'Fiber Cement Ceiling'),
(81, 40, 'Steel Railings'),
(82, 40, 'CHB Wall Plain Cement Painted Finish, Interior Wall Partition-Fiber '),
(83, 41, 'Ceramic Tiles Floor Finish'),
(84, 41, 'CHB Wall rough Plastered Cement Painted Finish'),
(85, 41, 'Plain Cement Painted Finish'),
(86, 41, 'Exposed T-joint Textured Painted Finish'),
(87, 41, 'Solid HDF Doors with Netal Jam B & Lockset'),
(88, 41, 'Steel Casement Windows'),
(89, 41, 'Steel Railings'),
(90, 42, 'Reinforced concrete for framing, slab on fill, suspended slab'),
(91, 42, 'Steel rafter type for roof framing'),
(92, 42, 'Painted smooth finish for exterior wall with decorative mouldings'),
(93, 42, 'Painted smooth finish for interior walls'),
(94, 42, 'Plain cement finish flooring (Ground and Second Floor)'),
(95, 42, 'Painted fiber cement board on light steel framing for ceiling'),
(96, 42, 'Tiled kitchen counter top'),
(97, 42, 'Painted solid wood panel main door'),
(98, 42, 'Painted fiber cement board on light steel framing partitions'),
(99, 42, 'Aluminum sliding windows for ront and sides'),
(100, 42, 'Jalousie windows for rear windows'),
(101, 42, 'Painted ribbed type roofing'),
(102, 42, 'Complete electricals and switches'),
(103, 43, 'Reinforced concrete for framing, slab on fill, suspended slab'),
(104, 43, 'Painted smooth finish for exterior wall with decorative mouldings'),
(105, 43, 'Steel rafter type for roof framing'),
(106, 43, 'Painted smooth finish for interior walls'),
(107, 43, 'Plain cement finish flooring (Ground and Second Floor)'),
(108, 43, 'Painted fiber cement board on light steel framing for ceiling'),
(109, 43, 'Painted fiber cement board on light steel framing partitions'),
(110, 43, 'Tiled kitchen counter top'),
(111, 43, 'Aluminum sliding windows for ront and sides'),
(112, 43, 'Concrete base kitchen cabinet with painted wooden doors'),
(113, 43, 'Painted solid wood panel main door'),
(114, 43, 'Jalousie windows for rear windows'),
(115, 43, 'Painted ribbed type roofing'),
(116, 43, 'Ceramic tiled bathroom with fixtures'),
(117, 44, 'Concrete Roof Tiles'),
(118, 44, 'Powder Coated Aluminum Windows'),
(119, 44, 'Painted Exterior and Interior Wall Finish'),
(120, 44, 'Tiled Kitchen Counter with Stainless Kitchen Sink'),
(121, 44, 'Tiled Toilet & Bath with Complete Set of Bathroom Fixtures (including tissue and soap holders)'),
(122, 44, 'Tiled Ceramic Flooring for Ground and Second Floors'),
(123, 44, 'Provisions for CATV, Telephone, and Air Conditioning Outlets'),
(124, 45, 'Pre-painted GI Roofing'),
(125, 45, 'Powder-coated Aluminum Windows'),
(126, 45, 'Exterior Wall Combination of Painted Sand Blast, Plain Cement, and Tiles'),
(127, 45, 'Interior Wall Painted Plain Cement Finish'),
(128, 45, 'Tiled Kitchen Counter with Stainless Kitchen Sink'),
(129, 45, 'Toilet and Bath with Complete Set of Bathroom Fixtures'),
(130, 45, 'Tiled Ceramic Flooring for Ground and Second Floors'),
(131, 45, 'Provision for CATV, Telephone, ACU, and Internet Connection Outlets'),
(132, 46, ''),
(133, 47, ''),
(134, 48, 'Painted sandblast finish with decorative stone accent for exterior wall'),
(135, 48, 'Painted plain cement finish for interior wall'),
(136, 48, 'Tiled Toilet and Bath'),
(137, 48, 'Tiled Kitchen Tower with Stainless Sink'),
(138, 48, 'Aluminum frame sliding windows'),
(139, 48, 'Concrete stairs with wood planks'),
(140, 48, 'Wood handrail with steel decorative railing'),
(141, 48, 'Ceramic tiles for ground floor'),
(142, 48, 'PVC tiles for second floor'),
(143, 48, 'Concrete roof tiles'),
(144, 49, 'Painted sandblast finish with decorative stone accent for exterior wall'),
(145, 49, 'Painted plain cement finish for interior wall'),
(146, 49, 'Tiled Toilet and Bath'),
(147, 49, 'Tiled Kitchen Tower with Stainless Sink'),
(148, 49, 'Aluminum frame sliding windows'),
(149, 49, 'Concrete stairs with wood planks'),
(150, 49, 'Wood handrail with steel decorative railing'),
(151, 49, 'Ceramic tiles for ground floor'),
(152, 49, 'PVC tiles for second floor'),
(153, 49, 'Concrete roof tiles'),
(154, 50, 'Pre-painted G.I. sheet roofing'),
(155, 50, 'Steel casement windows'),
(156, 50, 'Painted plain cement finish for interior and exterior walls'),
(157, 50, 'Tiled kitchen counter with stainless kitchen sink'),
(158, 50, 'Tiled T&B with complete set of bathroom fixtures (inc. tissue & soap holder)'),
(159, 50, 'Plain cement finish flooring for ground and second floor'),
(160, 50, 'Provision for CATV, telephone, A/C outlet'),
(161, 51, 'Pre-painted G.I. sheet roofing'),
(162, 51, 'Steel casement windows'),
(163, 51, 'Painted plain cement finish for interior and exterior walls'),
(164, 51, 'Tiled kitchen counter with stainless kitchen sink'),
(165, 51, 'Tiled T&B with complete set of bathroom fixtures (inc. tissue & soap holder)'),
(166, 51, 'Plain cement finish flooring for ground and second floor'),
(167, 51, 'Provision for CATV, telephone, A/C outlet'),
(168, 52, 'Painted interior and exterior walls'),
(169, 52, 'Elegant ceramic tiles for bathrooms'),
(170, 52, 'Living, dining and kitchen with floor tiles'),
(171, 53, '');

-- --------------------------------------------------------

--
-- Table structure for table `dh_house_project`
--

CREATE TABLE `dh_house_project` (
  `dh_house_proj_id` int(11) NOT NULL,
  `dh_subd_id` int(11) DEFAULT NULL,
  `dh_house_name` varchar(150) DEFAULT NULL,
  `dh_house_price` varchar(150) DEFAULT NULL,
  `dh_description` varchar(300) DEFAULT NULL,
  `dh_location` varchar(45) DEFAULT NULL,
  `dh_promos` varchar(45) DEFAULT NULL,
  `dh_date_created` date NOT NULL,
  `dh_user_id` int(11) NOT NULL,
  `dh_project_path` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_house_project`
--

INSERT INTO `dh_house_project` (`dh_house_proj_id`, `dh_subd_id`, `dh_house_name`, `dh_house_price`, `dh_description`, `dh_location`, `dh_promos`, `dh_date_created`, `dh_user_id`, `dh_project_path`) VALUES
(19, 2, 'Anastacia Model', '5400000', 'With a name that reminds one of timeless fairy tales, the grand facade of this house model is like a simple castle in a modern urban neighborhood. A huge balcony is always there to hold feasts.', 'Location Unknown', 'Promos None', '2018-03-04', 1, 'anastacia.php'),
(20, 2, 'Audrey Model', '4600000', 'Simple-looking but more than what anyone can ask for. This house model has a balcony by your bedroom - take a breath of fresh air in the morning or share stories with your loved ones.', 'Location Unknown', 'Promos None', '2018-03-13', 6, 'audrey.php'),
(40, 6, 'Fiona', '1930000', 'Fiona at Il Giardino two (2) storey house will be delivered complete finish as single detached and with ceramic floor tiles and bedroom partition located General Trias, Cavite with a three (3) bedroom house 60 sqm. floor area, seated on a 120 sqm. lot for sale thru installment basis. IL Giardino is ', 'General Trias Cavite', 'No Promo', '2018-05-19', 5, NULL),
(41, 6, 'Alessia', '2300000', 'Alessia at Il Giardino two (2) storey house will be delivered complete finish as single detached in General Trias, Cavite with a three (3) bedroom house 70 sqm. floor area, seated on a 120 sqm. lot for sale thru installment basis. IL Giardino is a peaceful village with easy access to main road. Easy', 'General Trias, Cavite', 'Promos None', '2018-05-19', 5, NULL),
(42, 1, 'Reva ', '1550000', 'is a 2-storey affordable Single Attached house and lot for sale in Imus Cavite. Reva Model has a house floor area of 40 square meters built on a minimum lot area of 60 square meter. Reva has Two (2) Bedrooms, One (1) Toilet & Bath, living room, dining area, kitchen, and provision for One (1) Carport', 'Bucandala, Cavite', 'No promo', '2018-05-19', 6, NULL),
(43, 1, 'Mika ', '1600000', 'Mika is a 2-storey affordable Single Attached house and lot for sale in Imus Cavite. Mika Model has a house floor area of 46 square meters built on a minimum lot area of 60 square meter. Mika has Two (2) Bedrooms, One (1) Toilet & Bath, living room, dining area, kitchen, and provision for One (1) Ca', 'Bucandala, Imus, Cavite', '30% Discount', '2018-05-19', 6, NULL),
(44, 3, 'Alexandra', '3451680', 'Our Alexandra model house in Lancaster New City will give you an idea how to dress up a four bedroom 2-storey house with style. It is the ideal residence for big families or those who value their exterior and interior living spaces, boasting an impressive 100 square meter floor area over a 120 squar', 'Imus, Cavite', '50% Discount', '2018-05-19', 6, NULL),
(45, 3, 'Briana', '5940000', 'The name Briana stands for STRENGTH, embodying Filipino families who continue to have stronger family ties as they spend more and more time together. The Briana house model is a single attached home with a floor area of 110 sqm on a lot area of 100 sqm.', 'Imus, Cavite', '40% Discount', '2018-05-19', 6, NULL),
(46, 9, 'Ashton', '1542900', 'Sterling manors is located in Imus Cavite along the highway.\r\nthe architecture of Sterling Manors brings out the classic English style and elegance. it is close proximity to the major shoppung centers, restaurant, schools,hospitals and government centers.', 'Imus, Cavite', 'Promos None', '2018-05-19', 5, NULL),
(47, 9, 'Burton', '1295000', 'Sterling Manors features of the Grand Life and style. The archetecture of Sterling Manors brings out the classic english style and elegance the attention to details on the Grand Residences, the splended Clubhouse, the imposing entrance gate, the verdant parks and gardens. All boast of the Grand Life', 'Imus, Cavite', 'No Promo', '2018-05-19', 5, NULL),
(48, 4, 'Charlotte', '3899000', 'Charlotte Single, Bellefort Estates features a Linear Park – Family Courtyard at every backyard. It is specifically designed to make Family and Neighborly ties stronger and for residents to ENJOY the traditional comforts of Home. Vast Spaces, Good Ventilation, Abundant Natural Light, and Safe Playgr', 'Bacoor, Cavite', 'Promos None', '2018-05-19', 6, NULL),
(49, 4, 'Sabine', '5231000', 'Sabine Single, Bellefort Estates features a Linear Park – Family Courtyard at every backyard. It is specifically designed to make Family and Neighborly ties stronger and for residents to ENJOY the traditional comforts of Home. Vast Spaces, Good Ventilation, Abundant Natural Light, and Safe Playgroun', 'Bacoor, Cavite', 'Promos None', '2018-05-19', 6, NULL),
(50, 5, 'Linden', '1674000', 'Linden at Carmona Estates is a single-attached house for sale in Alfarez Avenue, Barangay Lantic, Carmona Cavite that has 3 bedroom, 2 toilet and bath, 1 car-garage. Linden at Carmona Estates is just five minutes away from Walter Mart and public market. One of the best schools inside Linden at Carmo', 'Carmona, Cavite', 'No Promo', '2018-05-19', 6, NULL),
(51, 5, 'Maple', '2269080', 'Maple at Carmona Estates is a single-attached house for sale in Alfarez Avenue, Barangay Lantic, Carmona Cavite that has 3 bedroom, 2 toilet and bath, 1 car-garage. Maple at Carmona Estates is just five minutes away from Walter Mart and public market. One of the best schools inside Maple at Carmona ', 'Carmona, Cavite', 'No Promo', '2018-05-19', 6, NULL),
(52, 7, 'Belgium', '1248000', '', 'General Trias, Cavite', '20% discount', '2018-05-19', 5, NULL),
(53, 8, 'Arlyne', '1060000', 'Immerse Yourself In A Lifestyle Of No Worries And Pure Enjoyment. Hamilton Homes Is Only A Few Kilometers Away From Major Landmarks And Establishments Of Imus. ', 'Bucandala, Imus, Cavite', 'No Promo', '2018-05-19', 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dh_images`
--

CREATE TABLE `dh_images` (
  `dh_image_id` int(11) NOT NULL,
  `dh_house_project_id` int(11) DEFAULT NULL,
  `dh_news_id` int(11) DEFAULT NULL,
  `isHouseSample` int(11) DEFAULT '0',
  `isFloorPlan` int(11) DEFAULT '0',
  `isAmenities` int(11) DEFAULT '0',
  `isNews` int(11) DEFAULT '0',
  `dh_image_path` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_images`
--

INSERT INTO `dh_images` (`dh_image_id`, `dh_house_project_id`, `dh_news_id`, `isHouseSample`, `isFloorPlan`, `isAmenities`, `isNews`, `dh_image_path`) VALUES
(13, 19, NULL, 1, 0, 0, 0, 'uploads/anastacia.jpg'),
(14, 19, NULL, 0, 1, 0, 0, 'uploads/anasfp.jpg'),
(16, NULL, 2, 0, 0, 0, 1, 'uploads/buy-a-home.jpg'),
(20, 20, NULL, 1, 0, 0, 0, 'uploads/audrey.jpg'),
(21, 20, NULL, 0, 1, 0, 0, 'uploads/audreyfp.jpg'),
(38, NULL, 3, 0, 0, 0, 1, 'uploads/out.jpg'),
(39, NULL, 4, 0, 0, 0, 1, 'uploads/home-energy.jpg'),
(40, NULL, 5, 0, 0, 0, 1, 'uploads/funrun.jpg'),
(41, NULL, 1, 0, 0, 0, 1, 'uploads/Camella_The_Redefinition_of_Best.jpg'),
(81, NULL, 6, 0, 0, 0, 1, 'uploads/next.jpg'),
(82, NULL, 7, 0, 0, 0, 1, 'uploads/launch.jpg'),
(83, NULL, 8, 0, 0, 0, 1, 'uploads/somer.jpg'),
(84, 40, NULL, 1, 0, 0, 0, 'uploads/fiona.jpg'),
(85, 40, NULL, 0, 1, 0, 0, 'uploads/FionaHouseILGiardinoFloorPlan1.jpg'),
(86, 40, NULL, 0, 1, 0, 0, 'uploads/FionaILGiardinoFloorPlan.jpg'),
(87, 41, NULL, 1, 0, 0, 0, 'uploads/alessia.jpg'),
(88, 41, NULL, 0, 1, 0, 0, 'uploads/alessiafp.jpg'),
(89, 41, NULL, 0, 1, 0, 0, 'uploads/alessiafp1.jpg'),
(90, 42, NULL, 1, 0, 0, 0, 'uploads/reva.jpg'),
(91, 42, NULL, 0, 1, 0, 0, 'uploads/revafp.jpg'),
(92, 42, NULL, 0, 1, 0, 0, 'uploads/revafp1.jpg'),
(93, 43, NULL, 1, 0, 0, 0, 'uploads/mika.jpg'),
(94, 43, NULL, 0, 1, 0, 0, 'uploads/mikafp.JPG'),
(95, 43, NULL, 0, 1, 0, 0, 'uploads/mikafp1.JPG'),
(96, 44, NULL, 1, 0, 0, 0, 'uploads/alexandra.jpg'),
(97, 44, NULL, 0, 1, 0, 0, 'uploads/alexandrafp.jpg'),
(98, 45, NULL, 1, 0, 0, 0, 'uploads/briana.jpg'),
(99, 45, NULL, 0, 1, 0, 0, 'uploads/brianafp.jpg'),
(100, 46, NULL, 1, 0, 0, 0, 'uploads/ashton.jpg'),
(101, 46, NULL, 0, 1, 0, 0, 'uploads/ashtonfp.jpg'),
(102, 47, NULL, 1, 0, 0, 0, 'uploads/burton.jpg'),
(103, 47, NULL, 0, 1, 0, 0, 'uploads/burtonfp.jpg'),
(104, 47, NULL, 0, 1, 0, 0, 'uploads/burtonfp1.jpg'),
(105, 48, NULL, 1, 0, 0, 0, 'uploads/charlotte.jpg'),
(106, 48, NULL, 0, 1, 0, 0, 'uploads/charlottefp.jpg'),
(107, 48, NULL, 0, 1, 0, 0, 'uploads/charlottefp1.jpg'),
(108, 49, NULL, 1, 0, 0, 0, 'uploads/sabine.jpg'),
(109, 49, NULL, 0, 1, 0, 0, 'uploads/sabinefp.jpg'),
(110, 49, NULL, 0, 1, 0, 0, 'uploads/sabinefp1.JPG'),
(111, 50, NULL, 1, 0, 0, 0, 'uploads/linden.jpg'),
(112, 50, NULL, 0, 1, 0, 0, 'uploads/lindenfp.jpg'),
(113, 50, NULL, 0, 1, 0, 0, 'uploads/lindenfp1.JPG'),
(114, 51, NULL, 1, 0, 0, 0, 'uploads/maple.jpg'),
(115, 51, NULL, 0, 1, 0, 0, 'uploads/maplefp.jpg'),
(116, 51, NULL, 0, 1, 0, 0, 'uploads/maplefp1.JPG'),
(117, 52, NULL, 1, 0, 0, 0, 'uploads/belgium.JPG'),
(118, 52, NULL, 0, 1, 0, 0, 'uploads/belgiumfp.jpg'),
(119, 53, NULL, 1, 0, 0, 0, 'uploads/arlyne.jpg'),
(120, 53, NULL, 0, 1, 0, 0, 'uploads/arlynefp.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dh_news`
--

CREATE TABLE `dh_news` (
  `dh_news_id` int(11) NOT NULL,
  `dh_title` varchar(200) DEFAULT NULL,
  `dh_content` varchar(1000) DEFAULT NULL,
  `dh_date_created` date NOT NULL,
  `dh_news_status` int(11) DEFAULT '0',
  `dh_encoded_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_news`
--

INSERT INTO `dh_news` (`dh_news_id`, `dh_title`, `dh_content`, `dh_date_created`, `dh_news_status`, `dh_encoded_by`) VALUES
(1, 'The Redefinition of “Best”: Camella constantly evolving to provide the best for its buyers', 'For the past 4 decades, Camella has always prided itself for the painstaking process it undergoes to find excellent locations for land development. In order to continue building the best communities over time, Camella has grown more and more selective of the land it targets to develop. With each prospect area undergoing a more rigid land selection process via the planning teams and research agencies it engages, Camella has further emphasized the significance of expertly-selected areas most feasible for residential development.', '2018-03-07', 0, 6),
(2, 'Top Questions to Ask Yourself Before You Buy a Home', 'Can You Comfortably Afford a Mortgage?\r\nTake a look at your monthly budget for your current expenses and add your potential monthly mortgage payment. Can you honestly say that you could afford it? Lenders suggest that your expenses must only be around 28% of your gross income every month. So for instance, if you earn P 65,000 monthly, your allotted monthly mortgage payment shouldn’t exceed P 18,200.', '2018-04-13', 0, 1),
(3, 'Exodus: More People are Moving Out of Metro Manila', 'Life in Metro Manila is never boring – but it can definitely be a hassle. As the country’s center for commerce, leisure, services, and entertainment, there’s always something to go to, to do, and to see, in the nation’s capital. Top it off with being the main seat of government and it’s easy to see why many choose to live here.\r\n\r\nIn the recent years, however, many families are moving out of Metro Manila, as the urban area becomes even denser and even more crowded.\r\n\r\nJust drive through EDSA and you’ll notice the advertisements on billboards and buses. “Your home in Cavite” seems like a common tagline of real estate ads, with a decrease in posters promoting residential units within CBDs.', '2018-04-20', 1, 6),
(4, 'SUREFIRE WAYS TO REDUCE YOUR BILLS AT HOME', 'Perhaps, one of the biggest slice of a household budget goes to paying bills, particularly that of water and electricity. Being in a country with a tropical climate, Filipinos’ consumption of water and electricity are often affected by sudden weather changes.\r\n\r\nMany have endured the burden of utility skyrocketing bills, often because of neglect and unmindful consumption. However, there are tried-and-tested ways on how you can trim utility bills at home. You will be amazed at how these hacks and tips can save you from a monthly bill shock.', '2018-04-30', 1, 6),
(5, 'Top Family-Friendly Activities During Summer', 'Join fun runs and other health and wellness activities. Fun runs nowadays have become even more family friendly that even the kids can join. This is a good way for family members to bond and get fit at the same time. The kids can also join sport camps so they can use up their energy in a good and healthy way.\r\n\r\nSwimming. Family outings to beaches or pools are a staple part of summer in the Philippines. Who wouldn’t want to take a refreshing dip in the water to combat the warmth of summer, right?', '2018-05-01', 0, 6),
(6, '2018 Self-Check: The List To A Better You', '“The only person you should try to be better than is the person you were yesterday. -Matty Mullins\r\n\r\nIt’s a permanent goodbye for 2017! Was it a year of a “job well done” or a “better luck next time”? Nevertheless, your 2018 remains spotless!\r\n\r\nWhether the past year was a whirlwind of turmoil or life-changing events, you must have earned a couple of life lessons along the way. Ready to put your newly-acquired wisdom to good use? This new year, let’s chase after better things and strive for self-improvement!\r\n\r\nLet our newly-compiled list below guide you as you go about the process of changing yourself for the better, improved you. You might even want to include some in your long tradition of New Year’s resolution! ', '2018-05-19', 0, 6),
(7, 'Duraville launches 2008\'s biggest hit: Hamilton Homes', 'After several tedious months of careful preparation, fellow Duravellians the long wait is finally over! Let us all welcome this year’s biggest hit and the newest groundbreaker for the south---Hamilton Homes. Following the joint efforts of Senior Vice Ppresident and DRDC Executive Vice President, Duraville has again finally opened a “one-stop shop” that caters to all types of buyers everywhere. Launched last October 18, 2008, Hamilton Homes is now officially open to the public.\r\n\r\nEven before its launching we have received favorable feedbacks on the high demands of not only affordable but accessible and reachable housing from our South Sales Network Members. For sure this project will be another successful milestone for Duraville.', '2018-05-19', 0, 6),
(8, 'Somerset Place\'s groundbreaking ceremony: a hit!', 'Another big event was held at the Somerset Place: the project\'s Groundbreaking Ceremony last July 1. More than 200 guests, brokers and agents witnessed the ceremony.\r\n\r\nThe activities started with a Holy Mass, followed by the groundbreaking ceremony graced by celebrity guests Charrie Pineda, Maui David and Melissa Ricks, all members of the ABS-CBN star magic.\r\n\r\nA short program hosted by Mr. Jigger Cunanan, head of sales operations and management trainee Kit Gallardo followed where the celebrities entertained the attendees with song and dance numbers. Not to be outdone, DMI staff Apple Sambile and Helene Querimit showed their talents as well.', '2018-05-19', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `dh_seminar_calendar`
--

CREATE TABLE `dh_seminar_calendar` (
  `dh_seminar_id` int(11) NOT NULL,
  `dh_seminar_name` varchar(250) DEFAULT NULL,
  `dh_seminar_date` date NOT NULL,
  `dh_seminar_time_start` time DEFAULT NULL,
  `dh_seminar_time_end` time DEFAULT NULL,
  `dh_seminar_location` varchar(45) DEFAULT NULL,
  `dh_date_created` date NOT NULL,
  `dh_user_id` varchar(45) NOT NULL,
  `dh_seminar_status` varchar(45) DEFAULT 'For Approval'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dh_seminar_calendar`
--

INSERT INTO `dh_seminar_calendar` (`dh_seminar_id`, `dh_seminar_name`, `dh_seminar_date`, `dh_seminar_time_start`, `dh_seminar_time_end`, `dh_seminar_location`, `dh_date_created`, `dh_user_id`, `dh_seminar_status`) VALUES
(1, 'Sample Seminar', '2018-03-10', '08:00:00', '05:00:00', 'Molino, Cavite', '2018-03-10', '1', 'Approve'),
(3, 'Sample Request 1', '2017-04-01', '10:00:00', '05:00:00', 'Las Pinas', '2017-03-28', '3', 'Approve'),
(4, 'CCS', '2018-04-12', '00:00:10', '02:00:00', 'Imus, Cavite', '2018-04-13', '8', 'Approve'),
(5, 'Sample Seminar', '2018-04-19', '11:00:00', '03:00:00', 'Imus, Cavite', '2018-03-29', '5', 'Approve'),
(6, 'NROT', '2018-04-30', '00:00:07', '00:00:08', 'Bacoor', '2018-04-30', '8', 'Approve'),
(7, 'NROT', '2018-05-25', '00:00:07', '00:00:08', 'Bacoor', '2018-04-12', '8', 'Approve'),
(8, 'NROT', '2018-04-25', '00:00:10', '02:00:00', 'Imus, Cavite', '2018-04-16', '8', 'Approve'),
(10, 'CCS', '2018-04-25', '23:00:00', '02:00:00', 'adaptdev', '2018-04-27', '8', 'Approve'),
(14, 'adaptdev', '2018-04-28', '13:00:00', '14:00:00', 'Tia maria Bacoor', '2018-03-31', '8', 'Approve'),
(15, 'CCS', '2018-04-29', '00:00:08', '00:00:06', 'adaptdev', '2018-05-16', '8', 'Approve'),
(18, 'Adaptdev seminar', '2018-05-03', '00:00:09', '00:00:08', 'heyyuou', '2018-05-16', '8', 'For Approval'),
(21, 'School Seminar', '2018-04-29', '00:00:05', '00:00:06', 'umayoskasana', '2018-05-02', '5', 'Approve'),
(22, 'NROT', '2018-05-01', '01:00:00', '02:00:00', 'imus, cavite', '2018-05-03', '7', 'Approve'),
(23, 'School Seminar', '2018-05-05', '07:20:00', '08:00:00', 'ditolang', '2018-05-07', '5', 'Approve'),
(24, 'seminar', '2018-05-16', '00:00:08', '00:00:09', 'ccs office', '2018-05-07', '7', 'Approve'),
(25, 'Adaptdev seminar', '2018-05-16', '11:00:00', '13:00:00', 'Bacoor', '2018-05-16', '5', 'Approve'),
(32, 'seminartestz', '2018-07-04', '15:00:00', '16:00:00', 'whaatx', '2018-06-19', '7', 'For Approval'),
(33, 'lasttesting', '2018-07-11', '13:00:00', '15:00:00', 'hereat', '2018-07-11', '7', 'For Approval');

-- --------------------------------------------------------

--
-- Table structure for table `dh_subdivisions`
--

CREATE TABLE `dh_subdivisions` (
  `dh_subd_id` int(11) NOT NULL,
  `dh_subd_dev_id` varchar(45) DEFAULT NULL,
  `dh_subd_name` varchar(45) DEFAULT NULL,
  `dh_subd_location` varchar(45) DEFAULT NULL,
  `dh_subd_description` varchar(300) DEFAULT NULL,
  `dh_subd_logo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_subdivisions`
--

INSERT INTO `dh_subdivisions` (`dh_subd_id`, `dh_subd_dev_id`, `dh_subd_name`, `dh_subd_location`, `dh_subd_description`, `dh_subd_logo`) VALUES
(1, '1', 'Camella Homes', 'Bacoor, Cavite', 'Camella, the largest homebuilder in real estate industry. As builders of top quality yet affordable housing in the Philippines, Camella has an enviable reputation. With 40 years and counting, Camella has already built more than 300,000 homes across the country.', 'uploads/camella.jpg'),
(2, '2', 'Antel Grand Village', 'General Trias, Cavite', 'Your Quality lifetime home investment in a resort community.. Imagine you and your Family living within an upscale resort community ... Yes, that is the unique highlight of Antel Grand Village. You have exclusive access to a 5-star hotel-inspired resort oasis right in your very own residential neigh', 'uploads/antel.jpg'),
(3, '4', 'Lancaster New City', 'General Trias, Cavite', 'Lancaster New City Cavite is the only housing development near airport that offers affordable houses for sale in Gen. Trias City Cavite.', 'uploads/lancaster.jpg'),
(4, '4', 'Bellefort Estates', 'Molino IV, Bacoor Cavite', 'Bellefort Estates is the first (and only) subdivision in the Daang hari-Molino location with a linear park on a natural hilly terrain that offers a cooler refreshing breeze.', 'uploads/bellefort.png'),
(5, '4', 'Carmona Estates', 'Carmona, Cavite', 'Carmona Estates is a 87 hectare housing development that offers house for sale in Cavite and laguna comprising seven different villages with many phases.', 'uploads/carmona.png'),
(6, '3', 'IL Giardino Residences', 'General Trias, Cavite', 'Il Giardino Residences proudly features the old world charm of Italy. From the charming entry, to the splendid clubhouse, the reflecting swimming pool, the enchanting gardens, to the range of lovely home designs, every detail showcases Italian elegance and style.', 'uploads/giar.jpg'),
(7, '5', 'Wellington Place', 'General Trias, Cavite', 'Wellington Place creates a lifestyle that defines you and your family.', 'uploads/wellington.jpg'),
(8, '5', 'Hamilton Homes', 'Imus, Cavite', 'Immerse yourself in a lifestyle of no worries and pure enjoyment. Hamilton Homes is only a few kilometers away from major landmarks and establishments of Imus.', 'uploads/hamilton.jpg'),
(9, '3', 'Sterling Manors', 'Imus, Cavite', 'Sterling Manors features of the grand life and style.The architecture of Sterling Manors brings out the classic English style and elegance. The attention to details on the grand residences, the splendid clubhouse, the imposing entrance gate, the verdant parks and gardens, all boast of the grand life', 'uploads/sterling.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dh_users`
--

CREATE TABLE `dh_users` (
  `dh_user_id` int(11) NOT NULL,
  `dh_user_group_id` int(11) DEFAULT NULL,
  `dh_user_details_id` int(11) DEFAULT NULL,
  `dh_user_name` varchar(200) DEFAULT NULL,
  `dh_user_pass` varchar(200) DEFAULT NULL,
  `dh_date_created` date NOT NULL,
  `dh_status` varchar(45) DEFAULT 'Active',
  `is_deleted` int(11) DEFAULT '0',
  `login_counter` int(11) DEFAULT '0',
  `dh_account_create` varchar(25) DEFAULT 'Registration'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_users`
--

INSERT INTO `dh_users` (`dh_user_id`, `dh_user_group_id`, `dh_user_details_id`, `dh_user_name`, `dh_user_pass`, `dh_date_created`, `dh_status`, `is_deleted`, `login_counter`, `dh_account_create`) VALUES
(1, 1, 1, 'renzanima', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Registration'),
(2, 2, 2, 'manageruser', 'TWFuYWdlcnVzZXIx', '2018-04-30', 'Active', 0, 0, 'Encode'),
(4, 4, 4, 'salesagent', 'U2FsZXNhZ2VudDE=', '2018-04-27', 'Active', 0, 0, 'Encode'),
(6, 5, 6, 'melvinjay', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Encode'),
(7, 2, 7, 'dekllanes', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Encode'),
(8, 3, 8, 'Chinochins', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Encode'),
(9, 3, 9, 'neciagrant', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Encode'),
(10, 4, 10, 'Agbayanis', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Registration'),
(11, 4, 11, 'Karlgonzales', 'RHJlYW1ob3VzZTE=', '2018-01-21', 'Active', 0, 0, 'Registration'),
(13, 3, 13, 'LinaInverser', 'TGluYUludmVyc2UxMg==', '2018-02-14', 'Declined', 0, 0, 'Encode'),
(14, 4, 14, 'renzrenz', 'UmVuenJlbnox', '2018-04-01', 'Active', 0, 0, 'Registration'),
(16, 2, 16, 'exampleuser', 'RXhhbXBsZTEyMTM=', '2018-04-10', 'Active', 0, 0, 'Encode'),
(18, 3, 18, 'anamarie', 'QW5hbWFyaWUx', '2018-04-28', 'Active', 0, 0, 'Encode'),
(19, 3, 19, 'testingsample', 'RXhhbXBsZTE=', '2018-04-30', 'For Activation', 0, 0, 'Encode'),
(21, 2, 21, 'anothermanager', 'TWFuYWdlcnVzZXIx', '2018-05-01', 'Active', 0, 0, 'Encode'),
(22, 2, 22, 'divisiontesting', 'VGVzdGluZzE=', '2018-05-03', 'Active', 0, 0, 'Encode'),
(23, 3, 23, 'directortesting', 'RHJlYW1ob3VzZTE=', '2018-05-03', 'Active', 0, 0, 'Encode'),
(123, 2, 123, 'AdmiralKunkka', '', '2018-01-26', 'Active', 0, 0, 'Encode'),
(123123, 4, 123123, 'try12345678', 'UmFuZGVsbGUxMg==', '2018-05-16', 'Active', 0, 0, 'Registration'),
(1234652, 4, 1234652, 'kenjinuesca', 'RHJlYW1ob3VzZTE=', '2018-05-19', 'Active', 0, 0, 'Encode'),
(9521243, 3, 9521243, 'salesdirector', 'U2FsZXNkaXJlY3RvcjE=', '2018-04-10', 'Active', 0, 0, 'Encode'),
(9564370, 3, 9564370, 'directoruser', 'RGlyZWN0b3J1c2VyMQ==', '2018-04-24', 'Active', 0, 0, 'Encode'),
(11725436, 4, 11725436, 'zacchhuser', 'RHJlYW1ob3VzZTE=', '2018-05-19', 'Active', 0, 0, 'Encode');

-- --------------------------------------------------------

--
-- Table structure for table `dh_user_details`
--

CREATE TABLE `dh_user_details` (
  `dh_user_details_id` int(11) NOT NULL,
  `dh_division_id` int(11) NOT NULL,
  `dh_firstName` varchar(150) DEFAULT NULL,
  `dh_middleName` varchar(45) DEFAULT NULL,
  `dh_lastName` varchar(150) DEFAULT NULL,
  `dh_user_nickname` varchar(100) DEFAULT NULL,
  `dh_bday` date NOT NULL,
  `dh_age` int(11) DEFAULT NULL,
  `dh_gender` varchar(10) DEFAULT NULL,
  `dh_tin_number` int(9) DEFAULT NULL,
  `dh_email_address` varchar(45) DEFAULT NULL,
  `dh_contact_no` varchar(100) DEFAULT NULL,
  `dh_home_address` varchar(200) DEFAULT NULL,
  `dh_occupation` varchar(100) DEFAULT NULL,
  `dh_seminar_date` varchar(150) DEFAULT NULL,
  `dh_seminar_venue` varchar(150) DEFAULT NULL,
  `dh_recruited_by` varchar(150) DEFAULT NULL,
  `dh_recruited_by_position` varchar(150) DEFAULT NULL,
  `dh_recruited_by_division` varchar(150) DEFAULT NULL,
  `dh_trainor_name` varchar(150) DEFAULT NULL,
  `dh_sales_director` varchar(150) DEFAULT NULL,
  `dh_division_manager` varchar(150) DEFAULT NULL,
  `dh_executive_broker` varchar(150) DEFAULT 'Rodelio D. Parafina',
  `dh_realty_name` varchar(45) DEFAULT NULL,
  `dh_realty_pos` varchar(45) DEFAULT NULL,
  `dh_realty_from` varchar(45) DEFAULT NULL,
  `dh_realty_to` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_user_details`
--

INSERT INTO `dh_user_details` (`dh_user_details_id`, `dh_division_id`, `dh_firstName`, `dh_middleName`, `dh_lastName`, `dh_user_nickname`, `dh_bday`, `dh_age`, `dh_gender`, `dh_tin_number`, `dh_email_address`, `dh_contact_no`, `dh_home_address`, `dh_occupation`, `dh_seminar_date`, `dh_seminar_venue`, `dh_recruited_by`, `dh_recruited_by_position`, `dh_recruited_by_division`, `dh_trainor_name`, `dh_sales_director`, `dh_division_manager`, `dh_executive_broker`, `dh_realty_name`, `dh_realty_pos`, `dh_realty_from`, `dh_realty_to`) VALUES
(1, 0, 'Renz', 'Valiente', 'Anima', '', '1997-12-08', 20, 'Male', 0, 'animarenz08@gmail.com', '09538312034', 'Imus, Cavite', '', '01-19-2018', 'Gen Trias, Cavite', '', '', '', '', '', '', 'Rodelio D. Parafina', '', '', '', ''),
(2, 9, 'Ivan', 'Rose', 'Montenegre', '', '1997-03-26', 20, 'Female', 0, 'divisionmanager@example.com', '09090909091', 'Kawit, Cavite', 'It Support', '2018-04-05', 'Dasma, Cavite', '1', '', '', 'division', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(4, 9, 'Ashley', 'Delos Reyes', 'Morris', '', '1982-04-18', 41, 'Female', 0, 'salesagent@example.com', '09224949999', 'Cavite City', '', '2018-04-07', 'Bucanda, Cavite', '3', '3', '9', 'salestrainee', NULL, '2', 'Rodelio D. Parafina', '', '', '', ''),
(6, 5, 'Melvin jay', 'Demente', 'Erlano', '', '1996-04-20', 20, 'Male', 0, 'erlanomelvin@gmail.com', '09268317604', 'Addas, Bacoor, Cavite', '', '', 'Gen Trias, Cavite', '', '', '', '', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(7, 6, 'Reuben', 'Lapuz', 'Llanes', '', '1995-11-01', 20, 'Male', 0, 'bimbyllanes@gmail.com', '09768311977', 'Zapote, Cavite', '', '', 'Bacoor, Cavite', '1', '', '', '', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(8, 6, 'Chino', 'Paso', 'Apacible', 'nich', '1999-02-03', 22, 'Male', 0, 'chinopapac@yahoo.com', '09750123959', 'Paliparan, Cavite', '', '01-18-2018', 'Gen Trias, Cavite', '7', '2', '5', '', NULL, '6', 'Rodelio D. Parafina', '', '', '', ''),
(9, 6, 'Grant', 'Ranada', 'Necia', '', '1994-01-01', 25, 'Male', 0, 'grantneciagrant@gmail.com', '09994172155', 'Summer Pointe, Molino, Cavite', '', '', 'Kawit, Cavite', '7', '2', '6', '', NULL, '7', 'Rodelio D. Parafina', '', '', '', ''),
(10, 6, 'Rustine', '', 'Agbayani', '', '1995-04-20', 23, 'Male', 0, 'louiseagbayani@ymail.com', '09278102034', 'Dasmarinas, Cavite', '', '2017-11-11', 'Kawit, Cavite', '9', '3', '6', '', '9', '7', 'Rodelio D. Parafina', '', '', '', ''),
(11, 5, 'Karl', 'Alawi', 'Gonzales', '', '1996-09-26', 21, 'Male', 0, 'Guitarpick@gmail.com', '09095012346', 'Paliparan, Cavite', '', '2017-09-22', 'Gen Trias, Cavite', '8', '3', '5', '', '8', '6', 'Rodelio D. Parafina', '', '', '', ''),
(13, 7, 'Lina', '', 'Price', 'Lin', '1994-07-31', 23, 'Female', 123456789, 'Lina@dota2.com', '09999999999', 'Molino, Cavite', '', '2017-10-12', 'Robinson Imus', '12', '2', '7', '', NULL, '12', 'Rodelio D. Parafina', '', '', '', ''),
(14, 3, 'Draven', 'Rivera', 'Brown', '', '1997-12-12', 20, 'Male', 0, 'renzanima@example.com', '09264922381', 'Silang, Cavite', '', '03-29-2018', 'Imus, Cavite', '7', '2', '6', 'Kyrie Jordan', '', '7', 'Rodelio D. Parafina', '', '', '', ''),
(16, 8, 'Joshua', 'Lontok', 'Santos', '', '1999-04-15', 22, 'Female', 0, 'email@example.com', '09224949411', 'Carmona, Cavite', '', '2018-04-29', 'Adaptdev Bacoor', '1', NULL, NULL, 'facilitatot', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(18, 6, 'Ana Marie', '', 'Obon', '', '1989-09-09', 40, 'Female', 0, 'anamarie@example.com', '09090909090', 'Tagaytay, Cavite', '', '2017-12-12', 'ccs office', '7', '2', '6', 'melvin', NULL, '7', 'Rodelio D. Parafina', '', '', '', ''),
(19, 6, 'Russell', 'Lontoc', 'Soriano', '', '1967-10-10', 43, 'Female', 0, 'russell@rocketmail.com', '09090909090', 'blk 1 lot 1 Imus, Cavite', 'web developer', '2018-04-29', 'Bacoor, Cavite', '18', '3', '6', 'zvxcvxc', NULL, '7', 'Rodelio D. Parafina', 'fdsfdsfdsfsdf', 'sdfsdfsdfdf', '2010', '2013'),
(21, 1, 'Marc', 'Llanos', 'Perez', '', '1991-01-02', 25, 'Male', 0, 'manager@example.com', '09090909090', 'General Trias, Cavite', '', '2018-04-27', 'Gen. Trias, Cavite', '1', '', '', 'anothertwo', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(22, 2, 'Dict', '', 'Gayanilo', '', '1992-04-20', 24, 'Female', 0, 'gaya1232@rocketmail.com', '09090909090', 'Bacoor, Cavite', '', '2018-04-27', 'Gen. Trias, Cavite', '1', '', '', 'wewes', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(23, 1, 'Rayleo', 'Reyes', 'Roca', '', '1990-03-30', 25, 'Male', 0, 'renzamina@yahoo.com', '09224949411', 'Pasay City', '', '', 'Imus, Cavite', '21', '2', '1', 'testinglang', ' ', '21', 'Rodelio D. Parafina', '', '', '', ''),
(123, 7, 'Aldrin', '', 'Moaje', 'maje', '1987-07-14', 27, 'Male', 92323234, 'kunkha@rocketmail.com', '09999999999', 'Silang, Cavite', '', '', 'Bacoor, Cavite', '1', NULL, NULL, '', NULL, NULL, 'Rodelio D. Parafina', '', '', '', ''),
(123123, 7, 'Girlie', 'Brandon', 'Agulto', 'lie', '1978-05-03', 35, 'Male', 121212121, 'girlielie@yahoo.com', '09090909090', 'Las Pinas City', '', '', 'Imus, Cavite', '123', '2', '7', '', '8', '7', 'Rodelio D. Parafina', '', '', '', ''),
(1234652, 8, 'Kenji', '', 'Nuesca', '', '1995-07-14', 25, 'Male', 125436356, 'erlano.melvin@gmail.com', '09224949411', 'Zapote, Bacoor', '', '', 'CCS office', '16', '2', '8', 'Draven Dea vega', ' ', '16', 'Rodelio D. Parafina', '', '', '', ''),
(9521243, 5, 'Jb Rhoze', 'wood', 'Olaes', 'jb', '1962-04-05', 42, 'Female', 0, 'olaes@gmail.com', '09090909090', 'Zapote, Cavite', '', '', 'Gen. Trias, Cavite', '8', '3', '5', '', NULL, '7', 'Rodelio D. Parafina', '', '', '', ''),
(9564370, 9, 'Arthuro', 'bennett', 'Baculto', 'theD', '1995-05-01', 22, 'Male', 0, 'arthuro@yahoo.com', '09090909090', 'Sta Cruz, Manila', '', '2018-05-02', 'Gen. Trias, Cavite', '2', '2', '9', 'salesdirector', NULL, '2', 'Rodelio D. Parafina', '', '', '', ''),
(11725436, 1, 'Zach', 'Sotto', 'Yamio', '', '1997-02-14', 20, 'Male', 5123533, 'zach@gmail.com', '09224949999', 'blk 2 lot 10 ph 7 hugo street, Imus, Cavite', '', '2018-05-20', 'Perpetual', '23', '3', '1', 'Melvin jay Erlano', '23', '21', 'Rodelio D. Parafina', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dh_user_education`
--

CREATE TABLE `dh_user_education` (
  `dh_user_educ_id` int(11) NOT NULL,
  `dh_user_id` int(11) DEFAULT NULL,
  `dh_school_name` varchar(150) DEFAULT NULL,
  `dh_school_location` varchar(150) DEFAULT NULL,
  `dh_attainment` varchar(150) DEFAULT NULL,
  `dh_year_from` varchar(25) DEFAULT NULL,
  `dh_year_to` varchar(25) DEFAULT NULL,
  `is_vocational` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_user_education`
--

INSERT INTO `dh_user_education` (`dh_user_educ_id`, `dh_user_id`, `dh_school_name`, `dh_school_location`, `dh_attainment`, `dh_year_from`, `dh_year_to`, `is_vocational`) VALUES
(17, 1, 'perpetual molino', 'bacoor, cavite', 'BSIT', '2014', '2018', 0),
(18, 1, 'St Thomas More Academy', 'testign', 'Graduate', '2010', '2014', 0),
(19, 1, '', '', '', '', '', 0),
(20, 1, '', '', '', '', '', 0),
(21, 6, 'perpetual molino', 'molino, bacoor, cavite', 'BSIT', '2014', '2018', 0),
(22, 6, '', '', '', '', '', 0),
(23, 6, '', '', '', '', '', 0),
(24, 6, '', '', '', '', '', 0),
(25, 7, 'CVSU Imus', 'Imus, Cavite', 'BS Marine', '2012', '2017', 0),
(26, 7, '', '', '', '', '', 0),
(27, 7, '', '', '', '', '', 0),
(28, 7, '', '', '', '', '', 0),
(29, 8, 'DLSU Dasmarinas', 'Dasmarinas, Cavite', 'BS Engineer', '2013', '2019', 0),
(30, 8, 'Bacoor National Highschool', 'Bacoor, Cavite', 'Undergarduate', '2016', '2018', 0),
(31, 8, '', '', '', '', '', 0),
(32, 8, '', '', '', '', '', 0),
(33, 9, 'Adamson University', 'Carmona, Cavite', 'BSBA', '2011', '2016', 0),
(34, 9, '', '', '', '', '', 0),
(35, 9, '', '', '', '', '', 0),
(36, 9, '', '', '', '', '', 0),
(37, 10, 'FEU Cavite', 'Silang, Cavite', 'BSIT', '2014', '2019', 0),
(38, 10, '', '', '', '', '', 0),
(39, 10, '', '', '', '', '', 0),
(40, 10, '', '', '', '', '', 0),
(41, 11, 'LPU Cavite', 'Gen Trias, Cavite', 'BS Architecture', '2012', '2015', 0),
(42, 11, '', '', '', '', '', 0),
(43, 11, '', '', '', '', '', 0),
(44, 11, '', '', '', '', '', 0),
(45, 123, 'educsscs', '', '', '', '', 0),
(46, 123, 'adfds', '', '', '', '', 0),
(47, 123, '', '', '', '', '', 0),
(48, 123, '', '', '', '', '', 0),
(49, 13, '', '', '', '', '', 0),
(50, 13, '', '', '', '', '', 0),
(51, 13, '', '', '', '', '', 0),
(52, 13, '', '', '', '', '', 0),
(53, 14, 'perps molino', 'bacoor, cavite', 'BSIT', '2017', '2018', 0),
(54, 14, 'GEANHS ANNEX', 'Imus, Cavite', 'Graduate', '', '', 0),
(55, 14, '', '', '', '', '', 0),
(56, 14, '', '', '', '', '', 0),
(61, 16, 'colllge', 'locaitons', 'BSAttain', '2011', '2014', 0),
(62, 16, 'highschool', 'hsdon', 'Graduate', '2009', '2011', 0),
(63, 16, 'elementary', 'bahay', 'Undergraduate', '2005', '2009', 0),
(64, 16, '', '', '', '', '', 0),
(65, 9521243, 'new college', 'sales', 'college graduate', '2015', '2018', 0),
(66, 9521243, 'new sales hs', 'highschool', 'undergraduate', '2012', '2015', 0),
(67, 9521243, '', '', '', '', '', 0),
(68, 9521243, '', '', '', '', '', 0),
(69, 18, '', '', '', '', '', 0),
(70, 18, '', '', '', '', '', 0),
(71, 18, '', '', '', '', '', 0),
(72, 18, '', '', '', '', '', 0),
(73, 19, 'National University', 'Manila', 'Undergraduate', '2011', '2014', 0),
(74, 19, 'Holy National highschool', 'Cavite', 'graduate', '2005', '2011', 0),
(75, 19, 'bacoor elementary school', 'Bacoor, Cavite', 'graduate', '2000', '2005', 0),
(76, 19, 'Tesda', 'Bacoor, Cavite', '', '2014', '2018', 0),
(81, 9564370, 'Geanhs Annex', 'Bacoor, Cavite', 'Graduate', '2010', '2014', 0),
(82, 9564370, 'Pasong Buaya II Elementary School', 'Imus, Cavite', 'Graduate', '2004', '2010', 0),
(83, 9564370, '', '', '', '', '', 0),
(84, 9564370, '', '', '', '', '', 0),
(85, 4, '', '', '', '', '', 0),
(86, 4, '', '', '', '', '', 0),
(87, 4, '', '', '', '', '', 0),
(88, 4, '', '', '', '', '', 0),
(89, 2, '', '', '', '', '', 0),
(90, 2, '', '', '', '', '', 0),
(91, 2, '', '', '', '', '', 0),
(92, 2, '', '', '', '', '', 0),
(93, 21, '', '', '', '', '', 0),
(94, 21, '', '', '', '', '', 0),
(95, 21, '', '', '', '', '', 0),
(96, 21, '', '', '', '', '', 0),
(97, 22, '', '', '', '', '', 0),
(98, 22, '', '', '', '', '', 0),
(99, 22, '', '', '', '', '', 0),
(100, 22, '', '', '', '', '', 0),
(101, 23, '', '', '', '', '', 0),
(102, 23, '', '', '', '', '', 0),
(103, 23, '', '', '', '', '', 0),
(104, 23, '', '', '', '', '', 0),
(117, 123123, 'Perps LP', 'Las Pinas City', 'Undergraduate', '', '', 0),
(118, 123123, '', '', '', '', '', 0),
(119, 123123, '', '', '', '', '', 0),
(120, 123123, '', '', '', '', '', 0),
(121, 11725436, 'San Sebastian College', 'Cavite City', 'BSIT', '2010', '2014', 0),
(122, 11725436, 'Kawit National Highschool', 'Kawit, Cavite', 'Graduate', '2006', '2010', 0),
(123, 11725436, 'Buhay na tubig elementary school', 'imus, Cavite', 'Graduate', '2000', '2006', 0),
(124, 11725436, '', '', '', '', '', 0),
(125, 1234652, '', '', '', '', '', 0),
(126, 1234652, '', '', '', '', '', 0),
(127, 1234652, '', '', '', '', '', 0),
(128, 1234652, 'Cvsu Imus', 'Imus, Cavite', 'Technician', '2014', '2016', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dh_user_groups`
--

CREATE TABLE `dh_user_groups` (
  `dh_user_group_id` int(11) NOT NULL,
  `dh_user_group_name` varchar(45) DEFAULT NULL,
  `dh_user_access_lvl` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_user_groups`
--

INSERT INTO `dh_user_groups` (`dh_user_group_id`, `dh_user_group_name`, `dh_user_access_lvl`) VALUES
(1, 'Admin', '9999'),
(2, 'Division Manager', '5000'),
(3, 'Sales Director', '4000'),
(4, 'Sales Agent', '3000'),
(5, 'Employee', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `dh_vehicletrip_calendar`
--

CREATE TABLE `dh_vehicletrip_calendar` (
  `dh_vehicle_trip_id` int(11) NOT NULL,
  `dh_trip_date` date NOT NULL,
  `dh_location_pickup` varchar(45) DEFAULT NULL,
  `dh_location_pickup_time` varchar(45) DEFAULT NULL,
  `dh_location_destination` varchar(45) DEFAULT NULL,
  `dh_location_destination_time` varchar(45) DEFAULT NULL,
  `dh_driver_name` varchar(45) DEFAULT NULL,
  `dh_plateno` varchar(45) DEFAULT NULL,
  `dh_driver_contact` varchar(45) DEFAULT NULL,
  `dh_user_id` int(11) NOT NULL,
  `dh_date_created` date NOT NULL,
  `dh_trip_status` varchar(45) DEFAULT 'For Approval'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dh_vehicletrip_calendar`
--

INSERT INTO `dh_vehicletrip_calendar` (`dh_vehicle_trip_id`, `dh_trip_date`, `dh_location_pickup`, `dh_location_pickup_time`, `dh_location_destination`, `dh_location_destination_time`, `dh_driver_name`, `dh_plateno`, `dh_driver_contact`, `dh_user_id`, `dh_date_created`, `dh_trip_status`) VALUES
(3, '2018-04-20', 'SM Center Molino', '8:00 AM ~ 8:30 AM', 'Ponticelli Gardens', '9:30 AM ~ 10:00 AM', 'Kevin Hufina', 'ZXC 432', '09361234567', 1, '2018-05-19', 'Approve'),
(4, '2018-04-10', 'SM Southmall', '8:30AM ~ 9:00AM', 'Daang Hari Evia', '11:00AM', 'Marlon Vasquez', 'abc 3214', '09123456789', 3, '2018-03-20', 'Approve'),
(5, '2018-04-05', 'SM Molino', '9:00AM ~ 9:20AM', 'Evia', '10:00AM - 10:10AM', 'Peter Gabriel', 'abc123', '09090909091', 8, '2018-03-22', 'Approve'),
(6, '2018-04-08', 'SM Molino', '11:00 AM ~ 11:30 AM', 'SM Pala-Pala', '11:50 AM', 'Aljon Villanueva', 'asd153', '09224949411', 5, '2018-03-23', 'Approve'),
(7, '2018-04-28', 'SM Molino', '11:00 AM ~ 11:30 AM', 'SM Pala-Pala', '11:50 AM', 'Peter Gabriel', 'asd153', '09224949411', 8, '2018-04-03', 'Approve'),
(8, '2017-11-10', 'imus', '7:00AM ~ 6:00 PM', 'Bacoor', '6:00PM ~ 7:00 AM', 'Peter Gabriel', 'asd153', '09361234567', 8, '2017-11-12', 'Declined'),
(9, '2017-12-12', 'Gen Trias, Cavite', '7:00AM ~ 8:00 AM', 'Bucandala, Imus, Cavite', '9:00AM - 9:20AM', 'Peter Gabriel', 'fdsf45', '534342432', 8, '2017-12-16', 'Approve'),
(10, '2018-04-27', 'Imus', '6:00PM - 6:30PM', 'Bacoor', '7:00PM - 7:10PM', 'Aljon Villanueva', 'asd153', '09909909', 8, '2018-05-24', 'For Approval'),
(11, '2018-04-29', 'Bacoor', '6:00PM - 6:30PM', 'Kawit', '5:00 - 6:00PM', 'Charles Backus', 'poi321', '94930053911', 5, '2018-05-30', 'Approve'),
(12, '2018-05-01', 'imus', '5:00PM - 5:30PM', 'perpetual', '6:00PM ~ 7:00 PM', 'melvin', 'abc123', '09090909090', 18, '2018-05-01', 'Approve'),
(13, '2018-05-05', 'Molino', '9:00AM - 9:20AM', 'Gen Trias, Cavite', '10:00AM - 10:30AM', 'Peter Gabriel', 'asd153', '4324232343', 5, '2018-05-03', 'Approve'),
(14, '2018-05-11', 'Zapote', '4:00PM ~ 4:20PM', 'Sm Southmall', '6:00PM ~ 7:00 PM', 'Peter Gabriel', 'daa 512', '09361234567', 10, '2018-05-09', 'Approve'),
(15, '2018-05-27', 'cavite', '1231231', 'Perpetual Molino', '123123', 'Ralph Requinto', 'dcs534', '12312312312', 123123, '2018-05-16', 'Approve'),
(16, '2018-05-18', 'adaptdev', '11:00 AM ~ 11:30 AM', 'sm bacoor', '12:10PM ~ 12:30PM', 'Aljon Villanueva', 'abc123', '09361234567', 10, '2018-05-16', 'Approve'),
(21, '2018-07-01', 'Perpetual Molino', '2:00 AM - 2:00 AM', 'SM molino', '2:30 AM - 2:30 AM', 'heyyyy', 'aadsv1234', '09999999999', 7, '2018-07-01', 'Approve'),
(22, '2018-07-03', 'testing', '2:30 AM - 3:30 AM', 'wewes', '4:00 AM - 4:30 AM', 'Aljon Villanueva', 'ABC 123', '09361234567', 7, '2018-07-03', 'For Approval'),
(23, '2018-07-04', 'adaptdev', '8:30 AM - 9:00 AM', 'test', '9:30 AM - 10:00 AM', 'Marlon Hufina', 'ZXC 432', '09123456789', 8, '2018-07-03', 'For Approval'),
(24, '2018-07-11', 'cavite', '1:00 AM - 1:30 AM', 'manila', '2:30 AM - 3:00 AM', 'Ralph Dela Vega', 'ZXC 432', '09750123959', 10, '2018-07-03', 'For Approval');

-- --------------------------------------------------------

--
-- Table structure for table `dh_vehicle_info`
--

CREATE TABLE `dh_vehicle_info` (
  `dh_vehicle_id` int(11) NOT NULL,
  `dh_vehicle_brand` varchar(240) DEFAULT NULL,
  `dh_vehicle_plateNo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dh_vehicle_info`
--

INSERT INTO `dh_vehicle_info` (`dh_vehicle_id`, `dh_vehicle_brand`, `dh_vehicle_plateNo`) VALUES
(1, 'Hyundai L300', 'ZXC 432'),
(2, 'Toyota Hiace', 'ABC 123'),
(3, 'Nissan Urvan', 'DCS 619');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dh_closing`
--
ALTER TABLE `dh_closing`
  ADD PRIMARY KEY (`dh_closing_id`);

--
-- Indexes for table `dh_developers`
--
ALTER TABLE `dh_developers`
  ADD PRIMARY KEY (`dh_developers_id`);

--
-- Indexes for table `dh_divisions`
--
ALTER TABLE `dh_divisions`
  ADD PRIMARY KEY (`dh_division_id`);

--
-- Indexes for table `dh_driver_info`
--
ALTER TABLE `dh_driver_info`
  ADD PRIMARY KEY (`dh_driver_id`);

--
-- Indexes for table `dh_finish`
--
ALTER TABLE `dh_finish`
  ADD PRIMARY KEY (`dh_finish_id`);

--
-- Indexes for table `dh_house_project`
--
ALTER TABLE `dh_house_project`
  ADD PRIMARY KEY (`dh_house_proj_id`);

--
-- Indexes for table `dh_images`
--
ALTER TABLE `dh_images`
  ADD PRIMARY KEY (`dh_image_id`);

--
-- Indexes for table `dh_news`
--
ALTER TABLE `dh_news`
  ADD PRIMARY KEY (`dh_news_id`);

--
-- Indexes for table `dh_seminar_calendar`
--
ALTER TABLE `dh_seminar_calendar`
  ADD PRIMARY KEY (`dh_seminar_id`);

--
-- Indexes for table `dh_subdivisions`
--
ALTER TABLE `dh_subdivisions`
  ADD PRIMARY KEY (`dh_subd_id`);

--
-- Indexes for table `dh_users`
--
ALTER TABLE `dh_users`
  ADD PRIMARY KEY (`dh_user_id`);

--
-- Indexes for table `dh_user_details`
--
ALTER TABLE `dh_user_details`
  ADD PRIMARY KEY (`dh_user_details_id`);

--
-- Indexes for table `dh_user_education`
--
ALTER TABLE `dh_user_education`
  ADD PRIMARY KEY (`dh_user_educ_id`);

--
-- Indexes for table `dh_user_groups`
--
ALTER TABLE `dh_user_groups`
  ADD PRIMARY KEY (`dh_user_group_id`);

--
-- Indexes for table `dh_vehicletrip_calendar`
--
ALTER TABLE `dh_vehicletrip_calendar`
  ADD PRIMARY KEY (`dh_vehicle_trip_id`);

--
-- Indexes for table `dh_vehicle_info`
--
ALTER TABLE `dh_vehicle_info`
  ADD PRIMARY KEY (`dh_vehicle_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dh_closing`
--
ALTER TABLE `dh_closing`
  MODIFY `dh_closing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `dh_developers`
--
ALTER TABLE `dh_developers`
  MODIFY `dh_developers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dh_divisions`
--
ALTER TABLE `dh_divisions`
  MODIFY `dh_division_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `dh_driver_info`
--
ALTER TABLE `dh_driver_info`
  MODIFY `dh_driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dh_finish`
--
ALTER TABLE `dh_finish`
  MODIFY `dh_finish_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `dh_house_project`
--
ALTER TABLE `dh_house_project`
  MODIFY `dh_house_proj_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `dh_images`
--
ALTER TABLE `dh_images`
  MODIFY `dh_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `dh_news`
--
ALTER TABLE `dh_news`
  MODIFY `dh_news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dh_seminar_calendar`
--
ALTER TABLE `dh_seminar_calendar`
  MODIFY `dh_seminar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `dh_subdivisions`
--
ALTER TABLE `dh_subdivisions`
  MODIFY `dh_subd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `dh_users`
--
ALTER TABLE `dh_users`
  MODIFY `dh_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11725437;

--
-- AUTO_INCREMENT for table `dh_user_education`
--
ALTER TABLE `dh_user_education`
  MODIFY `dh_user_educ_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `dh_user_groups`
--
ALTER TABLE `dh_user_groups`
  MODIFY `dh_user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dh_vehicletrip_calendar`
--
ALTER TABLE `dh_vehicletrip_calendar`
  MODIFY `dh_vehicle_trip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `dh_vehicle_info`
--
ALTER TABLE `dh_vehicle_info`
  MODIFY `dh_vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
