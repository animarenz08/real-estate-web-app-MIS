<?php 
	require_once '../config/config.php';
	require_once 'class.database.php';

	class Property {
		private $db;
		private $dbName;
		private $default;

		public function __construct(){
			global $DB, $default_password;
			$database = new Database();
			$db = $database->dbConnection($DB);
			$dbName = $DB['db_name'];
			$this->dbName = $dbName;
			$this->conn = $db;
			$this->def_pass = $default_password;
		}


		/*
		 * Redirect to another Page
		 * @params string $url - Url to another page
		 */
		public function redirect($url){
			header("Location: $url");
		}


		public function getAllSubdivision() {
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_subdivisions as subd INNER JOIN dh_developers as dev ON dh_subd_dev_id = dh_developers_id");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function getAllDevelopers(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_developers ORDER BY dh_developers_id ASC");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllDevelopersId($devId){
			try {
				$stmt = $this->conn->prepare("SELECT dh_developers_id FROM dh_developers WHERE dh_developers_id = ? ");
				$stmt->execute(array($devId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}
		public function getAllHouseProjects(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project as uM 
						INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id WHERE isHouseSample = 1");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getHouseProjectbyMonth($month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project as uM 
						INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id WHERE isHouseSample = 1 AND MONTH(dh_date_created) = ?");
				$stmt->execute(array($month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllHouseProjects2(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_images as uMD 
						INNER JOIN  dh_house_project as uM 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllProjectsAmenities(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project as uM 
						INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id WHERE isAmenities = 1");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllHouseFinish(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_finish");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getHouseProjectsCount(){
			try {
				$stmt = $this->conn->prepare("SELECT COUNT(dh_house_proj_id) as Count FROM dh_house_project");
				$stmt->execute();
				$result = $stmt->fetchColumn();
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function HouseProjectsbySubdId($subdId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project  as uM INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id WHERE dh_subd_id = ? AND isHouseSample = 1");
				$stmt->execute(array($subdId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}
		public function HouseProjectsbyProjId($ProjId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project  as uM INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id INNER JOIN dh_subdivisions as sB 
						ON uM.dh_subd_id = sB.dh_subd_id WHERE dh_house_proj_id = ? AND isHouseSample = 1");
				$stmt->execute(array($ProjId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllProjbySubdNotProjId($subdId,$ProjId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project  as uM INNER JOIN dh_images as uMD 
						ON uM.dh_house_proj_id = uMD.dh_house_project_id INNER JOIN dh_subdivisions as sB 
						ON uM.dh_subd_id = sB.dh_subd_id WHERE uM.dh_subd_id = ? AND uM.dh_house_proj_id != ? AND isHouseSample = 1");
				$stmt->execute(array($subdId,$ProjId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}
		public function deleteHouseProject($ProjId){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("DELETE dh_house_project, dh_images FROM dh_house_project INNER JOIN dh_images ON dh_house_project.dh_house_proj_id = dh_images.dh_house_project_id WHERE dh_house_proj_id = ?");
				$stmt->execute(array($ProjId));

				$this->conn->commit();
				$_SESSION['Message'] = "House Project Successfully Deleted!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllSubdivisionbyEdit($subdId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_subdivisions WHERE dh_subd_id != ?");
				$stmt->execute(array($subdId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}
		public function HouseFinishbyProjId($ProjId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_finish WHERE dh_house_proj_id = ?");
				$stmt->execute(array($ProjId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}
	}