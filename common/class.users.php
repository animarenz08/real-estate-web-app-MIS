<?php 
	require_once '../config/config.php';
	require_once 'class.database.php';

	class User {
		private $db;
		private $dbName;
		private $default;

		public function __construct(){
			global $DB, $default_password;
			$database = new Database();
			$db = $database->dbConnection($DB);
			$dbName = $DB['db_name'];
			$this->dbName = $dbName;
			$this->conn = $db;
			$this->def_pass = $default_password;
		}


		/*
		 * Redirect to another Page
		 * @params string $url - Url to another page
		 */
		public function redirect($url){
			header("Location: $url");
		}

		/*
		 * Function redirect to Index.php if PageUserGroup is not Equal
		 * @param $userGroupId = $user_type
		 * @param $pageUserGroup = ['1,2,3,4,5']; User Levels Per Page
		 */
		public function isPageAccessible($userGroupId, $pageUserGroup) {
			if($userGroupId != $pageUserGroup){
				$this->redirect('index.php');
			}
		}

		public function updateLoginCounter($userId, $count){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_users SET login_counter = ? WHERE dh_user_id = ?");
				$stmt->execute(array($count,$userId));
				$this->conn->commit();
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get the next UserID
		 */ 
		public function getNextUserId(){
			try{
				$stmt = $this->conn->prepare("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'dh_users'");
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getNextFinishId(){
			try{
				$stmt = $this->conn->prepare("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'dh_finish'");
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get the next houseId
		 */ 
		public function getNextHouseId(){
			try{
				$stmt = $this->conn->prepare("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'dh_house_project'");
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getNextCloseId(){
			try{
				$stmt = $this->conn->prepare("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'dh_closing'");
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get The Next News Id
		 */
		public function getNextNewsId(){
			try {
				$stmt = $this->conn->prepare("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'dh_news'");
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get All Developers
		 */
		public function getDevelopers(){
			try{
				$stmt = $this->conn->prepare("SELECT * FROM dh_developers");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}


		public function getSubdivisionexceptEdit($subd){
			try{
				$stmt = $this->conn->prepare("SELECT * FROM dh_subdivisions WHERE dh_subd_name != ?");
				$stmt->execute(array($subd));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get All Division
		 */
		public function getAllDivision(){
			try{
				$stmt = $this->conn->prepare("SELECT * FROM dh_divisions WHERE dh_division_status =0");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllDivisionexceptEdit($divisionId){
			try{
				$stmt = $this->conn->prepare("SELECT * FROM dh_divisions WHERE dh_division_id != ?");
				$stmt->execute(array($divisionId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getDownlinesPerDivision($divId, $userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM 
						INNER JOIN dh_user_details as uMD 
						ON uM.dh_user_details_id = uMD.dh_user_details_id 
						INNER JOIN dh_user_groups as uG ON 
						uM.dh_user_group_id = uG.dh_user_group_id
						INNER JOIN dh_divisions as uDiv ON
						uMD.dh_division_id = uDiv.dh_division_id
						WHERE is_deleted = 0 AND dh_status = 'Active'
						AND uMD.dh_division_id = ? AND dh_user_id != ?");
				$stmt->execute(array($divId,$userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}		
		}

		public function getSaleDirectorPerDivision($divId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM 
						INNER JOIN dh_user_details as uMD 
						ON uM.dh_user_details_id = uMD.dh_user_details_id 
						INNER JOIN dh_user_groups as uG ON 
						uM.dh_user_group_id = uG.dh_user_group_id
						INNER JOIN dh_divisions as uDiv ON
						uMD.dh_division_id = uDiv.dh_division_id
						WHERE is_deleted = 0 AND dh_status = 'Active'
						AND uMD.dh_division_id = ? AND uM.dh_user_group_id = 3");
				$stmt->execute(array($divId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getDownlinesSalesDirector($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM 
						INNER JOIN dh_user_details as uMD 
						ON uM.dh_user_details_id = uMD.dh_user_details_id 
						INNER JOIN dh_user_groups as uG ON 
						uM.dh_user_group_id = uG.dh_user_group_id
						INNER JOIN dh_divisions as uDiv ON
						uMD.dh_division_id = uDiv.dh_division_id
						WHERE is_deleted = 0 AND dh_status = 'Active'
						AND uMD.dh_recruited_by = ? AND uM.dh_user_group_id != 3 AND uM.dh_user_group_id != 2");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getMyClosingSales($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing WHERE dh_prepared_user_id = ? AND dh_closing_status = 'Approve' ORDER BY dh_closing_id DESC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function ClosingSalesbysalesId($salesId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing  as cL INNER JOIN dh_users as uM 
						ON cL.dh_prepared_user_id = uM.dh_user_id INNER JOIN dh_divisions as uD 
						ON cL.dh_division = uD.dh_division_id INNER JOIN dh_user_details as uMD 
						ON cL.dh_prepared_user_id = uMD.dh_user_details_id WHERE dh_closing_id = ?");
				$stmt->execute(array($salesId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getMySeminarRequest($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar WHERE dh_user_id = ? ORDER BY dh_seminar_id DESC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getMyVehicleTripRequest($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE dh_user_id = ? ORDER BY dh_vehicle_trip_id DESC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getAllHouseProject($houseprojId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project WHERE dh_house_proj_id = ? ORDER BY dh_house_proj_id DESC");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllSeminarRequest($status){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar INNER JOIN dh_user_details ON dh_user_id = dh_user_details_id WHERE dh_seminar_status = ? ORDER BY dh_seminar_id DESC");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllVehicleTripRequest($status){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar INNER JOIN dh_user_details ON dh_user_id = dh_user_details_id WHERE dh_trip_status = ? ORDER BY dh_vehicle_trip_id DESC");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllClosingSalesRequest($status){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing as cL INNER JOIN dh_user_details as uMD ON cL.dh_user_id = uMD.dh_user_details_id WHERE dh_closing_status = ? ORDER BY dh_closing_id DESC");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get All Registered Users
		 * @param Status (For Activation , Active , Null)
		 */
		public function getAllUser($status = null){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE is_deleted = 0 AND dh_status = ?");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}


		/*
		 * Get All Active Sales Agent For Activation
		 * @param userId
		 */
		public function getAllSalesUser($status, $userGroupId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE is_deleted = 0 AND dh_status = ? AND uM.dh_user_group_id = ?");
				$stmt->execute(array($status, $userGroupId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllUsers($status){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE is_deleted = 0 AND dh_status = ?");
				$stmt->execute(array($status));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Get All Active Users Except Employee and Admin
		 * 
		 */
		public function getActiveUsersExceptEmpAdmin(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id != 5 AND dh_user_group_id != 1");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}


		public function getAllActiveFirstnameSalesDirector(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM 
						INNER JOIN dh_user_details as uMD 
						ON uM.dh_user_details_id = uMD.dh_user_details_id 
						INNER JOIN dh_user_groups as uG ON 
						uM.dh_user_group_id = uG.dh_user_group_id
						INNER JOIN dh_divisions as uDiv ON
						uMD.dh_division_id = uDiv.dh_division_id
						WHERE is_deleted = 0 AND dh_status = 'Active'
						AND uMD.dh_division_id = 6 AND dh_user_id != 7");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getAllActiveFirstnameDivisionManager(){
			try {
				$stmt = $this->conn->prepare("SELECT dh_firstName FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_divisions as uG ON uMD.dh_division_id = uG.dh_division_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 2 ");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}


		/*
		 * Get All Active Sales Director
		 *
		 */
		public function getAllActiveSalesDirector(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_divisions as uG ON uMD.dh_division_id = uG.dh_division_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 3 ");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getAllActiveSalesAgent(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_divisions as uG ON uMD.dh_division_id = uG.dh_division_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 4 ");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		/*
		 * Get All Active Sales Director and Division Manager
		 *
		 */
		public function getAllSalesAndDiv(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 3 OR dh_user_group_id = 2 ORDER BY uM.dh_user_id DESC");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		public function getAllRecruiter(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE is_deleted = 0 AND dh_status = 'Active' ORDER BY uM.dh_user_id DESC");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		/*
		 * Get All Active Division Manager
		 *
		 */
		public function getAllActiveDivisionManager(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_divisions as uD ON uMD.dh_division_id = uD.dh_division_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 2 ");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}


		public function getAllDivisionManagerwithDivisionName(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_divisions as dI ON uMD.dh_division_id = dI.dh_division_id WHERE is_deleted = 0 AND dh_status = 'Active' AND dh_user_group_id = 2 ");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}			
		}

		/*
		 * Logout Function
		 */
		public function doLogout(){
			unset($_SESSION['user_session']);
			unset($_SESSION['user_type']);
			unset($_SESSION['message']);
			return true;
		}

		/*
		 * Executes Login
		 * @param string $uname - Username
		 * @param string $pass - Password
		 */
		public function login($uname,$pass){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_user_name = ? AND is_deleted = 0 AND dh_status = 'Active' LIMIT 1");
				$stmt->execute(array($uname));

				$userRow = $stmt->fetch(PDO::FETCH_ASSOC);
				if($stmt->rowCount() > 0){
					if($userRow['login_counter'] < 3 && $userRow['dh_user_pass'] == base64_encode($pass)){
						$_SESSION['user_session'] = $userRow['dh_user_id'];
						$_SESSION['user_fullName'] = $userRow['dh_firstName'].' '.$userRow['dh_lastName'];
						$_SESSION['user_type'] = $userRow['dh_user_group_id'];
						$_SESSION['user_division'] = $userRow['dh_division_id'];
						$_SESSION['MsgCode'] = 1;
						if($userRow['dh_user_group_id'] != '1'){
							$_SESSION['Message'] = 'Welcome! '.$userRow['dh_firstName'].' '.$userRow['dh_lastName'];
						} else {
							$_SESSION['Message'] = 'Welcome! Administrator';
						}
						$this->updateLoginCounter($userRow['dh_user_id'], 0);
						return true;
					} else {
						if($userRow['login_counter'] == '0'){
							$_SESSION["Message"] = "Incorrect Password Please Try Again... Number of tries remaining 3";
							$this->updateLoginCounter($userRow['dh_user_id'],1);
						} else if ($userRow['login_counter'] == '1') {
							$_SESSION["Message"] = "Incorrect Password Please Try Again... Number of tries remaining 2";
							$this->updateLoginCounter($userRow['dh_user_id'],2);
						} else if ($userRow['login_counter'] == '2') {
							$_SESSION["Message"] = "Incorrect Password Please Try Again... Number of tries remaining 1";
							$this->updateLoginCounter($userRow['dh_user_id'],3);
						} else {
							$_SESSION["Message"] = "Account has been locked. Please Contact administrator for more details.";
							$this->updateLoginCounter($userRow['dh_user_id'],4);
						}
						$_SESSION['MsgCode'] = 2;
						return false;
					}
				} else {
					if($userRow['login_counter'] > 3){
						$_SESSION["Message"] = "Account has been Locked. Please Contact an Administrator for more Details.";
					} else {
						$_SESSION["Message"] = "Username Doesn't Exist";
					}
					$_SESSION['MsgCode'] = 2;
					return false;
				}
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}


		/*
		 * Register New User
		 * @param array $params - data to be inserted (register.php)
		 * 
		 */
		public function insertUserData($params){
			try {
				$this->conn->beginTransaction();

				$getNextUserId = $this->getNextUserId();

				$stmt = $this->conn->prepare("INSERT INTO dh_users
				(dh_user_id,dh_user_group_id,dh_user_details_id,dh_user_name,dh_user_pass,dh_date_created,dh_status,dh_account_create) 
				VALUES (?,?,?,?,?,?,?,?)");
				if($params['password'] == ""){
					$password = base64_encode($this->default_password); // Default password if Leave Blank
				} else {
					$password = base64_encode($params['password']);
				}

					$dataUsers = array(
						$params['userId'],
						$params['userType'],
						$params['idUser'],
						$params['username'],
						$password,
						date("Y-m-d"),
						'For Activation',
						$params['accountCreate']
					);

				$stmt->execute($dataUsers);

				// User Details
				$detailsStmt = $this->conn->prepare("INSERT INTO dh_user_details (dh_user_details_id,dh_division_id, dh_firstName,dh_middleName,dh_lastName,dh_user_nickname,dh_bday,dh_age,dh_gender,dh_tin_number,dh_email_address,dh_contact_no,dh_home_address,dh_occupation,dh_seminar_date,dh_seminar_venue,dh_recruited_by,dh_recruited_by_position,dh_recruited_by_division,dh_trainor_name,dh_sales_director,dh_division_manager,dh_realty_name,dh_realty_pos,dh_realty_from,dh_realty_to) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					
					$dataUserDetails = array(
						$params['userId'],
						$params['divisionId'],
						$params['firstName'],
						$params['middleName'],
						$params['lastName'],
						$params['nickName'],
						$params['DOB'],
						$params['Age'],
						$params['gender'],
						$params['tinNumber'],
						$params['emailAddress'],
						$params['contactNo'],
						$params['homeAddress'],
						$params['occupation'],
						$params['seminarDate'],
						$params['seminarVenue'],
						$params['recruitedBy'],
						$params['recruitedByPos'],
						$params['recruitedByDiv'],
						$params['trainorName'],
						$params['salesDirector'],
						$params['divManager'],
						$params['occ_name'],
						$params['occ_pos'],
						$params['occ_from'],
						$params['occ_to'],
						);

				$detailsStmt->execute($dataUserDetails);
				
				// User Education Details
				$educStmt = $this->conn->prepare("INSERT INTO dh_user_education (dh_user_id, dh_school_name, dh_school_location, dh_attainment, dh_year_from, dh_year_to) VALUES (?,?,?,?,?,?)");

				foreach($params['educSchool'] as $key=>$value){
					$dataUserEducation = array(
						$params['userId'],
						$params['educSchool'][$key],
						$params['educLocation'][$key],
						$params['educAttainment'][$key],
						$params['educYearFrom'][$key],
						$params['educYearTo'][$key]
					);

					$educStmt->execute($dataUserEducation);
				}


				$this->conn->commit();
				if($params['userType'] == 4){
					$_SESSION['Message'] = "Registered Successfully! Thank you for your time. we'll contact you once your account has been activated";
				} else {
					$_SESSION['Message'] = "Registered Successfully!";
				}

				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function insertUserDatabyAdmin($params){
			try {
				$this->conn->beginTransaction();

				$getNextUserId = $this->getNextUserId();

				$stmt = $this->conn->prepare("INSERT INTO dh_users
				(dh_user_id,dh_user_group_id,dh_user_details_id,dh_user_name,dh_user_pass,dh_date_created,dh_status,dh_account_create) 
				VALUES (?,?,?,?,?,?,?,?)");
				if($params['password'] == ""){
					$password = base64_encode($this->default_password); // Default password if Leave Blank
				} else {
					$password = base64_encode($params['password']);
				}

					$dataUsers = array(
						$params['userId'],
						$params['userType'],
						$params['idUser'],
						$params['username'],
						$password,
						date("Y-m-d"),
						$params['userstatus'],
						$params['accountCreate']
					);

				$stmt->execute($dataUsers);

				// User Details
				$detailsStmt = $this->conn->prepare("INSERT INTO dh_user_details (dh_user_details_id,dh_division_id, dh_firstName,dh_middleName,dh_lastName,dh_user_nickname,dh_bday,dh_age,dh_gender,dh_tin_number,dh_email_address,dh_contact_no,dh_home_address,dh_occupation,dh_seminar_date,dh_seminar_venue,dh_recruited_by,dh_recruited_by_position,dh_recruited_by_division,dh_trainor_name,dh_sales_director,dh_division_manager,dh_realty_name,dh_realty_pos,dh_realty_from,dh_realty_to) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					
					$dataUserDetails = array(
						$params['userId'],
						$params['divisionId'],
						$params['firstName'],
						$params['middleName'],
						$params['lastName'],
						$params['nickName'],
						$params['DOB'],
						$params['Age'],
						$params['gender'],
						$params['tinNumber'],
						$params['emailAddress'],
						$params['contactNo'],
						$params['homeAddress'],
						$params['occupation'],
						$params['seminarDate'],
						$params['seminarVenue'],
						$params['recruitedBy'],
						$params['recruitedByPos'],
						$params['recruitedByDiv'],
						$params['trainorName'],
						$params['salesDirector'],
						$params['divManager'],
						$params['occ_name'],
						$params['occ_pos'],
						$params['occ_from'],
						$params['occ_to'],
						);

				$detailsStmt->execute($dataUserDetails);
				
				// User Education Details
				$educStmt = $this->conn->prepare("INSERT INTO dh_user_education (dh_user_id, dh_school_name, dh_school_location, dh_attainment, dh_year_from, dh_year_to) VALUES (?,?,?,?,?,?)");

				foreach($params['educSchool'] as $key=>$value){
					$dataUserEducation = array(
						$params['userId'],
						$params['educSchool'][$key],
						$params['educLocation'][$key],
						$params['educAttainment'][$key],
						$params['educYearFrom'][$key],
						$params['educYearTo'][$key]
					);

					$educStmt->execute($dataUserEducation);
				}


				$this->conn->commit();
				$_SESSION['Message'] = "Registered Successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}


		/*
		 * Add Closing
		 * @param array $params - Data to be Inserted (empEncode.php-closingForm)
		 *
		 */
		public function insertClosingData($params){
			try {
				$this->conn->beginTransaction();

				$getNextCloseId = $this->getNextCloseId();

				$stmt = $this->conn->prepare("INSERT INTO dh_closing (dh_familyName, dh_Firstname, dh_phoneNo, dh_telNo, dh_subdivision, dh_location, dh_developer, dh_phase, dh_block, dh_lot, dh_houseModel, dh_floorArea, dh_lotArea, dh_tcpDev, dh_reservationFee, dh_oiPR, dh_division, dh_divisionManager, dh_div_comrate, dh_salesDirector, dh_dir_comrate, dh_salesTrainee, dh_sales_comrate, dh_rc_divisionManager, dh_rcdiv_comrate, dh_rc_salesDirector, dh_rcdir_comrate, dh_rc_salesTrainee, dh_rcsales_comrate, dh_rfa, dh_rfa_comrate, dh_bookAssistant, dh_book_comrate, dh_aAssistant, dh_a_comrate, dh_closingDate, dh_created_date, dh_user_id, dh_prepared_user_id,dh_closing_status) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


				$dataClosing = array(
					$params['familyName'],
					$params['firstName'],
					$params['phoneNo'],
					$params['telno'],
					$params['subdivision'],
					$params['location'],
					$params['developer'],
					$params['phase'],
					$params['block'],
					$params['lot'],
					$params['housemodel'],
					$params['floorarea'],
					$params['lotarea'],
					$params['tcpdev'],
					$params['reservationfee'],
					$params['oipr'],
					$params['division'],
					$params['divisionmanager'],
					$params['divcomrate'],
					$params['salesdirector'],
					$params['dircomrate'],
					$params['salestrainee'],
					$params['salescomrate'],
					$params['rcdiv'],
					$params['rcdivcomrate'],
					$params['rcdir'],
					$params['rcdircomrate'],
					$params['rcsales'],
					$params['rcsalescomrate'],
					$params['rfa'],
					$params['rfacomrate'],
					$params['bookassist'],
					$params['bookcomrate'],
					$params['aassist'],
					$params['acomrate'],
					$params['closingdate'],
					$params['dateCreated'],
					$params['user'],
					$params['preparedBy'],
					$params['status']
				);

				$stmt->execute($dataClosing);
				
				$this->conn->commit();
				$_SESSION['Message'] = "Closing Successfully Added! Wait for the Approval";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}			
		}

		// public function insertClosingData($params){
		// 	try {
		// 		$this->conn->beginTransaction();

		// 		$getNextCloseId = $this->getNextCloseId();

		// 		$stmt = $this->conn->prepare("INSERT INTO dh_closing (dh_familyName, dh_Firstname, dh_phoneNo, dh_telNo, dh_subdivision, dh_location, dh_developer, dh_phase, dh_block, dh_lot, dh_houseModel, dh_floorArea, dh_lotArea, dh_tcpDev, dh_reservationFee, dh_oiPR, dh_division, dh_divisionManager, dh_div_comrate, dh_salesDirector, dh_dir_comrate, dh_salesTrainee, dh_sales_comrate, dh_rc_divisionManager, dh_rcdiv_comrate, dh_rc_salesDirector, dh_rcdir_comrate, dh_rc_salesTrainee, dh_rcsales_comrate, dh_rfa, dh_rfa_comrate, dh_bookAssistant, dh_book_comrate, dh_aAssistant, dh_a_comrate, dh_closingDate, dh_created_date, dh_user_id, dh_prepared_user_id) 
		// 		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


		// 		$dataClosing = array(
		// 			$params['familyName'],
		// 			$params['firstName'],
		// 			$params['phoneNo'],
		// 			$params['telno'],
		// 			$params['subdivision'],
		// 			$params['location'],
		// 			$params['developer'],
		// 			$params['phase'],
		// 			$params['block'],
		// 			$params['lot'],
		// 			$params['housemodel'],
		// 			$params['floorarea'],
		// 			$params['lotarea'],
		// 			$params['tcpdev'],
		// 			$params['reservationfee'],
		// 			$params['oipr'],
		// 			$params['division'],
		// 			$params['divisionmanager'],
		// 			$params['divcomrate'],
		// 			$params['salesdirector'],
		// 			$params['dircomrate'],
		// 			$params['salestrainee'],
		// 			$params['salescomrate'],
		// 			$params['rcdiv'],
		// 			$params['rcdivcomrate'],
		// 			$params['rcdir'],
		// 			$params['rcdircomrate'],
		// 			$params['rcsales'],
		// 			$params['rcsalescomrate'],
		// 			$params['rfa'],
		// 			$params['rfacomrate'],
		// 			$params['bookassist'],
		// 			$params['bookcomrate'],
		// 			$params['aassist'],
		// 			$params['acomrate'],
		// 			$params['closingdate'],
		// 			$params['dateCreated'],
		// 			$params['user'],
		// 			$params['preparedBy']
		// 		);

		// 		$stmt->execute($dataClosing);
				
		// 		$this->conn->commit();
		// 		$_SESSION['Message'] = "Closing Successfully Added!";
		// 		return true;
		// 	} catch (PDOException $e){
		// 		echo $e->getMessage();
		// 		throw $e;
		// 	}			
		// }

		public function updateClosingData($params){
			try {
				$this->conn->beginTransaction();

				$stmt = $this->conn->prepare("UPDATE dh_closing SET dh_familyName=?, dh_Firstname=?, dh_phoneNo=?, dh_telNo=?, dh_subdivision=?, dh_location=?, dh_developer=?, dh_phase=?, dh_block=?, dh_lot=?, dh_houseModel=?, dh_floorArea=?, dh_lotArea=?, dh_tcpDev=?, dh_reservationFee=?, dh_oiPR=?, dh_division=?, dh_divisionManager=?, dh_div_comrate=?, dh_salesDirector=?, dh_dir_comrate=?, dh_salesTrainee=?, dh_sales_comrate=?, dh_rc_divisionManager=?, dh_rcdiv_comrate=?, dh_rc_salesDirector=?, dh_rcdir_comrate=?, dh_rc_salesTrainee=?, dh_rcsales_comrate=?, dh_rfa=?, dh_rfa_comrate=?, dh_bookAssistant=?, dh_book_comrate=?, dh_aAssistant=?, dh_a_comrate=?, dh_closingDate=? WHERE dh_closing_id = ?");


				$dataClosing = array(
					$params['familyName'],
					$params['firstName'],
					$params['phoneNo'],
					$params['telno'],
					$params['subdivision'],
					$params['location'],
					$params['developer'],
					$params['phase'],
					$params['block'],
					$params['lot'],
					$params['housemodel'],
					$params['floorarea'],
					$params['lotarea'],
					$params['tcpdev'],
					$params['reservationfee'],
					$params['oipr'],
					$params['division'],
					$params['divisionmanager'],
					$params['divcomrate'],
					$params['salesdirector'],
					$params['dircomrate'],
					$params['salestrainee'],
					$params['salescomrate'],
					$params['rcdiv'],
					$params['rcdivcomrate'],
					$params['rcdir'],
					$params['rcdircomrate'],
					$params['rcsales'],
					$params['rcsalescomrate'],
					$params['rfa'],
					$params['rfacomrate'],
					$params['bookassist'],
					$params['bookcomrate'],
					$params['aassist'],
					$params['acomrate'],
					$params['closingdate'],
					$params['salesId']

				);

				$stmt->execute($dataClosing);

				
				$this->conn->commit();
				$_SESSION['Message'] = "Closing Update Successfully! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}			
		}

		/*
		 * Update Status
		 * @param array - EmpId and Status to be change
		 */
		public function updateStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_users set dh_status=? WHERE dh_user_id = ?");
				$updateData = array(
					$params['status'],
					$params['userId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateHousebyId($params){
			try{
				$this->conn->beginTransaction();
				$getNextFinishId = $this->getNextFinishId();
				$stmt = $this->conn->prepare("UPDATE dh_house_project set dh_house_name=? , dh_description=?, dh_location=?, dh_promos=?, dh_subd_id=? WHERE dh_house_proj_id = ?");
				$updateData = array(
					$params['houseName'],
					$params['description'],
					$params['location'],
					$params['promos'],
					$params['subd'],
					$params['houseprojId']
				);
				$stmt->execute($updateData);

				$finStmt = $this->conn->prepare("DELETE FROM dh_finish WHERE dh_finish_id=?");
				foreach($params['finishId'] as $key=>$value){
					$dataHouseFinish = array(
						$params['finishId'][$key]
					);

					$finStmt->execute($dataHouseFinish);
				}

				$finStmt = $this->conn->prepare("INSERT INTO dh_finish(dh_house_proj_id,dh_finish_description) VALUES (?,?)");

				foreach($params['finish'] as $key=>$value){
					$dataHouseFinish = array(
						$params['finishhouseprojId'][$key],
						$params['finish'][$key]
					);

					$finStmt->execute($dataHouseFinish);
				}
				
				$this->conn->commit();
				$_SESSION['Message'] = "House Project Update Successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateNewsbyId($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_news set dh_title=? , dh_content=? WHERE dh_news_id = ?");
				$updateData = array(
					$params['title'],
					$params['content'],
					$params['newsId']
				);
				$stmt->execute($updateData);
				
				$this->conn->commit();
				$_SESSION['Message'] = "News Update Successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateHouse($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_house_project set dh_house_name=? WHERE dh_house_proj_id = ?");
				$updateData = array(
					$params['houseName'],
					$params['houseprojId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Update Status
		 * @param array - SeminarId and Status to be change
		 */
		public function updateSeminarStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_seminar_calendar set dh_seminar_status=? WHERE dh_seminar_id = ?");
				$updateData = array(
					$params['status'],
					$params['seminarId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/*
		 * Update Status
		 * @param array - EmpId and Status to be change
		 */
		public function updateVehicleTripStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_vehicletrip_calendar set dh_trip_status=? WHERE dh_vehicle_trip_id = ?");
				$updateData = array(
					$params['status'],
					$params['tripId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateClosingStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_closing set dh_closing_status=? WHERE dh_closing_id = ?");
				$updateData = array(
					$params['status'],
					$params['closingId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function archiveNewsbyStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_news set dh_news_status=? WHERE dh_news_id = ?");
				$updateData = array(
					$params['status'],
					$params['newsId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Status Changed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateSeminarbyId($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_seminar_calendar set dh_seminar_name=?,dh_seminar_date=?,dh_seminar_time_start=?,dh_seminar_time_end=?,dh_seminar_location=? WHERE dh_seminar_id = ?");
				$updateData = array(
					$params['seminarName'],
					$params['seminarDate'],
					$params['timeStart'],
					$params['timeEnd'],
					$params['seminarLocation'],
					$params['seminarId']

				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Seminar Update Successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}


		/*
		 * Insert House Project Data
		 * @params - Data to be inserted (empEncode.php-houseProject)
		 */
		public function insertHouseProject($params){
			try {
				$this->conn->beginTransaction();
				$getNextHouseId = $this->getNextHouseId();
				$stmt = $this->conn->prepare("INSERT INTO dh_house_project
				(dh_house_name,dh_house_price,dh_description,dh_location,dh_promos,dh_subd_id,dh_date_created,dh_user_id) 
				VALUES (?,?,?,?,?,?,?,?)");

					$houseData = array(
						$params['houseName'],
						$params['houseprice'],
						$params['description'],
						$params['location'],
						$params['promos'],
						$params['subd'],
						date("Y-m-d"),
						$params['userId']
					);

				$stmt->execute($houseData);

				// User Education Details
				$finStmt = $this->conn->prepare("INSERT INTO dh_finish (dh_house_proj_id, dh_finish_description) VALUES (?,?)");
				foreach($params['finish'] as $key=>$value){
					$dataHouseFinish = array(
						$getNextHouseId['AUTO_INCREMENT'],
						$params['finish'][$key]
					);

					$finStmt->execute($dataHouseFinish);
				}

				$this->conn->commit();
				$_SESSION['Message'] = "House Project Successfully Added! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		/* 
		 * Insert News Data
		 * @params - Data to be inserted (empEncode.php - News / Article)
		 */
		public function insertNews($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_news
				(dh_title,dh_content,dh_date_created,dh_encoded_by) 
				VALUES (?,?,?,?)");

					$newsData = array(
						$params['title'],
						$params['content'],
						date("Y-m-d"),
						$params['userId']
					);

				$stmt->execute($newsData);
				$this->conn->commit();
				$_SESSION['Message'] = "News Article Successfully Added! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}			
		}

		/*
		 * Insert Media Images
		 * @params Images
		 */
		public function insertImages($params,$imagetype = null){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_images
				(dh_house_project_id,dh_news_id,isHouseSample,isFloorPlan,isNews,dh_image_path) 
				VALUES (?,?,?,?,?,?)");

				$houseId = $this->getNextHouseId();
				$newsId = $this->getNextNewsId();


				if($imagetype != null){
					$imageData = array(
						null,
						$newsId['AUTO_INCREMENT'],
						0,
						0,
						$params['isNews'],
						$params['imagePath']
					);
				} else {
					$imageData = array(
						$houseId['AUTO_INCREMENT'],
						null,
						$params['isHouseSample'],
						$params['isFloorPlan'],
						0,
						$params['imagePath'],
					);
				}
				

				$stmt->execute($imageData);
				$this->conn->commit();
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function validateSeminar($semname,$semdate,$start,$end){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar WHERE dh_seminar_name = ? AND dh_seminar_date = ? AND dh_seminar_time_start = ? AND dh_seminar_time_end = ?");
				$stmt->execute(array($semname,$semdate,$start,$end));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if ($stmt->rowCount() > 0){
					
					$vals = 'true';
					return $vals;
				}
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function validateTripping($tripdate,$pickup,$destination){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE dh_trip_date = ? AND dh_location_pickup_time = ? AND dh_location_destination_time = ?");
				$stmt->execute(array($tripdate,$pickup,$destination));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if ($stmt->rowCount() > 0){
					
					$vals = true;
					return $vals;
				}
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function validateTrippingDriver($tripdate,$pickup,$destination,$drivername){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE dh_trip_date = ? AND dh_location_pickup_time = ? AND dh_location_destination_time = ? AND dh_driver_name = ?");
				$stmt->execute(array($tripdate,$pickup,$destination,$drivername));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if ($stmt->rowCount() > 0){
					
					$vals = true;
					return $vals;
				}
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function validateTrippingVehicle($tripdate,$pickup,$destination,$plateno){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE dh_trip_date = ? AND dh_location_pickup_time = ? AND dh_location_destination_time = ? AND dh_plateno = ?");
				$stmt->execute(array($tripdate,$pickup,$destination,$plateno));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if ($stmt->rowCount() > 0){
					
					$vals = true;
					return $vals;
				}
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}


		/*
		 * Insert Request Seminar Data
		 * @params - Data to be inserted (divRequestSched.php - Seminar)
		 */
		public function insertRequestSeminar($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_seminar_calendar
				(dh_seminar_name, dh_seminar_date, dh_seminar_time_start, dh_seminar_time_end, dh_seminar_location, dh_date_created, dh_user_id) 
				VALUES (?,?,?,?,?,?,?)");

				$seminarData = array(
					$params['seminarName'],
					$params['seminarDate'],
					$params['timeStart'],
					$params['timeEnd'],
					$params['seminarLocation'],
					date("Y-m-d"),
					$params['userId']
				);

				$stmt->execute($seminarData);
				$this->conn->commit();
				$_SESSION['Message'] = "Request Successfully Submitted for Approval! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function insertSeminar($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_seminar_calendar
				(dh_seminar_name, dh_seminar_date, dh_seminar_time_start, dh_seminar_time_end, dh_seminar_location, dh_date_created, dh_user_id, dh_seminar_status) 
				VALUES (?,?,?,?,?,?,?,?)");

				$seminarData = array(
					$params['seminarName'],
					$params['seminarDate'],
					$params['timeStart'],
					$params['timeEnd'],
					$params['seminarLocation'],
					date("Y-m-d"),
					$params['userId'],
					$params['status']

				);

				$stmt->execute($seminarData);
				$this->conn->commit();
				$_SESSION['Message'] = "Seminar Schedule Successfully Submitted! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		/*
		 * Insert Request Scheduled Trip Data
		 * @params - Data to be inserted (divRequestSched.php - Vehicle Trip)
		 */
		public function insertRequestVehicleTrip($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_vehicletrip_calendar
				(dh_trip_date, dh_location_pickup, dh_location_pickup_time, dh_location_destination, dh_location_destination_time, dh_driver_name, dh_plateno, dh_driver_contact,dh_user_id, dh_date_created) 
				VALUES (?,?,?,?,?,?,?,?,?,?)");

				$tripData = array(
					$params['tripDate'],
					$params['locationPickUp'],
					$params['locationPickUpTime'],
					$params['locationDestination'],
					$params['locationDestinationTime'],
					$params['driverName'],
					$params['plateNo'],
					$params['contactNo'],
					$params['userId'],
					date("Y-m-d")
				);

				$stmt->execute($tripData);
				$this->conn->commit();
				$_SESSION['Message'] = "Vehicle Tripping Schedule Successfully Submitted! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function insertVehicleTrip($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("INSERT INTO dh_vehicletrip_calendar
				(dh_trip_date, dh_location_pickup, dh_location_pickup_time, dh_location_destination, dh_location_destination_time, dh_driver_name, dh_plateno, dh_driver_contact,dh_user_id, dh_date_created,dh_trip_status) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?)");

				$tripData = array(
					$params['tripDate'],
					$params['locationPickUp'],
					$params['locationPickUpTime'],
					$params['locationDestination'],
					$params['locationDestinationTime'],
					$params['driverName'],
					$params['plateNo'],
					$params['contactNo'],
					$params['userId'],
					date("Y-m-d"),
					$params['status']
				);

				$stmt->execute($tripData);
				$this->conn->commit();
				$_SESSION['Message'] = "Vehicle Tripping Schedule Successfully Submitted! ";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		/* reports */

		public function getAllUserDetails($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_user_id != ? AND dh_user_group_id > 1 ORDER BY dh_lastName ASC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllUserDetailsAdmin($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE dh_user_id != ? AND uM.dh_user_group_id > 1 AND dh_status != 'Declined' ORDER BY dh_lastName ASC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllSubdivision(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_subdivisions ORDER BY dh_subd_id ASC");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllSeminar(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getSeminarbyDay($day, $month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar WHERE DAY(dh_date_created) = ? AND MONTH(dh_date_created) = ?");
				$stmt->execute(array($day, $month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getSeminarbyMonth($month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_seminar_calendar WHERE MONTH(dh_date_created) = ?");
				$stmt->execute(array($month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getVehicleTripbyDay($day,$month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE DAY(dh_date_created) = ? AND MONTH(dh_date_created) = ?");
				$stmt->execute(array($day,$month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getVehicleTripbyMonth($month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar WHERE MONTH(dh_date_created) = ?");
				$stmt->execute(array($month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllClosingSales(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing as cL INNER JOIN dh_users as uM on cL.dh_prepared_user_id = uM.dh_user_id INNER JOIN dh_user_details as uMD on uM.dh_user_details_id = uMD.dh_user_details_id");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllClosingSalesbyDay($day,$month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing as cL INNER JOIN dh_users as uM on cL.dh_prepared_user_id = uM.dh_user_id INNER JOIN dh_user_details as uMD on uM.dh_user_details_id = uMD.dh_user_details_id WHERE DAY(dh_created_date) = ? AND MONTH(dh_created_date) = ?");
				$stmt->execute(array($day,$month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllClosingSalesbyMonth($month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing as cL INNER JOIN dh_users as uM on cL.dh_prepared_user_id = uM.dh_user_id INNER JOIN dh_user_details as uMD on uM.dh_user_details_id = uMD.dh_user_details_id WHERE MONTH(dh_created_date) = ?");
				$stmt->execute(array($month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllUsersbyMonth($userId,$month){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE dh_user_id != ? AND uM.dh_user_group_id > 1 AND MONTH(dh_date_created) = ?");
				$stmt->execute(array($userId,$month));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}


		public function getAllVehicleTrip(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicletrip_calendar");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getSeminarbyId($semId){
			try {
				$stmt = $this->conn->prepare("SELECT dh_seminar_id,dh_seminar_name, dh_seminar_date,dh_seminar_time_start,dh_seminar_time_end,dh_seminar_location FROM dh_seminar_calendar WHERE dh_seminar_id = ?");
				$stmt->execute(array($semId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUserbyUserId($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_user_id = ?");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUserbyEmailAdd($email){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_email_address = ?");
				$stmt->execute(array($email));
				$result = $stmt->fetchColumn();
				if( $result == 0 ){
					$_SESSION['Message'] = "User with that email doesn't exist!";
					return $result;
				}else{

					$ustmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_email_address = ?");
					$ustmt->execute(array($email));
					$result = $ustmt->fetchAll(PDO::FETCH_ASSOC);

					foreach($result as $userData){
						$email = $userData['dh_email_address'];
						$password = $userData['dh_user_pass'];
						$firstname = $userData['dh_firstName'];
					}

					$to = $email;
					$subject = 'Password Reset Link @ dreamhouserealty.000webhostapp.com';
					$message_body = '
					Hello '.$firstname.',

					You have requested password reset!

					Please click this link to reset your password:

					https://dreamhouserealty.000webhostapp.com/website/resetpassword.php?email='.$email.'&password='.$password;  

					mail($to, $subject, $message_body);

					return $result;
				}

			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUsertoResetPassword($email,$pass){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id WHERE dh_email_address = ? AND dh_user_pass = ?");
				$stmt->execute(array($email,$pass));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if($result == 0){
					$_SESSION['message'] = "You have entered invalid URL for password reset!";
					return $result;
				}else{
					$_SESSION['message'] = "Sorry, verification failed, try again!";
					return $result;
				}
				
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function UpdatePasswordbyEmail($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id SET dh_user_pass=? WHERE dh_email_address=? ");

				if($params['newpassword'] == ""){
					$password = base64_encode($this->default_password); // Default password if Leave Blank
				} else {
					$password = base64_encode($params['newpassword']);
				}

				$passData = array(
					$password,
					$params['email']
				);
				

				$stmt->execute($passData);
				$this->conn->commit();
				$_SESSION['message'] = "Your password has been change successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function SearchAll($projectName,$description,$location,$price){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_house_project as hP INNER JOIN dh_images as iM ON hP.dh_house_proj_id = iM.dh_house_project_id INNER JOIN dh_subdivisions as sU ON hP.dh_subd_id = sU.dh_subd_id WHERE dh_house_name LIKE ? OR dh_description LIKE ? OR dh_subd_location LIKE ? OR dh_house_price LIKE ? AND isHouseSample = 1 GROUP BY hP.dh_house_proj_id");
				$stmt->execute(array($projectName,$description,$location,$price));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUserFullDetailsbyUserId($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_groups as uG ON uM.dh_user_group_id = uG.dh_user_group_id WHERE dh_user_id = ? AND dh_status = 'Active'");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUserEducbyUserId($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_user_education as uE INNER JOIN dh_users as uM ON uE.dh_user_id = uM.dh_user_id WHERE uM.dh_user_id = ? AND dh_status = 'Active'");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getUserSalesbyUserId($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_closing WHERE dh_prepared_user_id = ? AND dh_closing_status = 'Approve'");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}



		public function updateUserbyId($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id SET dh_user_id=?,dh_firstName=?, dh_middleName=?, dh_lastName=?, dh_user_nickname=?, dh_bday=?, dh_age=?, dh_gender=?, dh_tin_number=?, dh_email_address=?, dh_contact_no=?, dh_home_address=?, dh_user_name=? WHERE dh_user_id = ?");
				
				$updateData = array(
					$params['usersId'],
					$params['firstName'],
					$params['middleName'],
					$params['lastName'],
					$params['nickName'],
					$params['DOB'],
					$params['Age'],
					$params['gender'],
					$params['tinNumber'],
					$params['emailAddress'],
					$params['contactNo'],
					$params['homeAddress'],
					$params['username'],
					$params['userId']
				);
				$stmt->execute($updateData);

				// $ustmt = $this->conn->prepare("UPDATE dh_users SET dh_user_id=?, dh_user_details_id=? WHERE dh_user_id=?");
				// $userData = array(
				// 	$params['usersId'],
				// 	$params['userDetails'],
				// 	$params['userId']

				// );

				// $ustmt->execute($userData);

				// $astmt = $this->conn->prepare("UPDATE dh_user_details SET dh_user_details_id=? WHERE dh_user_details_id=?");

				// $aData = array(
				// 	$params['detailsUser'],
				// 	$params['userDetails']

				// );

				// $astmt->execute($aData);

				// User Education Details
				$educStmt = $this->conn->prepare("UPDATE dh_user_education SET dh_school_name=?, dh_school_location=?, dh_attainment=?, dh_year_from=?, dh_year_to=? WHERE dh_user_educ_id = ?");

				foreach($params['educSchool'] as $key=>$value){
					$dataUserEducation = array(
						$params['educSchool'][$key],
						$params['educLocation'][$key],
						$params['educAttainment'][$key],
						$params['educYearFrom'][$key],
						$params['educYearTo'][$key],
						$params['educId'][$key]
						
					);

					$educStmt->execute($dataUserEducation);
				}

				$this->conn->commit();
				$_SESSION['Message'] = "User Update Successfully!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function updateImagesHouse($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_images SET dh_image_path=? WHERE dh_image_id=? ");

					$imageData = array(
						$params['imagePath'],
						$params['imageId']
					);
				

				$stmt->execute($imageData);
				$this->conn->commit();
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function updateImagesNews($params){
			try {
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_images SET dh_image_path=? WHERE dh_image_id=? ");

					$imageData = array(
						$params['imagePath'],
						$params['imageId']
					);
				

				$stmt->execute($imageData);
				$this->conn->commit();
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
			}
		}

		public function getAllUserInfo($userId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_users as uM INNER JOIN dh_user_details as uMD ON uM.dh_user_details_id = uMD.dh_user_details_id INNER JOIN dh_user_education as uE ON 
						uM.dh_user_id = uE.dh_user_id WHERE uM.dh_user_id != ? AND dh_user_group_id > 1 AND uE.dh_user_id >1 ORDER BY dh_lastName ASC");
				$stmt->execute(array($userId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAlluserGroups(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_user_groups GROUP BY dh_user_group_id DESC");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAlluserGroupsexceptempAdmin(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_user_groups WHERE dh_user_group_id != 1 GROUP BY dh_user_group_id DESC");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllNews(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_news as dN INNER JOIN dh_images as iM ON dN.dh_news_id = iM.dh_news_id WHERE isNews = 1 AND dh_news_status = 0");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllDriver(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_driver_info");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllVehicle(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_vehicle_info");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getNewsbyId($newsId){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_news as dN INNER JOIN dh_images as iM ON dN.dh_news_id = iM.dh_news_id WHERE dN.dh_news_id = ? AND isNews = 1 ORDER BY dh_date_created DESC");
				$stmt->execute(array($newsId));
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function getAllArchiveNews(){
			try {
				$stmt = $this->conn->prepare("SELECT * FROM dh_news as dN INNER JOIN dh_images as iM ON dN.dh_news_id = iM.dh_news_id WHERE isNews = 1 AND dh_news_status = 1 ORDER BY dh_date_created DESC");
				$stmt->execute(array());
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch (PDOException $e) {
				echo $e->getMessage();
				throw $e;
			}
		}

		public function deleteUserStatus($params){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("UPDATE dh_users set dh_status=? WHERE dh_user_id = ?");
				$updateData = array(
					$params['userstatus'],
					$params['empId']
				);
				$stmt->execute($updateData);

				$this->conn->commit();
				$_SESSION['Message'] = "Account User Successfully Deleted!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function deleteUserbyId($userId){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("DELETE dh_users, dh_user_details, dh_user_education FROM dh_users INNER JOIN dh_user_details ON dh_users.dh_user_details_id = dh_user_details.dh_user_details_id INNER JOIN dh_user_education ON dh_users.dh_user_id = dh_user_education.dh_user_id WHERE dh_users.dh_user_id = ?");
				$stmt->execute(array($userId));

				$this->conn->commit();
				$_SESSION['Message'] = "Account User Successfully Deleted!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function deleteSalesbyId($salesId){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("DELETE FROM dh_closing WHERE dh_closing_id = ?");
				$stmt->execute(array($salesId));

				$this->conn->commit();
				$_SESSION['Message'] = "Closing Sales Successfully Removed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function deleteTrippingbyId($tripId){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("DELETE FROM dh_vehicletrip_calendar WHERE dh_vehicle_trip_id = ?");
				$stmt->execute(array($tripId));

				$this->conn->commit();
				$_SESSION['Message'] = "Vehicle Tripping Schedule Successfully Removed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

		public function deleteSeminarbyId($semId){
			try{
				$this->conn->beginTransaction();
				$stmt = $this->conn->prepare("DELETE FROM dh_seminar_calendar WHERE dh_seminar_id = ?");
				$stmt->execute(array($semId));

				$this->conn->commit();
				$_SESSION['Message'] = "Seminar Schedule Successfully Removed!";
				return true;
			} catch (PDOException $e){
				echo $e->getMessage();
				throw $e;
			}
		}

	}